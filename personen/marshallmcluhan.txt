#title "Marshall McLuhan"

**Herbert Marshall McLuhan**, CC (* 21. Juli 1911 in Edmonton, Alberta; † 31. Dezember 1980 in Toronto) war ein kanadischer Philosoph, Geisteswissenschaftler, Professor für englische Literatur, Literaturkritiker, Rhetoriker und Kommunikationstheoretiker. McLuhans Werk gilt als ein Grundstein der [Medientheorie](Medien). Seine zentrale These lautet *»Das Medium ist die Botschaft«*. Außerdem formulierte er die Begriffe der [Gutenberg-Galalxis](Gutenberg-Galaxis) und des *[Global Village](Global Village)*. McLuhan prägte die Diskussion über Medien von den späten 1960er Jahren bis zu seinem Tod.

## Links

  * <%= wpde("Marshall McLuhan") %> in der Wikipedia