#title "John Resig"

**John Resig** ist ein [JavaScript](JavaScript)-Guru, dem wir unter anderem [jQuery](jQuery) und [Processing.js](ProcessingJS) verdanken.

## Links

  * [Homepage John Resig](http://ejohn.org/)