#title "Matt Neuburg"

**Matt Neuburg** ist ein [Frontier](Frontier)-Veteran und Schöpfer von [RubyFrontier](RubyFrontier), der [Static Site Engine](Statische Seiten), die auch [dieses Wiki](Startseite) antreibt. Er ist Autor diverser Bücher zur [Mac](MacOS X)-Programmierung.

Sein Buch <a href="http://www.amazon.de/gp/product/1565923839/ref=as_li_ss_tl?ie=UTF8&tag=derschockwell-21&linkCode=as2&camp=1638&creative=19454&creativeASIN=1565923839">Frontier. The Definitive Guide</a><img src="http://www.assoc-amazon.de/e/ir?t=derschockwell-21&l=as2&o=3&a=1565923839" width="1" height="1" border="0" alt="" style="border:none !important; margin:0px !important;" /> war lange Jahre so etwas wie meine Bibel.

## Links

  * [Homepage Matt Neuburg](http://www.tidbits.com/matt/)