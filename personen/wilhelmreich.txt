#title "Wilhelm Reich"

<div style="float:right; margin-left: 8px;"><iframe src="http://rcm-de.amazon.de/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=derschockwell-21&o=3&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=3866476663" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe></div>

**Wilhelm Reich** (* 24. März 1897 in Dobzau, Galizien, Österreich-Ungarn; † 3. November 1957 in Lewisburg, Pennsylvania, USA) war ein österreichisch-US-amerikanischer Psychiater, Psychoanalytiker, Sexualforscher und Soziologe. Wilhelm Reich lebte zwei Leben, bis 1940 das des Theoretikers und (Psycho-) Analytikers, nachdem er nach 1940 jedoch glaubte, mit dem *Orgon* eine neue Energie entdeckt zu haben, glitt er in die Esoterik ab.

Mit seinem 1933 veröffentlichten Werk <a href="http://www.amazon.de/gp/product/3866476663/ref=as_li_ss_tl?ie=UTF8&tag=derschockwell-21&linkCode=as2&camp=1638&creative=19454&creativeASIN=3866476663">Die Massenpsychologie des Faschismus</a><img src="http://www.assoc-amazon.de/e/ir?t=derschockwell-21&l=as2&o=3&a=3866476663" width="1" height="1" border="0" alt="" style="border:none !important; margin:0px !important;" /> schuf er jedoch eine wissenschaftliche Grundlage für die Theoriekritik des Faschismus. Als erster durchschaute Reich mit seinem klinisch und soziologisch geschulten Blick den fundamentalen Zusammenhang zwischen autoritärer Triebunterdrückung und faschistischer Ideologie.

Reichs Werk geriet nach seinem mysteriösen Tod im amerikanischen Gefängnis 1957 in Vergessenheit, er wurde jedoch dann von der Studentenbewegung wiederentdeckt. *Die Massenpsychologie des Faschismus* wurde zu dem Werk, das am häufigsten »raubkopiert« wurde. (In der Hauptsache, weil es im regulären Buchhandel nicht (mehr) erhältlich war.)

## Links

  * <%= wpde("Wilhlem Reich") %> in der Wikipedia
  * Bernd Nitzschke: *[Zum 150. Geburtstag von Wilhelm Reich](http://www.hagalil.com/archiv/2012/03/22/wilhelm-reich/)*, HaGalil.com am 22. März 2012
  * <%= wpde("Bernd Nitzschke") %>: *[Der »Fall« Wilhelm Reich](http://www.hagalil.com/archiv/2012/03/22/reich/)*, HaGalil.com am 22. März 2012
  * Im Original: *[Die Massenpsychologie des Faschismus](http://www.hagalil.com/archiv/2007/11/reich.htm)*, HaGalil.com am 22. März 2012
  * Pharmacon.net: *[Reichsche Massage](http://www.pharmacon.net/2009/06/reich/)*, vom 15. Juni 2009