#title "Hans Magnus Enzensberger"

**Hans Magnus Enzensberger** (* 11. November 1929 in Kaufbeuren) ist ein deutscher Dichter, Schriftsteller, Herausgeber, Übersetzer und Redakteur.

## In diesem Wiki

[[FIXME]]

## Literatur

  * Hans Magnus Enzensberger: *[Das digitale Evangelium](http://www.spiegel.de/spiegel/print/d-15376078.html), Spiegel 2/2000, vom 10. Januar 2000
  * Hans Magnus Enzensberger: *[Wehrt Euch! Enzensbergers Regeln für die digitale Welt](http://www.faz.net/aktuell/feuilleton/debatten/enzensbergers-regeln-fuer-die-digitale-welt-wehrt-euch-12826195.html)*, FAZ.net vom 28. Februar 2014
    * Meine Meinung: Auch wenn er teilweise Recht hat, so spricht doch insgesamt eine große kulturpessimistische Technikphobie aus dem Beitrag. Man sollte sich die Technik aneignen wo sie nützlich ist und ihre Auswüchse bekämpfen wo notwendig, statt alles boykottieren zu wollen. Das haben schon die Maschinenstürmer des 19. Jahrhunderts vergeblich versucht und wie Marx sagte: Die Geschichte wiederholt sich, aber nur als Farce.
  * Don Dahlmann: *[Enzensberger und die Weltrevolution](http://www.dondahlmann.de/?p=24474)*, 2. März 2014

## Links

  * <%= wpde("Hans Magnus Enzensberger") %> in der Wikipedia
