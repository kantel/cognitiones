#title "Stable Diffusion"
#created "7. Oktober 2023"
#updated "8. Oktober 2023"

**Stable Diffusion** ist ein Deep-Learning-Text-zu-Bild-Generator. Die in [Python](Python) erstellte Open-Source-Software (MIT-Lizenz) wird hauptsächlich zur Generierung detaillierter Bilder auf der Grundlage von Textbeschreibungen verwendet, kann aber auch für andere Aufgaben wie Inpainting, Outpainting und die Erzeugung von Bild-zu-Bild-Übersetzungen auf der Grundlage einer Textaufforderung *(Prompt)* eingesetzt werden.

## Literatur und Tutorials

- Stefan Luber: *[Was ist Stable Diffusion](https://www.bigdata-insider.de/was-ist-stable-diffusion-a-9e8e626b48fadc255238436e0399abdc/)?*, BigData Insider vom 4. Juli 2023
- Kevin Pahlke: *[Stable Diffusion -- Eine Einführung](https://www.adesso.de/de/news/blog/stable-diffusion-eine-einfuehrung.jsp)*, adesso Blog vpm 3. Juli 2023

## Links

- [Stable Diffusion Home](https://stability.ai/stable-diffusion)
- [Stable Diffusion Playground DreamStudio](https://beta.dreamstudio.ai/generate)
- [Stable Diffusion XL Playground Clipdrop](https://clipdrop.co/stable-diffusion)
- [Stable Diffusion @ GitHub](https://github.com/Stability-AI/stablediffusion)
- [Stable Diffusion @ Hugging Space](https://huggingface.co/spaces/stabilityai/stable-diffusion)
- [Stable Diffusion in der Wikipedia](https://de.wikipedia.org/wiki/Stable_Diffusion)