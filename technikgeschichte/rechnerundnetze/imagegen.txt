#title "Imagegen"
#created "2. Mai 2023"
#updated "5. Februar 2024"


<%= imageref("corgiimagegen") %>

**Imagegen** ist Googles Antwort auf [DALL-E](DALL-E). Imagen wurde am 1. Februar 2024 in Googles Chatbot [Bard](Google Bard) integriert, war aber am 5. Februar 2024 in Deutschland noch nicht verfügbar.

## Video

<iframe width="560" height="315" src="https://www.youtube.com/embed/HyOW6fmkgrc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Literatur 

- Saharia, Chitwan, et al.: *[Photorealistic Text-to-Image Diffusion Models with Deep Language Understanding](https://arxiv.org/abs/2205.11487)*, arXiv preprint arXiv:2205.11487 (2022).
- Silke Hahn: *[Fotorealistische KI-Bildsynthese: Google macht DALL-E-2 Konkurrenz – mit Imagen](https://www.heise.de/news/Fotorealistische-KI-Bildsynthese-Google-macht-DALL-E-2-Konkurrenz-mit-Imagen-7121826.html)*, Heise Online vom 25. Mai 2022  

- ## Links  

- [Imagegen Home](https://imagen.research.google/)  