#title "Leonardo AI"
#created "7. Oktober 2023"
#updated "7. Oktober 2023"

<%= imageref("leonardo-ai-1") %>

**Leonardo AI** ist ein innovativer, kreativer AI-Bildgenerator, der sich auf die Erzeugung von Spielinhalten wie Texturen, Gegenständen und Konzeptgrafiken spezialisiert hat. Das Tool basiert auf [Stable Diffusion](Stable Diffusion), kann über das Leonardo-Dashboard auf der Hauptwebsite verwendet werden und ist in der Grundversion (150 »Tokens« per Tag, nicht alle Features verfügbar) kostenlos. Leonardo AI setzt fortschrittliche Algorithmen und Funktionen ein, um beeindruckende AI-generierte Bilder zu kreieren. Hinter dem Projekt steht ein engagiertes Team aus Künstlern, Entwicklern und Technologieenthusiasten, die bereits eine starke Community von über einer Million Mitgliedern auf ihrem Discord-Server versammelt haben.

## Literatur

- Finn Hillebrandt: *[Leonardo.ai Erfahrungen und Test](https://www.blogmojo.de/tool/leonardo-ai/)*, Blogmojo.de, ohne Datum (2023)

## Links

- [Leonardo AI Home](https://app.leonardo.ai/)