#title "Ultimate Machine"

<iframe width="480" height="296" src="http://www.youtube.com/embed/cZ34RDn34Ws" frameborder="0" allowfullscreen></iframe>

Die **Ultimate Machine** von [Claude Elwood Shannon](Claude Elwood Shannon) ist ein Kästchen mit einem Schalter, den eine mechanische Hand wieder auf »aus« stellt, nachdem man ihn eingeschaltet hatte.

## Links

  * [Ultimate Machine Fanpage auf Facebook](http://www.facebook.com/The.Ultimate.Machine.Fan.Page)



