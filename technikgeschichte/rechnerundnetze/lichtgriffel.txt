#title "Lichtgriffel"

Ein **Lichtgriffel** ist ein Computer-Zeigegerät zum direkten Arbeiten auf einem Röhren-Bildschirm. Er wurde 1949 am Lincoln Laboratory (Massachusetts Institute of Technology) entwickelt. (Manche Quellen sprechen von 1955; die erste CAD-Anwendung Sketchpad mit Lichtgriffel wurde 1963 vorgestellt.)

## Links

  * [Lichtgriffel](http://de.wikipedia.org/wiki/Lichtgriffel) in der Wikipedia