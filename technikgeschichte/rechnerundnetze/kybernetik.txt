#title "Kybernetik"

Die **Kybernetik** (zu altgr. κυβερνετικὴ τέχνη, Steuermannskunst, zu kybernétes, »Steuermann«, lat. kybernesis, »Leitung«) ist die Wissenschaft von der Struktur komplexer Systeme, insbesondere der Kommunikation und Steuerung einer Rückkopplung (engl. feedback) bzw. eines Regelkreises.

Als »Vater der Kybernetik« gilt der amerikanische Wissenschaftler [Norbert Wiener](Norbert Wiener) (1894 - 1964)

## Links

  * <%= wpde("Kybernetik") %> in der Wikipedia
