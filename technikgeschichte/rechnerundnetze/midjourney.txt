#title "Midjourney"
#updated "3. Oktober 2023"

**Midjourney** ist ein weiteres Werkzeug, das mit KI-Hilfe aus Texteingaben Bilder realisiert. Das proprietäre Programm wurde von dem gleichnamigen Forschungsinstitut aus San Francisco, Kalifornien, USA, geschaffen, welches von *David Holz* gegründet wurde und aktuell geleitet wird. Es ist zur Zeit (seit Juli 2022) in einer *offenen beta* und die Ergebnisse sollen »atemberaubend« sein.

Midjourney konnte bis Ende März 2023 von jedermann zunächst kostenlos ausprobiert werden, aufgrund von »zuviel Mißbrauch« wurde diese Möglichkeit jedoch beendet. Wer Midjourney nun nutzen will, muß blechen.

## Videos

<iframe width="560" height="315" src="https://www.youtube.com/embed/Eg41fjT2MbM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Links  

- [Midjourney Home](https://www.midjourney.com/)
- [Midjourney in der Wikipedia](https://de.wikipedia.org/wiki/Midjourney)