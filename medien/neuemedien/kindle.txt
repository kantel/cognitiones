#title "Amazon Kindle"

**Amazon Kindle** ist eine Produktserie von eBook-Readern von Amazon. Mit den Geräten können elektronische Bücher (E-Bücher bzw. [eBooks](eBook)), elektronische Zeitschriften und elektronische Zeitungen (E-Papers) von Amazon heruntergeladen und gelesen werden.

Außerdem bietet Amazon ebenfalls *Kindle* genannte Applikationen für MacOS X, iOS (iPad, iPhone), Android und Windows an, mit denen ebenfalls Kindle-Inhalte heruntergeladen und dargestellt werden können.

## Formate

Unterstützte Anzeigeformate sind Kindle (.azw), Text (.txt), HTML (.html), Microsoft Word (.doc), Bild (.jpeg, .gif, .png, .bmp), Portable Document Format (.pdf) und [Mobipocket](Mobipocket) (.mobi, .prc).

## Links

  * <%= wpde("Amazon Kindle") %> in der Wikipedia