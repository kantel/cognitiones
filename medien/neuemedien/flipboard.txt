#title "Flipboard"

<iframe width="480" height="296" src="http://www.youtube.com/embed/-JWklY5Ntk8" frameborder="0" allowfullscreen></iframe>

**Flipboard** ist ein persönlicher *Newsreader* für das iPad, der in seiner Optik an ein Magazin erinnert. Er liefert Teaser-Texte und Bilder der diversen Nachrichtenquellen, die die Software aus den Empfehlungen der Twitter- und Facebook-Freunde extrahiert. Dabei liest sie nicht den [RSS](RSS)- oder Atom-Feed der Quellen aus, sondern fungiert eher als eine Art *Screen-Scraper*.

## in diesem Wiki

Für das Web wurde so etwas schon 1995 mit den [Krakatoa Chronicle](Krakatoa Chronicle) versucht.

## Quellen

  * Matthias Schwenk: [Flipboard: Schnelles Ende der iPad-Euphorie bei den Verlagen?](http://carta.info/31075/flipboard-schnelles-ende-der-ipad-euphorie-bei-den-verlagen/)
   * [Dave Winer](Dave Winer): [About Flipboard and reading surfaces](http://scripting.com/stories/2010/07/22/aboutFlipboardAndReadingSu.html)

## Links

  * [Flipboard Homepage](http://www.flipboard.com/)


  