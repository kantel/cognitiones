#title "Create Space"

**Create Space** ist (nicht nur) das *Print on Demand*-Angebot von [Amazon](Amazon). Es erlaubt auch den Vertreib von [eBooks](eBook), [Audio](Audio)-Dateien und DVDs. Seit Mai 2012 ist *Create Space* (eingeschränkt) auch in Europa verfügbar (der Vertrieb über Dritte wie z.B. den Buchhandel funktioniert -- im Gegensatz zum US-amerikanischen Angebot -- noch nicht). Dafür ist *Create Space* unschlagbar günstig, [so die eBook News](http://www.e-book-news.de/neue-chancen-fur-self-publisher-amazon-startet-print-on-demand-in-europa/):

<blockquote>Ein <a href="https://www.createspace.com/Products/Book/#content5">Preiskalkulator</a> ermöglicht es nämlich, bei Create Space die voraussichtliche Gewinnspanne vorab zu berechnen. Ein Buch in schwarz-weiß mit 120 Seiten im Format 6 mal 9 Zoll (ca. 15 x 22 cm) erzielt bei einem anvisierten Verkaufspreis von 10 Euro beispielsweise 3,96 Euro Tantiemen, also immerhin knapp 40 Prozent. Der <a href="http://www.epubli.de/projects/preise#">Preiskalkulator</a> der deutschen Print-On-Demand-Plattform <a href="epubli">epubli</a> dagegen zeigt bei vergleichbaren Kalkulationsdaten lediglich 1,53 Euro Tantieme an. […] Denn die epubli-Tantieme im obigen Beispiel gilt nur für den Verkauf im firmeneigenen Shop. Beim Vertrieb über Amazon liegt bei einem Endpreis von 10 Euro die Tantieme lediglich bei 22 Cent.</blockquote>

##Create Space Video Tutorial

<iframe width="560" height="315" src="http://www.youtube.com/embed/_rB8700WLZg?rel=0" frameborder="0" allowfullscreen></iframe>

*Self Publishing Books on Amazon.com in Less than 7 Days*

## Literatur

<div style="float:right; margin-left: 8px;"><iframe src="http://rcm-de.amazon.de/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=derschockwell-21&o=3&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=B008A8LE0E" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe></div>

  * Daniel Morawek: *[Erfahrungsbericht: Mein erstes Print-Buch mit Amazons CreateSpace](http://www.danielmorawek.de/2012/05/24/erfahrungsbericht-mein-erstes-print-buch-mit-amazons-createspace/)*
  * Wolfgang Meilenstein: *[Veröffentlichung über Create Space](http://wolfgangmeilenstein.jimdo.com/artikel/veröffentlichung-über-createspace/)*

## Link

  * [Create Space Homepage](https://www.createspace.com/)
  * [Ankündigung des Starts in Europa](https://www.createspace.com/Special/PRArchive/2012/20120517_EuropeDistribution.jsp)
  * [Create Space Preiskalkulator](https://www.createspace.com/Products/Book/#content5)

 
