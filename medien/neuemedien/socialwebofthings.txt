#title "Social Web of Things"

<iframe src="http://player.vimeo.com/video/22022800?title=0&amp;byline=0&amp;portrait=0" width="480" height="270" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>

Der schwedische Ericsson Konzern hat mal eine Vision der zukünftigen technologischen Ausstattung im Haushalt in einen kurzen Film gegossen.

## Links

  * Nico Lumma: *[Wenn das Sofa mit dem Staubsauger redet](http://lumma.de/2012/04/27/wenn-das-sofa-mit-dem-staubsauger-redet/)*, Lummaland, 27. April 2012