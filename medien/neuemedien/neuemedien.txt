#title "Neue Medien"

Hier sammle ich Arbeitsmaterial zu den »**Neuen Medien**« und zur Geschichte der neuen Medien.

<iframe width="480" height="295" src="http://www.youtube.com/embed/hFSPHfZQpIQ" frameborder="0" allowfullscreen></iframe>

*The Secret History of Silicon Valley*, Vortrag von *Steve Blank* auf der Google TechTalk vom 18. Dezember 2007.

## Reader

  * Im [E-Publishing Reader](E-Publishing Reader) sammele ich Links und Dokumente zum Bereich E-Publishing, Neue Medien und Open Access.
  * [Amazon Kindle](Amazon Kindle)

## In diesem Wiki

  * [Book on Demand](Book on Demand)
  * [eBook](eBook)
  * Der elektrische Reporter über [digitalen Aktivismus](Digitaler Aktivismus)
  * The [Krakatoa Chronicle](Krakatoa Chronicle)
  * [Flipboard](Flipboard)
  * [Medium.com](mediumcom)
  * [Tablet Newspaper (1994)](Tablet Newspaper 1994)

## Software

  * [Book Creator](Book Creator)
  * [Calibre](Calibre)
  * [E-Book-Creator](E-Book-Creator)
  * [LovelyReader](LovelyReader)
  * [Press-Room](Press-Room)
  * [Sigil](Sigil)
  * [Stanza](Stanza) ist ein Epub-Reader für das iPhone und als Stanza Desktop eine Konvertierungssoftware für den Mac, die diverse Formate in das Epub-Format umwandelt.

## Visionen

  * [Social Web of Things](Social Web of Things)

## Literatur

  * [The Web That Wasn't](http://www.youtube.com/watch?v=72nfrhXroo8), Knapp einstündiges Video eines Vortrags von *Alex Wright* über die frühe Geschichte des Internets auf der Google TechTalk vom 23.Oktober 2007

## Links

  * [Elektrischer Reporter](http://www.elektrischer-reporter.de/)
  * [Netzpolitik.org](http://www.netzpolitik.org/)
  
