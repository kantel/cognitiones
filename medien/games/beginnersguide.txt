#title "The Beginner's Guide"

<iframe width="560" height="315" src="https://www.youtube.com/embed/OPP9pdApRQE" frameborder="0" allowfullscreen></iframe>

**The Beginner's Guide** ist ein Videospiel für Mac und PC, das sich in ungefähr zwei Stunden durchspielen läßt. Im Gegensatz zu traditionellen Spielen besitzt es keine der üblichen Spielmechaniken und kein Ziel. Stattdessen wird einfach eine Geschichte erzählt, in der jemand mit etwas konfrontiert wird, was er selber nicht versteht.

  * Autor: Davey Wreden
  * Produzent: Everything Unlimited Ltd.
  * Preis: etwa 9 Euro

## Literatur

  * Benedikt Frank: *[Computerspiel »The Beginner's Guide«: Das Zwei-Stunden-Meisterwerk](http://www.spiegel.de/netzwelt/games/the-beginner-s-guide-fuer-pc-und-mac-im-test-a-1057532.html)*, Spiegel Online vom 14. Oktober 2015

## Links

  * [The Beginners Guide Home](http://thebeginnersgui.de/)