#title "Games"

Da für meine Überlegungen nicht nur [Retro Games](Retro Games) interessant sein können, sondern unter Umständen auch aktuelle Spiele, habe ich ihnen eine eigene Seite gewidmet.

## Genre

  * [Adventure](Adventure)
  * [Interactive Fiction](Interactive Fiction)
  * [Japanisches Adventure](Japanisches Adventure)
  * [LitRPG](LitRPG)

## Spiele

  * [Retrogames](Retrogames)
  * [The Beginner's Guide](The Beginner's Guide)
  * [Minetest](Minetest)