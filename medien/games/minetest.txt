#title "Minetest"

<%= imageref("minetest") %>

**Minetest** (ursprünglich *Minetest-c55*) ist ein freies (GPL) Open-World-Spiel für Windows, Linux, macOS und andere. Minetest wurde inspiriert von [Minecraft](Minecraft). In zufällig generierten Welten, die aus Blöcken bestehen, kann der Spieler verschiedene Rohstoffe abbauen, diese miteinander kombinieren und die Welt nach seinem Belieben gestalten. Minetest ist in [C++](C++) implementiert, nutzt die [3D](3D)-Graphik-Engine [Irrlicht](Irrlicht) und kann durch Modding in der Skriptsprache [Lua](Lua) erweitert werden.

Das Spiel hat kein Ziel oder Ende, jedoch liegt der Fokus auf dem Konstruieren von Bauwerken aus Blöcken mit verschiedenen Texturen in einer 3D-Welt, auch bekannt als Voxel-Welt.

Minetest wird gerne im Unterricht und in sonstigen Bildungsprojekten eingesetzt.

## Tutorials

- Auf dem *Wiener Bildungsserver* gibt es eine [vierteilige Playlist](https://www.youtube.com/playlist?list=PLPY2zeEhdxt-kn5CquiJqlPGQAn8FrxKS) zu Minetest (jedes Video mit etwa fünf Minuten Spieldauer).
- Das *KidsLab Augsburg* hat eine [zweiteilige Playlist](https://www.youtube.com/playlist?list=PL1v6GySgoQyzwpuByaSzlHFigcNJ8WkLw) (Gesamtspieldauer 25 Minuten) zu Minetest online gestellt.
- Und vom User *henry8086* gibt es [diverse (meist deutschsprachige) Videos zu Minetest](https://www.youtube.com/channel/UCtr_VmMA2zsEjPyqCkQm1Bw/featured), darunter eine [Monsterplaylist mit 67 Videos](https://www.youtube.com/playlist?list=PL1IvRewDjrBYKfZa4YRd9QPi2NfRCC904) und einer Laufzeit von mehreren Stunden (mindestens ein Tag).

## Literatur

- Landesmedienzentrum Baden-Württemberg: *[Was ist Minetest](https://www.lmz-bw.de/medien-und-bildung/medienwissen/game-based-learning/blockalot/was-ist-minetest/)?* 
- Minetest Bildungsnetzwerk: *[Erste Schritte mit Minetest](https://blogs.rpi-virtuell.de/minetest/anleitungen/erste-schritte/).

## Links  

- [Minetest Home](https://www.minetest.net/)  
- [Minetest @ GitHub](https://github.com/minetest/minetest)  
- [Minetest Lua Modding API Reference](https://minetest.gitlab.io/minetest/) 
- [Minetest in der Wikipedia](https://de.wikipedia.org/wiki/Minetest) 