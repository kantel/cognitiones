# title "Medien und Medientheorie" #

## Seiten

  * [Alles Kino](Alles Kino) oder was?
  * [Datenjournalismus](Datenjournalismus)
  * [Open Data](Open Data)
  * [Zettelkasten](Zettelkasten)

## Konzepte ##

  * [Global Village](Global Village)
  * [Gutenberg-Galaxis](Gutenberg-Galaxis)
  * [Hypertext](Hypertext) und [Hypermedia](Hypermedia)
  * [IndieWeb](IndieWeb)
  * [Neue Medien](Neue Medien)
  * [Podcasting](Podcasting)
  * [Turing-Galaxis](Turing-Galaxis)

## Programme

  * [Auszeichnungssprachen](Auszeichnungssprachen)
  * [LaTeX](LaTeX)

## Tagungen

  * [HyperKult](HyperKult)


## Personen

  * [Wolfgang Coy](Wolfgang Coy)
  * [Niklas Luhmann](Niklas Luhmann)
  * [Marshall McLuhan](Marshall McLuhan)

## Sonstiges

  * [Retro Games](Retro Games)
  * [Games](Games)

## Literatur

  * Hans Magnus Enzensberger: *[Das digitale Evangelium](http://www.spiegel.de/spiegel/print/d-15376078.html)*, Spiegel 2/2000, vom 10. Januar 2000
  * Frank Schirrmacher: *[Das Armband der Neelie Kroes](http://www.faz.net/frankfurter-allgemeine-zeitung/frank-schirrmacher-12826199.html?printPagedArticle=true)*, FAZ.net vom 3. März 2014
  * 

