#title "Retro Games"
#updated "21. März 2023"


<iframe width="480" height="295" src="http://www.youtube.com/embed/OuO-s1kugLs" frameborder="0" allowfullscreen></iframe>

Video: [The evolution of PC games](http://www.theverge.com/gaming/2012/7/17/3157713/video-the-evolution-of-pc-games)

## (Freie) Emulatoren

  * [OpenEmu](OpenEmu)

## In diesem Wiki

[[dr]]<iframe src="http://rcm-eu.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=derschockwell-21&o=3&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=3956451155" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe>[[dd]]

  * [Angband](Angband)
  * [Bomberman](Bomberman) und der Bomberman-Klon [Bombsquad](Bombsquad)
  * [Baldur's Gate](Baldur's Gate)
  * [Cave Story](Cave Story)
  * [Computerspiel 1958](Computerspiel 1958)
  * [Final Fantasy](Final Fantasy)
  * [Freeciv](Freeciv)
  * [FreeCol](FreeCol)
  * [NetHack](NetHack)
  * [Pacman Dungeon](Pacman Dungeon)
  * [Pokémon](Pokémon)
  * [Rogue](Rogue)
  * [Space Invaders](Space Invaders)
  * [Super Mario](Super Mario)
  * [Zangband](Zangband)

## HyperCard basierte frühe Spiele

  * [Manhole](Manhole)
  * [Cosmic Osmo](Cosmic Osmo)
  * [Spelunx](Spelunx)
  * [Myst](Myst) und [Riven](Riven)

## Literatur

  * Tom Hillenbrand, Konrad Lischka: *[Buch-Crowdfunding: Nie war es leicher, Verleger zu sein](http://www.spiegel.de/netzwelt/games/rollenspiel-buch-drachenvaeter-crowdfunding-als-selbstversuch-a-951475.html)* über die Geburt Ihres Buches <a href="http://www.amazon.de/gp/product/3956451155/ref=as_li_ss_tl?ie=UTF8&camp=1638&creative=19454&creativeASIN=3956451155&linkCode=as2&tag=derschockwell-21">Drachenväter – Die Geschichte des Rollenspiels und die Geburt der virtuellen Welt</a><img src="http://ir-de.amazon-adsystem.com/e/ir?t=derschockwell-21&l=as2&o=3&a=3956451155" width="1" height="1" border="0" alt="" style="border:none !important; margin:0px !important;" />, Spiegel Online vom 30. März 2014
  * Benjamin Knaack: *[»Age of Empires II« wiederentdeckt: Spät nachts, wenn alle über 17 schlafen](http://www.spiegel.de/netzwelt/games/age-of-empires-2-im-retro-test-erinnerungen-an-den-klassiker-a-1172669.html)*, Spiegel Online vom 22. Oktober 2017
  * Matthias Kreienbrink: *[»Secret of Mana« wiederentdeckt: Die Puschel tanzen Pogo](http://www.spiegel.de/netzwelt/games/secret-of-mana-im-retro-test-die-puschel-tanzen-pogo-a-1172365.html)*, Spiegel Online vom 17. Oktober 2017
