#title "Space Invaders"

Space Invaders ist ein *Shoot 'em up*-Spiel, entworfen und programmiert von *Toshihiro Nishikado* und vertrieben von *Taito*. Erschienen ist es im Jahre 1978. Neben *Pacman* war Space Invaders eines der Spiele, die die frühe Entwicklung der Videospiele geprägt haben.

## Beschreibung

Der Spieler steuert eine Kanone, die er am unteren Bildschirmrand nach links und rechts bewegen kann. Jeder Level beginnt mit mehreren Reihen regelmäßig angeordneter Aliens, die sich ständig horizontal und dabei nach und nach abwärts bewegen und den Spieler mit Geschossen angreifen. Der Spieler selbst hat einen unbegrenzten Munitionsvorrat, kann aber erst dann ein neues Geschoß abfeuern, wenn das vorige vom Bildschirm verschwunden ist. Wenn es einem der Aliens gelingt, den unteren Bildschirmrand zu erreichen und neben der Kanone zu landen, verliert der Spieler eines seiner Leben. 

## Links

  * <%= wpde("Space Invaders") %> in der Wikipedia
  * Spiegel Online: [30 Jahre Space Invaders: Angriff der Aliens](http://einestages.spiegel.de/static/topicalbumbackground/2376/angriff_der_aliens.html)
  