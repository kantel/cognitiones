#title "Bomberman"

**Bomberman** (auf manchen Plattformen **Dynablaster** oder **Dyna Blaster**) ist ein labyrinthartig aufgebautes, kommerzielles Computerspiel, welches ursprünglich 1983 von Hudson entwickelt wurde.

<iframe width="560" height="315" src="https://www.youtube.com/embed/3smytj9Bu_E?rel=0" frameborder="0" allowfullscreen></iframe>

In der klassischen Variante besteht das Spielfeld aus einer Anordnung von zerstörbaren und unzerstörbaren Wänden. Durch das Legen von Bomben können somit immer mehr Bereiche des Spielfelds erschlossen werden. Hinter einigen Wänden verstecken sich Bonusgegenstände. Die wichtigsten, die in nahezu jeder Version vorkommen, sind die *Bombe*, mit ihr erhält der Spieler die Möglichkeit, eine zusätzliche Bombe zeitgleich zu legen (maximal 8). Eine *Flamme* erhöht die Reichweite einer Bombe um ein Feld.

<iframe width="560" height="315" src="https://www.youtube.com/embed/reKN7IBUakY?rel=0" frameborder="0" allowfullscreen></iframe>

Die Explosion wird durch Feuerstrahlen in alle vier Richtungen des zweidimensionalen Raums dargestellt und bringt andere Bomben (falls sie durch die Sprengkraft erreicht werden) zur sofortigen Zündung, was gewisse Taktiken ermöglicht und erfordert.

<iframe width="560" height="315" src="https://www.youtube.com/embed/4yG5N6RgctA?rel=0" frameborder="0" allowfullscreen></iframe>

Ein beliebter Spielmodus von Bomberman ist auch der Mehrspielermodus, in welchem alle Mitspieler besiegt werden müssen.

## Links

  * [Bomberman](https://de.wikipedia.org/wiki/Bomberman) in der Wikipedia

