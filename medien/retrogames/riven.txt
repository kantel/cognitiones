#title "Riven"

<%= imageref("rivenios") %>

**Riven** ist das 1997 erschienene Nachfolgespiel zu [Myst](Myst) und das erste Spiel der *Cyan*-Gründer *Rand* und *Robyn Miller*, das nicht mehr mit [HyperCard](HyperCard) erstellt wurde. Nach dem Erscheinen des Spieles hat Robyn Miller Cyan verlassen, um seinen anderen Interessen, speziell als Filmregisseur und Produzent, nachzugehen, während sein Bruder die Spieleschmiede bis heute weiterbetreibt.

Riven ist dynamischer als der Vorgänger Myst und setzt aber die Handlung genau dort fort, wo Myst aufhörte.

Auch Riven gibt es -- allerdings nur in einer englischen Fassung -- für $&nbsp;3,99 für iOS im AppStore zu erwerben.

## Links

  * [Riven @ Cyan](http://cyan.com/games/riven/)
  * [Riven for iOS @ Cyan](http://cyan.com/iOS_Riven/Riven_iOS/Welcome.html)
  * [Riven for iOS im AppStore](https://itunes.apple.com/us/app/riven-the-sequel-to-myst/id400293367)
  * <%= wp("Riven") %> in der Wikipedia