#title "Gutenberg-Galaxis"

<div style="float:right; margin-left: 8px"><iframe src="http://rcm-de.amazon.de/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=derschockwell-21&o=3&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=3943330001" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe></div>

Der Begriff der **Gutenberg-Galaxis** wurde von [Marshall McLuhan](Marshall McLuhan) in seinem 1962 erschienenen Buch *<a href="http://www.amazon.de/gp/product/3943330001/ref=as_li_ss_tl?ie=UTF8&camp=1638&creative=19454&creativeASIN=3943330001&linkCode=as2&tag=derschockwell-21">The Gutenberg Galaxy</a><img src="http://www.assoc-amazon.de/e/ir?t=derschockwell-21&l=as2&o=3&a=3943330001" width="1" height="1" border="0" alt="" style="border:none !important; margin:0px !important;" />* geprägt; er bezeichnet eine Welt, die grundlegend vom Buch als Leitmedium geprägt ist.

## Links

  * Die <%= wpde("Gutenberg-Galaxis") %> in der Wikipedia