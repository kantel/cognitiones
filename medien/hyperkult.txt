#title "HyperKult"

Die **HyperKult** ist die Tagung der Fachgruppe »Computer als Medium« der Gesellschaft für Informatik, die seit der Gründung der Fachgruppe 1991 jährlich abgehalten wird. Sie findet regelmäßig Anfang Juli an der *Leuphana Universität Lüneburg* statt.

## Literatur

<div style="float:right; margin-left: 8px;"><iframe src="http://rcm-de.amazon.de/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=derschockwell-21&o=3&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=3861091410" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe>&nbsp;<iframe src="http://rcm-de.amazon.de/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=derschockwell-21&o=3&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=3899422740" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe></div>

  * [Wolfgang Coy](Wolfgang Coy), Georg Christoph Tholen, Martin Warnke (Hrsg.): *<a href="http://www.amazon.de/gp/product/3861091410/ref=as_li_ss_tl?ie=UTF8&camp=1638&creative=19454&creativeASIN=3861091410&linkCode=as2&tag=derschockwell-21">HyperKult – Geschichte, Theorie und Kontext digitaler Medien</a><img src="http://www.assoc-amazon.de/e/ir?t=derschockwell-21&l=as2&o=3&a=3861091410" width="1" height="1" border="0" alt="" style="border:none !important; margin:0px !important;" />*, Basel und Frankfurt am Main (Stroemfeld) 1997
  * [Wolfgang Coy](Wolfgang Coy), Georg Christoph Tholen, Martin Warnke (Hrsg.): *<a href="http://www.amazon.de/gp/product/3899422740/ref=as_li_ss_tl?ie=UTF8&camp=1638&creative=19454&creativeASIN=3899422740&linkCode=as2&tag=derschockwell-21">HyperKult II – Zur Ortsbestimmung analoger und digitaler Medien</a><img src="http://www.assoc-amazon.de/e/ir?t=derschockwell-21&l=as2&o=3&a=3899422740" width="1" height="1" border="0" alt="" style="border:none !important; margin:0px !important;" />*, Bielefeld (Transcript) 2005

## Links

  * [Homepage HyperKult](http://www.leuphana.de/institute/icam/forschung-projekte/hyperkult.html)
  * <%= wpde("HyperKult") %> in der Wikipedia