#title "Hypertext"

**Hypertext** ist das Verfahren, Textpassagen untereinander durch Software-Links netzartig zu verknüpfen. [Ted Nelson](Ted Nelson) hatte es Mitte der sechziger Jahre, als an ein ziviles Internet noch nicht zu denken war, im Rahmen des [Xanadu](Xanadu)-Projekts mit der Absicht konzipiert, das digitalisierte Wissen der Menschheit effizienter zu organisieren.

## Links

  * Gundolf S. Freyermuth: *[Ein Nachruf auf die Hypertext-Bewegung](http://www.heise.de/tp/r4/artikel/2/2756/1.html). Mit einem Ausblick auf die Rolle der Literatur im Zeitalter ihrer Digitalisierung*, (Telepolis News, 1999)
 

