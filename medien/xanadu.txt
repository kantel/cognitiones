#title "Xanadu"

**Xanadu** ist ein 1960 begründetes [Hypertext](Hypertext)-Projekt von [Ted Nelson](Ted Nelson); durch das nach dem legendären Ort Xanadu benannte Projekt sollte das *Docuverse*, eine universale Bibliothek mit zahllosen miteinander vernetzten Dokumenten, entstehen: 

  * Wie das World Wide Web war Xanadu als dezentrales Speichersystem für Dokumente gedacht. Jedes Dokument in Nelsons Hypertext-Raum sollte eine absolut eindeutige Adresse (unabhängig vom Speicherort) besitzen.
  * Verweise (Links) sollten bidirektional sein; wenn man eine Seite in Xanadu betrachtete, sollte man also auch sehen, welche anderen Seiten auf diese Seite verwiesen.
  * Nur die Adressen von Inhalten sollten an den Stellen eingefügt werden, wo man sie zitiert. Der Client sorgt dann für die vollständige Darstellung.

Nelsons konzeptuelle Ideen für Xanadu beeinflußten gleichermaßen [Tim Berners-Lee](Tim Berners-Lee) bei der Entwicklung des World Wide Web wie auch [Ward Cunningham](Ward Cunningham) bei seinem [Wiki](Wiki)-Konzept.

## Video

<iframe width="560" height="315" src="http://www.youtube.com/embed/En_2T7KH6RA?rel=0" frameborder="0" allowfullscreen></iframe>

*Ted Nelson demonstriert [Xanadu Space](http://xanarama.net/).*

## Literatur

  * Gary Wolf: *[The Curse of Xanadu](http://www.wired.com/wired/archive/3.06/xanadu.html)*. In: *Wired*. Ausgabe 3.06, Juni 1995 (siehe auch die [öffentlichen Erwiderungen](http://www.wired.com/wired/archive/3.09/rants.html) auf den Artikel)
  * Georg Jünger: *[Xanadu - Ein Wissens- und Informationssystem](http://fsub.schule.de/freie/xanadu.htm)*. In: *taz*. vom 17./18. April 2003
  * Ted Nelson: *[Toward a Deep Electronic Literature: The Generalization of Documents and Media](http://xanadu.com/XanaduSpace/xuGzn.htm)*, Xanadu.com, zuletzt besucht am 29. Dezember 2012 

## Links

  * [Homepage von Ted Nelsons *Xanadu*](http://xanadu.com/)
  * [History of the Xanadu Projects](http://web.archive.org/web/20021209175848/udanax.com/history/index.html)