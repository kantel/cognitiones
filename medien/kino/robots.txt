#title "Robots"

<iframe width="560" height="315" src="http://www.youtube.com/embed/OJ7ZHdg7bA4?rel=0" frameborder="0" allowfullscreen></iframe>

Ich war gestern abend im Kino und habe mir **Robots** angeschaut. Und ich muß sagen, so schlimm, wie die Kritik befürchten ließ, war der Film doch gar nicht. Sicher, die Story war absolut Kinderfilm-kompatibel, also dünn und einfallslos und wie in fast allen amerikanischen Filmen wurde uns die Familie als Rettung von allem Übel verkauft, aber das sind wir doch schon gewohnt, oder? Dafür waren die Figuren der Roboter absolut liebevoll im Stile der Küchenmaschinen der 50er Jahre gestaltet, die Animationen teilweise atemberaubend, der Schnitt einfach Klasse. Und witzig — obwohl die Synchronisation mit den »Großen« der deutschen Comedy-Szene das Schlimmste befürchten ließ..., ja witzig waren selbst die meisten Dialoge. Der Zuschauer hatte also gar keine Zeit, sich bei der dünnen Story zu langweilen, es gab ständig etwas Neues in den Bildern zu entdecken und ich war wenige Sekunden, nachdem der Film begonnen hatte, gefesselt.

Außerdem mag ich es, wenn Filmzitate in Kinofilmen auftauchen, und in Robots wurde gnadenlos die gesamte Filmgeschichte geplündert. Und im Soundtrack wurde sich auch bei allem bedient, was in der Popgeschichte Rang und Namen hat, inklusive des von uns geliebten Tom Waits.

Wie man sieht, Gabi, Petra und ich haben uns köstlich amüsiert. Laßt Euch daher von der Kritik den Spaß an dem Film nicht verderben.

*(Geschrieben am [20. März 2005](http://www.schockwellenreiter.de/2005/03/20.html#gesternImKino))*

[[dr]]<iframe src="http://rcm-de.amazon.de/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=derschockwell-21&o=3&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=B000A2S8A2" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe>[[dd]]


## Links

  * <%= wpde("Robots") %> in der Wikipedia

