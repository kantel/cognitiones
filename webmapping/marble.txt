#title "Marble"

<%= imageref("marble", {:width => "480", :height => "378"}) %>

**Marble** (LGPL) ist ein freier geographischer Atlas und ein virtueller Globus. Mit Marble kann man einen Ort finden, auf einfache Art Karten erstellen, Entfernungen messen und Informationen über bestimmte Orte abfragen.

Neben der Erde kann das Programm weitere Himmelskörper wie Mond, Mars oder Venus anzeigen. Der mitgelieferte Umfang beträgt 5–10 Megabyte an Vektor- und Bitmap-Daten. Weiteres Kartenmaterial wird automatisch aus dem Internet nachgeladen und kann manuell hinzugefügt werden. Zudem ist es auch möglich, eigene Karten zu erstellen und zu integrieren.

Marble gibt es u.a. für Windows, [Linux](Linux) und [MacOS X](MacOS X).

## Marble und OpenStreetMap

<%= imageref("marble-osm", {:width => "480", :height => "377"}) %>

Eines der mitgelieferten Module integriert [OpenStreetMap](OpenStreetMap) in Marble. Es gibt weitere Module, die zum Beispiel Wikipedia-Artikel zu den Orten oder das aktuelle Wetter dort anzeigen.

Marble kann auch mit [KML](KML)-Dateien umgehen.

## Links

  * [Marble Homepage](http://edu.kde.org/marble/)
  * <%= wpde("Marble (Computerprogramm)") %> in der Wikipedia
  * Das [Handbuch zu Marble](http://docs.kde.org/stable/de/kdeedu/marble/)