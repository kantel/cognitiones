#title "GRASS GIS"

<%= imageref("grass") %>

**GRASS** *(Geographic Resources Analysis Support System)* ist eine umfangreiche Sammlung kleiner Kommandozeilen-Tools für geographische Raster- und Vektordaten. Die Software gibt es für Windows, [Linux](Linux), [MacOS X](MacOS X) und diverse Unixe.

Durch den modularen Aufbau (GRASS GIS ist eine unixtypische Sammlung vieler kleiner Programme) lassen sich einzelne Elemente einfach mit eigenen Applikationen unter einer Benutzeroberfläche verbinden.

<div style="float:right; margin-left: 8px"><iframe src="http://rcm-de.amazon.de/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=derschockwell-21&o=3&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=1441942068" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe></div>

## Links

  * [GRASS Home](http://grass.fbk.eu/)
  * <%= wpde("GRASS GIS") %> in der Wikipedia