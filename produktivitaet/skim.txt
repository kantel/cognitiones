#title "Skim"

**Skim** (BSD-Lizenz) ist ein annotationsfähiger PDF-Reader für [MacOS X](MacOS X). Er ist designed für Wissenschaftler, die Papiere kommentieren wollen, aber die Software ist auch als allgemeiner Viewer für alle Arten von PDF-Dokumeten geeignet.

<%= imageref("screenshotskim") %>

Die Software will über einen extensiven [AppleScript](AppleScript)-Support verfügen und auch sonst ist mein erster Eindruck nicht schlecht.

Skim ist der ideale Viewer für [LaTeX](LaTeX)-Dokumente im Zusammenarbeit mit [Aquamacs](Aquamacs), weil dort die Textstellen synchronisiert werden.

## Links

  * [Skim Homepage](http://skim-app.sourceforge.net/)