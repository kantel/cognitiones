#title "Excel2LaTeX"

**Excel2LaTeX** (Freeware) kann eine Hilfe sein, wenn man komplexe (oder große) Tabellen in [LaTeX](LaTeX) nutzen will. Benötigt VBA (Visual Basic for Applications). 

## Links

  * [Excel2LaTeX auf CTAN](http://www.ctan.org/tex-archive/support/excel2latex/)