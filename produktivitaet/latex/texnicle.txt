#title "TeXnicle"

**TeXnicle** ist ein [LaTeX](LaTeX)-Editor und Projekt Organizer für [MacOS X](MacOS X) Snow Leopard und Lion. Die Software ist noch beta und frei wie Freibier (was sie nach Ankündigung der Macher auch bleiben soll).

## Links

  * [TeXnicle Home](http://www.bobsoft-mac.de/texnicle/texnicle.html)
