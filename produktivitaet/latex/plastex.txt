#title "plasTeX"

**plasTeX** (Python Software Foundation Licence) ist ein freies [Python](Python)-Framework, das [LaTeX](LaTeX)-Dokumente nach (X)HTML und [DocBook XML](DocBook XML). Aufgrund seiner Offenheit kann es auch angepaßt werden, um andere XML-Formate zu bedienen.

plasTeX unterscheident sich von ähnlichen Werkzeugen wie *LaTeX2HTML*, [TeX4ht](TeX4ht) oder [tth](tth) dadurch, daß es im ersten Schritt eine DOM-Struktur erzeugt, auf die erst im zweiten Schritt der Renderer zugreift. Durch diese Offenheit kann plasTeX unterschiedliche Ausgabeformate bedienen.

## Links

  * [plasTeX Home](http://plastex.sourceforge.net/) at SourceForge
  * [Download](http://sourceforge.net/projects/plastex/)
  

