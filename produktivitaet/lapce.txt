#title "Lapce"

<%= imageref("lapcelogo") %>

**Lapce** ist ein in [Rust](Rust) geschriebenener, freier (Apache-Lizenz), plattformübergreifender (macOS, Linux, Windows) Texteditor mit integriertem Terminal und responsiver Oberfläche, der durch ein Plugin-System erweiterbar ist.

## Literatur

<iframe width="560" height="315" src="https://www.youtube.com/embed/_52ZUg-QmW0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

GameFromScratch: *[Lapce Lightning Fast New Open Source Code Editor](https://gamefromscratch.com/lapce-lightning-fast-new-open-source-code-editor/)*, 10. September 2022

## Links

- [Lapce Home](https://lapce.dev/)  
- [Lapce @ GitHub](https://github.com/lapce/lapce)  
- [Lapce Dokumentation](https://docs.lapce.dev/)  