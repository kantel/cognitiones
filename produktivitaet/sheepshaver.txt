#title "SheepShaver"

<%= imageref("sheepshaverhc-b") %>

**SheepShaver** ist ein freier (GPL) Apple-Power-Macintosh-Emulator für Unix mit X11 (Linux und BSD), MacOS X (PowerPC und Intel), Windows und BeOS als Wirtsystem. Als PowerPC-Emulator unterstützt SheepShaver gegenwärtig MacOS 7.5.2 bis 9.0.4 als Gastsysteme.

Der Emulator ist zur Zeit die einzige Möglichkeit, für MacOS geschriebene Programme auf Intel-basierten Macs sowie auf allen Macs, auf denen MacOS X 10.5 und neuer läuft, zu nutzen. Dabei ist es sowohl auf PowerPC- als auch auf x86-Rechnern stabil und ausreichend schnell.

James Friend bietet SheepShaver zusammen mit [HyperCard](HyperCard) als [Bundle](https://jamesfriend.com.au/running-hypercard-stack-2014) an. Damit können auch alte HyperCard-Stacks auf modernen Macs wieder ausgeführt werden (siehe Screenshot).

<%= imageref("sheep") %>

## Links

  * [SheepShaver Home](http://sheepshaver.cebix.net/)
  * [Sourcecode @ GitHub](https://github.com/cebix/macemu)
  * [SheepShaver Wiki](http://www.emaculation.com/doku.php/sheepshaver)
  * [James Friends Sheepshaver-HyperCard-Bundle](https://jamesfriend.com.au/running-hypercard-stack-2014)
  * <%= wpde("SheepShaver") %> in der Wikipedia
