#title "LilyPond"

<%= imageref("screenshot-lilypond", {:width => "560", :height => "359"}) %>

**LilyPond** (GPL) ist ein freies Notensatzprogramm für Linux, Windows und MacOS X

Die LilyPond-Entwickler kritisieren, daß selbst in seriösen Musikverlagen erscheinende Noten seit der Einführung des Computernotensatzes nicht mehr die Qualitätsstandards und das ästhetische Niveau handwerklich gesetzter Noten erreichen würden. Sie haben es sich deshalb zum Ziel gesetzt, mit LilyPond dem traditionellen Notenbild näher zu kommen als dies mit anderer Software möglich ist.

## Besonderheiten

Im Gegensatz zu anderen gängigen Notensatzprogrammen bietet LilyPond keine graphische Benutzeroberfläche zur Eingabe von Musik. Stattdessen erstellt der Benutzer in einem beliebigen Texteditor die Noten und andere Notationselemente in Form einer an [LaTeX](LaTeX) angelehnten Syntax. Die fertiggestellte Textdatei wird vom LilyPond-Programm in verschiedene Dateiformate kompiliert (aktuell werden PDF, PS, PNG und SVG unterstützt), bei Bedarf kann auch eine MIDI-Datei der notierten Musik erzeugt werden.

<%= imageref("lilypond-logo-with-music") %>

LilyPond-Noten können auch in ein LaTeX-Dokument integriert werden.

## LilyPond in diesem Wiki

  * Tutorial: [Leadsheets mit LilyPond](Leadsheets mit LilyPond)
  * Tutorial 2: [Wiederholungen und deutsche Akkordbezeichnungen](Wiederholungen und deutsche Akkordbezeichnungen)
  * Tutorial 3: [Striche und Balken](Striche und Balken)

## Weitere (freie) Notensatz-Programme in diesem Wiki

  * [MuseScore](MuseScore)

## Tools

  * [LilyPond Engines for TeXShop](http://users.dimi.uniud.it/~nicola.vitacolonna/home/content/lilypond-engines-texshop) -- damit kann man LilyPond von [TeXShop](TeXShop) aus aufrufen.
  * [LilyPond in TextWrangler](http://users.dimi.uniud.it/~nicola.vitacolonna/home/content/lilypond-textwrangler) benötigt die obigen *LilyPond Engines for TeXShop* (ruft im Hintergrund TeXShop auf), dann aber kann man alle nötigen Arbeiten aus [TextWrangler](TextWrangler) heraus erledigen.
  * [LilyPondTool](http://lilypondtool.blogspot.de/) ist eine LilyPond-Erweiterung für [jEdit](jEdit).
  * [Frescobaldi](http://www.frescobaldi.org/) ist ein leichter, aber sehr mächtiger LilyPond-Musik- und -Texteditor mit eingebauter PDF-Vorschau. Er ist einfach zu benutzen und läuft auf allen gängigen Betriebssystemen (GNU/Linux, MacOS X und Windows).

## Literatur und Tutorials

  * Urs Liska: *[Music Functions 1. Getting to Grips with Scheme in LilyPond](http://lilypondblog.org/2014/03/music-functions-1-getting-to-grips-with-scheme-in-lilypond/)* Scores of Beauty, 28. März 2014

## Links

  * [LilyPond Homepage](http://lilypond.org/)
  * [LilyPond Dokumentation](http://lilypond.org/manuals.html)
  * [Scores of Beauty](http://lilypondblog.org/) ist das »offizielle« LilyPond-Blog
  * <%= wpde("LilyPond") %> in der Wikipedia
  * Essay: [Was steckt hinter LilyPond](http://lilypond.org/web/about/automated-engraving/big-page.de.html) – über die Motivation der Entwickler, ein Programm wie LilyPond zu schreiben.
