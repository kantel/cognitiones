#title "Outliner"

Ein **Outliner** (dt. manchmal *»Gliederungseditor«*) ist ein Computerprogramm, das irgendwo zwischen Gliederung, Textverarbeitung und Zettelkasten steht. Outliner erlauben das Verschieben einzelner Gliederungen (und ihrer Unterpunkte) mit der Maus und das »Wegklappen« einzelner Punkte.

Der (kommerzielle) Outliner *More* war Ende der 1980er Jahre der Hauptgrund für mich, einen Macintosh-Computer zu kaufen. Denn weder für den *Atari ST* noch für den (IBM-) PC gab es seinerzeit ein vergleichbares Programm.

## In diesem Wiki

  * [Cactus](Cactus)
  * [Concord](Concord)
  * [(Electric) Drummer](Drummer)
  * [Fargo](Fargo)
  * [Frontier](Frontier)
  * [OPML Editor](OPML Editor)
  * [OPML](OPML)
  * [Emacs Org-mode](orgmode)
  * [WorkFlowy](WorkFlowy)

## Links

  * [Eine Übersicht über Outliner-Software](http://www.heise.de/software/download/o0g0s3l11k32) auf heise.de
  * [Gliederungseditor](http://de.wikipedia.org/wiki/Gliederungseditor) in der deutschsprachigen Wikipedia
  * [Outliner](http://en.wikipedia.org/wiki/Outliner) in der englischsprachigen Wikipedia (mit einer Liste von Outliner-Programmen)
  
