#title "Mailsmith"

<%= imageref("mailsmith") %>

**Mailsmith** (frei wie Freibier) ist optisch ein eher spartanischer Mail-Client, unter dessen Haube jedoch die gleiche, leistungsfähige Text-Engine wie auch bei [BBEdit](BBEdit) (kommerziell) oder [TextWrangler](TextWrangler) (frei wie Freibier) werkelt. Außerdem ist der Client sehr weit konfigurierbar und skriptfähig. Mailsmith war daher lange Jahre der Mail-Client meiner Wahl, bis ich aus Bequemlichkeit zu einem Web-Client ([Google Mail](Google Mail)) wechselte. Im Zuge der Diskussion über mitlesenden Geheimdienste überlege ich allerdings, wieder zurück zu Mailsmith zu wechseln.

Mailsmith wurde ursprünglich von *Bare Bones Software*, den Entwickerln von BBEdit und TextWrangler entwickelt und (kommerziell) vertrieben, dann aber von *Stickshift Software* übernommen.

## Links

  * [Mailsmith Home](http://www.mailsmith.org/)