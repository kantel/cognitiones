#title "Apache Solr"

<%= imageref("solr") %>

**Apache Solr** ist eine freie (Apache-Lizenz) Suchmaschinenplattform, die auf [Lucene](Lucene) basiert. Solr ist in [Java](Java) geschrieben und läuft als Servlet innerhalb einer [Tomcat](Tomcat)-Umgebung.

## Links

  * [Solr Home](http://lucene.apache.org/solr/)
  * <%= wp("Apache Solr") %> in der Wikipedia
  * heise developer news: [Die Neuerungen von Apache Solr 4.0](http://www.heise.de/developer/artikel/Die-Neuerungen-von-Apache-Solr-4-0-1650785.html)

