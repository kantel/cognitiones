#title "Drupal"

**Drupal** (GPL) ist ein Web-Content-Management-System ([CMS](CMS)) und Framework, das ursprünglich von dem belgischen Informatiker *Dr. Dries Buytaert* (* 19. November 1978) konzipiert wurde und inzwischen in den unterschiedlichsten Anwendungsbereichen weltweit zum Einsatz kommt.

<%= imageref("drupal") %>

Drupal ist in [PHP](PHP) geschrieben und verwendet [MySQL](MySQL) oder [PostgreSQL](PostgreSQL) als Datenbank (Oracle in Entwicklung).

## Drupal 7

<iframe src="http://player.vimeo.com/video/18352872?title=0&amp;byline=0&amp;portrait=0" width="480" height="270" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>

Anfang Januar 2011 ist das langerwartete [Drupal 7](http://drupal.org/drupal-7.0) erschienen. Es soll flexibler und einfacher zu nutzen sein. Die Template-Engine wurde komplett überarbeitet und die Skalierbarkeit verbessert.

## Using Drupal

<iframe width="480" height="296" src="http://www.youtube.com/embed/AZ04xTyx6yo" frameborder="0" allowfullscreen></iframe>

## Drupal Hosting

  * [DrupalCONCEPT](http://www.drupalconcept.de/) bietet – nach eigenen Angaben – Drupal-Hosting ohne Sorgen.

## Literatur

  * Olav Schettler, Friedrich Stahl: *[Praxiswissen Drupal 6](Praxiswissen Drupal 6)*, Köln (O'Reilly) 2008
  * Angela Byron, Addison Berry, Nathan Haug, Jeff Eaton, James Walker und Jeff Robbins: *[Using Drupal](Using Drupal)*, Sebastopol, CA (O'Reilly) 2008

## Links

  * [Drupal Home](http://drupal.org/)
  * [Deutsche Drupal Website](http://www.drupal.de/)
  * <%= wpde("Drupal") %> in der Wikipedia
  * Kurt Cagle: [Getting Started with Drupal](http://broadcast.oreilly.com/2008/11/getting-started-with-drupal.html)
  * [The Ultimate Drupal Tutorial Collection](http://mogdesign.eu/blog/ultimate-drupal-tutorial-collection/)
  * [The Ultimate Drupal Toolbox](http://www.noupe.com/drupal/the-ultimate-drupal-toolbox.html): 150+ Themes, Plug-Ins, and More
  * [200+ Massive Free Drupal Theme Compilation](http://www.tripwiremagazine.com/design/templates/200-massive-free-drupal-theme-compilation.html)
  * [Getting Started with Drupal: A Comprehensive Hands-On Guide](http://sixrevisions.com/web-development/getting-started-with-drupal-a-comprehensive-hands-on-guide/)
  