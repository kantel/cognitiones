#title "Radio UserLand"

<%= imageref("radiouserland-logo") %>

**Radio UserLand** (kommerziell) war ein *spin off* von [Manila](Manila) und basierte wie dieses auf dem UserLand-[Framework](Frameworks) [Frontier](Frontier) und eine weitere Entwicklung [Dave Winers](Dave Winer), der ob der Überlastung seines Manila-Servers *EditThisPage* nach einer Weblog-Lösung suchte, die [statische Seiten](Statische Seiten) herausschrieb. Die Software war nicht nur ein Weblog-Tool, sondern auch ein [RSS](RSS)-Aggregator, ein [Outliner](Outliner) und (mit User Talk) eine Skriptsprache. [Mein Weblog](http://www.schockwellenreiter.de/) lief etliche Jahre mit Radio UserLand.

[[dr]]<%= imageref("radiobox") %>[[dd]]

Die Software erschien 2000 erst unter dem Namen *Spike*, dann entwickelte Winer das Tool zu einer [Podcast](Podcasting)-fähigen Software weiter und nannte es *Radio* UserLand. 2009 schloß UserLand seine Pforten und die Software wurde nicht weiter vertrieben. Mittlerweile gibt es Bestrebungen, sie im freien [OPML Editor](OPML Editor) unter dem Namen [Radio2](Radio2) wiederzubeleben -- bisher allerdings noch nicht wirklich vom Erfolg gekrönt.

## Links

  <%= wp("Radio UserLand") %> in der englischsprachigen Wikipedia.

<br />

<%= imageref("radioretro") %>

