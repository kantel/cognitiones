#title "Alfresco"

<%= imageref("alfresco") %>

**Alfresco** ist eine freie (LGPL) Software zur Dokumentenverwaltung elektronisch gespeicherter Dokumente. Alfresco ist in [Java](Java) geschrieben und sollte daher überall dort laufen, wo eine [JVM](JVM) installiert ist.

## Links

  * [Alfresco Home](http://www.alfresco.com/de/)
  * [Alfresco Anwender- und Entwicklerforum](http://forums.alfresco.com/de/)
  * [Alfresco Documentation](http://www.alfresco.com/resources/documentation)
  * <%= wpde("Alfresco (Software)") %> in der Wikipedia
