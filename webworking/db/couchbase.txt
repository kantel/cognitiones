#title "Couchbase Server"

<%= imageref("couchbaselogo") %>

**Couchbase Server** (Apache-Lizenz) hieß früher **Membase** und ist wohl so etwas wie der Versuch, [CouchDB](CouchDB) statt in [Erlang](Erlang) zumindest teilweise in [C++](C++) nachzuprogrammieren. Ob es auch eine *Fork* von CouchDB ist, ist mir zur Zeit noch nicht klar. Das Buch »<a href="http://www.amazon.de/gp/product/1449307523/ref=as_li_ss_tl?ie=UTF8&camp=1638&creative=19454&creativeASIN=1449307523&linkCode=as2&tag=derschockwell-21">Getting Started with Geo, CouchDB, and Node.Js</a><img src="http://ir-de.amazon-adsystem.com/e/ir?t=derschockwell-21&l=as2&o=3&a=1449307523" width="1" height="1" border="0" alt="" style="border:none !important; margin:0px !important;" />« setzt jedenfalls, trotz seines Titels, auf Couchbase statt auf CouchDB.

Die Software läuft unter Windows, Linux und MacOS X.

## Video-Tutorial

<iframe width="560" height="315" src="//www.youtube.com/embed/S7Io_FUXT8c?rel=0" frameborder="0" allowfullscreen></iframe>

Couchbase Server 2.0, Getting Started

## Literatur

[[dr]]<iframe src="http://rcm-eu.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=derschockwell-21&o=3&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=1449307523" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe>&nbsp;<iframe src="http://rcm-eu.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=derschockwell-21&o=3&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=1449331165" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe>&nbsp;<iframe src="http://rcm-eu.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=derschockwell-21&o=3&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=1449331068" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe>[[dd]]

  * Golem.de: [Couchbase Server statt Apache CouchDB](http://www.golem.de/1201/88815.html), Artikel vom 5. Januar 2012

## Links

  * [Couchbase Home](http://www.couchbase.com/)
  * [Couchbase @ GitHub](https://github.com/couchbase)
  * <%= wp("Couchbase Server") %> in der Wikipedia
