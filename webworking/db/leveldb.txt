#title "LevelDB"

**LevelDB** ist ein freier (BSD-Lizenz) *Key-Value*-Datenspeicher, der von Google entwickelt und fregegeben wurde. Die Software läuft unter Linux, MacOS X, Windows und [Android](Android). Facebooks [RocksDB](RocksDB) setzt auf LevelDB auf.

## Links

  * [LevelDB @ Google Code](https://code.google.com/p/leveldb/)
  * <%= wpde("LevelDB") %> in der Wikipedia