#title "PouchDB"

**PouchDB** ist eine freie (Apache-Lizenz) dokumentenorientierte Datenbank, die komplett in [JavaScript](JavaScript) geschrieben wurde. Sie ist nicht nur von der [CouchDB](CouchDB) inspiriert und kompatibel, sondern sie kann sich auch mit einer CouchDB-Instanz im Web synchronisieren.

PouchDB ist klein, gerade einmal 65 KB fett und kann sowohl im Browser als auch auf dem Server via [Node.js](nodejs) laufen. Das heißt, man kann offline im Browser mit der DB arbeiten und sobald man wieder online ist, synct sich die Datenbank mit ihrer Serverinstanz und bringt auch alle anderen angeschlossenen Clients auf den aktuellen Stand.

Die Software für den Browser ist momentan noch im Beta-Stadium, ein stabiles Release soll aber kurz vor der Fertigstellung stehen. Die Serverversion für Node.js ist momentan noch Alpha.

## Links

  * [PouchDB Home](http://pouchdb.com/)
  * [PouchDB @ GitHub](https://github.com/daleharvey/pouchdb)
  * [PouchDB Getting Started Guide](http://pouchdb.com/getting-started.html)
  * [PouchDB API Reference](http://pouchdb.com/api.html)
  * [PouchDB Wiki](https://github.com/daleharvey/pouchdb/wiki)

