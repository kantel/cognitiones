#title "Handlebars"

**Handlebars** ist ein semantisches, in [JavaScript](JavaScript) geschriebenes Template-System und ein Superset von [Mustache](Mustache). Das heißt, Handlebars kann auch Mustache-Templates herausrendern. Während Mustache logikfrei ist, gibt es in Handlebars wieder Kontrollelemente wie `#if`, `#unless`, `#width`, und `#each`.

## Links

  * [Handlebars Home](http://handlebarsjs.com/)
  * [Handelbars.js @ GitHub](https://github.com/wycats/handlebars.js/)