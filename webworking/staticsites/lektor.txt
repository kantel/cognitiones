#title "Lektor"

<iframe width="560" height="315" src="https://www.youtube.com/embed/lTWTCwuPdrU?rel=0" frameborder="0" allowfullscreen></iframe>

**Lektor** ist ein freies (BSD-Lizent) *Content Management System* ([CMS](CMS)) für [statische Seiten](Statische Seiten). Es ist in [Python](Python) geschrieben und besitzt (vorert nur für MacOS X) einen Desktop-Client, der mit [Node.JS](nodejs) realisiert wurde.

Auf Betriebssystemen, für die es den Desktop-Client (noch) nicht gibt, kann Lektor als Komanndozeilenwerkzeug genutzt werden.

Lektor benötigt keine Datenbank. Die Software liest Textdateien ein und schreibt statische HTML-Seiten heraus. Die Quelldateien können sowohl in einem Versionskontrollsystem oder in der Dropbox liegen, aber auch einfach in einem Ordner auf der heimischen Festplatte.

## Literatur

  * Armin Ronacher: *[Introducing Lektor — A Static File Content Management System For Python](http://lucumr.pocoo.org/2015/12/21/introducing-lektor/)* vom 21. Dezember 2015 (Armin Ronacher ist der Schöpfer von Lektor)

## Links

  * [Lektor Home](https://www.getlektor.com)
  * [Lektor Dokumentation](https://www.getlektor.com/docs/)
  * [Lektor's Blog](https://www.getlektor.com/blog/)
  * [Lektor @ GitHub](https://github.com/lektor/lektor/)