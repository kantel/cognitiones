#title "RubyFrontier"

<%= imageref("rubyfrontier1st", {:border => "0", :width => "480", :height => "467", :alt => "RubyFrontier Screenshot"}) %>

**RubyFrontier** (MIT-Lizenz) ist ein Versuch von [Matt Neuburg](Matt Neuburg), das *[Frontier](Frontier) Static Site Framework* nach [Ruby](Ruby) zu klonen. Es läuft als ein [TextMate 2](TextMate)-Tool und benötigt MacOS X >= 10.5.

Es erinnert an wenig an [meine Experimente](http://www.schockwellenreiter.de/2003/11/11.html#198) mit [Python](Python) und [BBEdit](BBEdit)/[TextWrangler](TextWrangler), die ich schon vor einigen Jahren durchgeführt hatte.

## Schockwellenreiter

Seit etlichen Jahren wird auch mein Blog [Schockwellenreiter](http://blog.schockwellenreiter.de/) komplett mit RubyFrontier erstellt und ist so etwas wie meine Referenz und meine Experimentier-Plattform.

## Testbett

Mein [Testbett](http://www.testbett.de/) war die erste Website, die ich komplett mit RubyFrontier erstellt hatte und lange (m)eine *learning by doing*-Plattform für RubyFrontier und andere Web-Dinge.

## Andere Sites, die ich mit RubyFrontier erstellt habe

Was aus meinen [Webnotizen](http://jweb.kantel-chaos-team.de/) einmal werden soll, ist mir selber noch nicht ganz klar. Erst einmal sind auch sie nur eine Fingerübung für RuybFrontier.

Natürlich wird auch [dieses Wiki](Startseite) komplett mit RubyFrontier erstellt.

## RubyFrontier-Buch

Ich arbeite an einem [RubyFrontier-Buch](http://rubyfrontier.kantel-chaos-team.de), einem deutschsprachigen Tutorial für RubyFrontier.

(Fast) alle meine [statischen Seiten](Statische Seiten) und *alle* mit RubyFrontier erstellten Seiten -- auch dieses Wiki -- liegen zur Zeit bei [Amazon S3](Amazon S3). Ein [Tutorial dazu](http://rubyfrontier.kantel-chaos-team.de/s3.html), gibt es im RubyFrontier-Buch.

## RubyFrontier-Fehler ...

können manchmal dadurch behoben werden, indem man unter `~/Library/Caches` den Ordner `com.macromates.textmate` in die Tonne tritt. Merken!

## Links

  * [RubyFrontier-Dokumentation](http://www.apeth.com/RubyFrontierDocs/default.html)
  * <del>[Download bei SourceForge.net](http://sourceforge.net/projects/rubyfrontier/)</del>
  * [Download bei GitHub](https://github.com/mattneub/RubyFrontier)
  * [Homepage Matt Neuburg](http://www.tidbits.com/matt/)

