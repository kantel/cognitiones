#title "Quarto"

<%= imageref("quarto") %>

**Quarto** ist ein wissenschaftlich-technisches *Open-Source*-Veröffentlichungssystem, das auf [Pandoc](Pandoc) aufbaut. Es erleichtert die nahtlose Integration von Text und Code in Publikationsqualität.

## Literatur 

- Lloyd Hamilton: *[Markdown just got a facelift. Hello Quarto](https://medium.com/@lloyd.hamilton/markdown-just-got-a-facelift-hello-quarto-742ce406228b)*, Medium.com vom 30. Mai 2022  
- Charles Teague: *[Positioning Content in the Margin](https://quarto.org/docs/blog/posts/2022-02-17-advanced-layout/). Create ‘Tufte’ style documents with sidenotes, margin tables and figures, and other margin content*, Quarto Blog vom 17. Februar 2022  
- Tony Hirst: *[A Quick Look at the Quarto Pandoc Publishing System and Visual Markdown Editor](https://blog.ouseful.info/2021/09/07/a-quick-look-at-the-quarto-pandoc-publishing-system-and-visual-markdown-editor/)*, OUseful.Info vom 7. September 2021  

## Videos  

<iframe width="560" height="315" src="https://www.youtube.com/embed/y5VcxMOnj3M" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Tutorials 

### Auf Quartos Webseiten  

- [Quarto Tutorials](https://quarto.org/docs/get-started/)  
- [Quarto Guide](https://quarto.org/docs/guide/)  
- [Quarto Reference](https://quarto.org/docs/reference/)  
- [Quarto Gallery](https://quarto.org/docs/gallery/)  

## Links  

- [Quarto Home](https://quarto.org/)  
- [Quarto @ GitHub](https://github.com/quarto-dev/quarto-cli)  
- [Quarto Blog](https://quarto.org/docs/blog/)  