#title "Selfoss"

<%= imageref("selfoss02", {:width => "560", :height => "405"}) %>

**Selfoss** (GPL) ist nicht nur ein selbstgehosteter Feedreader, sondern darüber hinaus auch ein Mashup-Werkzeug. Es benötgigt einen Webserver, [PHP](PHP) und [SQLite](SQLite)/[MySQL](MySQL).

Selfoss läuft auch unter [XAMPP](XAMPP) (darunter wurde es entwickelt) wie auch unter [MAMP](MAMP). Daher steht einem Einsatz der Software als *Webserver auf dem Desktop* nichts im Wege.

## Selfoss im Schockwellenreiter

Selfoss läßt sich unter MAMP auf [MacOS X](MacOS X)-Maschinen auch als Service auf dem Desktop betreiben. Wie das geht, habe ich hier beschrieben: 

  * Schockwellenreiter: [Worknote: Selfoss und MAMP, Teil 1](http://blog.schockwellenreiter.de/essays/selfoss01.html)
  * Schockwellenreiter: [Worknote: Selfoss und MAMP, Teil 2](http://blog.schockwellenreiter.de/essays/selfoss02.html)

## Links

  * [Selfoss Home](http://selfoss.aditu.de/)
  * [Selfoss @ Github](https://github.com/SSilence/selfoss)
  * [Selfoss Demo](http://stuporglue.org/selfoss/)
  * [Blog des Entwicklers](http://blog.aditu.de/)

