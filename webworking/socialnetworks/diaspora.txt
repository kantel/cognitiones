#title "Diaspora"

<iframe src="http://player.vimeo.com/video/11099292?title=0&amp;byline=0&amp;portrait=0" width="480" height="270" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>

Mit meinem Wunsch nach einem [P2P-Social-Network](http://www.schockwellenreiter.de/blog/2010/05/09/offentlichkeit-und-privatheit/) stehe ich nicht alleine da: **Diaspora** aus New York will mit einem dezentralen [Social Network](Social Network Engines) Betreibern wie *Facebook* & Co [den Rang ablaufen](http://netzwertig.com/2010/05/10/diaspora-mit-einem-dezentralen-netzwerk-gegen-facebook-co/): Statt die Kommunikation über zentrale Server eines gewinnorientierten Unternehmen abzuwickeln, betreiben Nutzer ihren eigenen Diaspora-»Seed«, der ihre persönlichen Daten enthält, und über den sie die volle Kontrolle haben. Dieser Seed, der entweder über einen eigenen Server läuft oder extern gehostet werden kann, aggregiert sämtliche Informationen von sozialen Netzwerken und läßt sich mit anderen Seeds (= Usern) verbinden, um beliebige Inhalte auszutauschen. Die Kommunikation wird dabei verschlüsselt.

## Kritik

Im Februar 2012 kritisierte die c't, das ursprüngliche Versprechen, ein Peer-to-Peer-Netzwerk zu bieten, sei bisher von Diaspora nicht eingelöst worden, weil die Installation für den eigenen PC oder für den eigenen Webhoster für die meisten Anwender zu umfangreich und zu kompliziert seien. Auf einem [Linux](Linux)- oder [MacOS X](MacOS X)-Server müßten [Ruby](Ruby), [SQLite3](SQLite), OpenSSL, libcurl, [ImageMagick](ImageMagick), [Git](Git) und Redis eingerichtet werden, bevor man Diaspora installieren könne. Deshalb seien die meisten Benutzer von Diaspora weiterhin darauf angewiesen, daß der Pod, der ihren Account führt, mit ihren Daten vertrauensvoll umgeht. Die Alternative [Friendica](Friendica) sei in Bezug auf einfache Installation schon weiter als Diaspora.

## Links

  * [Diaspora Homepage](http://www.joindiaspora.com/)
  * [Mein Diaspora-Account](https://joindiaspora.com/people/4cf1056b2c174368ad00739b) (pre-alpha)
  * Golem.de: [Diaspora: Auf dem Weg zu einem sicheren und freien sozialen Netzwerk](http://www.golem.de/1005/75092.html)
  * Blogrebellen Kreuzberg: [Diaspora -- ein erster Eindruck](http://blog.rebellen.info/2010/09/17/diaspora-ein-erster-eindruck/)
  * Stefan Münz: [Erste Erfahrungen mit Diaspora](http://webkompetenz.wikidot.com/blog:89)
