#title "Friendica"

<%= imageref("friendica-logo") %>

**Friendica** (ehemals Friendika) ist freie Software für ein verteiltes soziales Netzwerk. Der Fokus liegt auf wirkungsvollen Datenschutzeinstellungen und leichter Installation auf eigenen Servern. Zudem lassen sich andere soziale Netzwerke und Blogs leicht integrieren. Im Juni 2012 hatte Friendica etwa 5.500 Benutzer

## Installation

Das Ziel der Entwickler ist es, die Installation der Server-Software auch für technische Laien einfach zu machen. Sie argumentieren, daß Dezentralisierung auf kleinen Servern eine Schlüsselbedingung für die Freiheit der Anwender und für ihre datentechnische Selbstbestimmung darstellt. Friendica kann auf einem eigenen Server installiert werden, die Schwierigkeitsstufe ähnelt dann einer Installation von WordPress. Zudem betreiben Freiwillige auch öffentliche Server, so daß Einsteiger die Installation eigener Software auch ganz vermeiden können.

## Links

  * [Friendica Home](http://friendica.com/)
  * Jonke Suhr: *[Friendica im Test](http://d3j.de/blog/post/friendica-im-test/)*, d3j-Blog vom 7. Mai 2012
  * <%= wpde("Friendica") %> in der Wikipedia
