#title "Parallel Flickr"

Der ehemalige [Flickr](Flickr)-Mitarbeiter *Aaron Straup* hat auf Github **Parallel Flickr** veröffentlicht. Damit kann man sich, wie der Name andeutet, nicht nur alle seine Photos und Metadaten von Flickr auf den eigenen Server laden, sondern auch die ganzen Privatsphäre-Einstellungen (wer welche Bilder sehen kann) nachbilden, also wirklich ein paralleles Flickr betreiben.

## Notiz an mich

Die [Screenshots](http://www.flickr.com/photos/straup/tags/parallelflickr/) regen mich an, meine eigene Arbeit an einem (statischen) Flickr-Klon (genannt *Photr*) wieder aufzunehmen.

## Links

  * [Parallel Flickr auf Github](https://github.com/straup/parallel-flickr)
  * [Aaron Staups Blogpost-Eintrag](http://www.aaronland.info/weblog/2011/10/14/pixelspace/#parallel-flickr) zu Parallel Flickr.