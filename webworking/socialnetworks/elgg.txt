#title "Elgg"

<iframe width="480" height="296" src="http://www.youtube.com/embed/rPkSOFC13_g" frameborder="0" allowfullscreen></iframe>

**Elgg** (GPL und MIT-Lizenz) ist eine [Social Network Engine](Social Network Engines) für das Internet (ähnlich *Facebook*) und das Intranet (ähnlich *Sharepoint*).

## Voraussetzungen

Elgg läuft hinter einem Apache (mit mod_rewrite), benötigt [PHP](PHP) (> 5.2) und [MySQL](MySQL) (> 5). Statt des Apaches kann aber auch ein anderer Server eingesetzt werden, wie z.B. *lighttpd* oder *nginx*.

Mit diesen Voraussetzungen sollte Elgg als Testinstallation auch hinter [MAMP](MAMP) laufen. (Testen!)

## Ähnliche Software

  * [Diaspora](Diaspora)

## Links

  * [Elgg Home](http://elgg.org/)
  * <%= wp("Elgg (software)") %> in der Wikipedia
  * [Elgg Tutorials](http://www.phpqa.in/search/label/elgg)
  * [Elgg: Social Networking Platform als Open Source Software](http://www.workshop.ch/openmind/2008/04/04/elgg-social-networking-platform-als-open-source-software/)
  * [Elgg Plugin](http://community.elgg.org/pg/plugins/project/504052/developer/caedes/dokuwiki-for-groups) für [DokuWiki](DokuWiki)