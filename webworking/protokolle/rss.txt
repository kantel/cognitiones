#title "RSS"

<iframe src="http://rcm-de.amazon.de/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=derschockwell-21&o=3&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=3897215276" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe>

## Tools

  * [ListGarden](http://www.softwaregarden.com/products/listgarden/) ist ein in [Perl](Perl) geschriebener RSS-Feed-Generator, [speziell für statische Seiten](http://paulhutch.com/wordpress/?p=767).

## Links

  * [RSS Refugee](http://reader.smallpicture.com/?opmlurl=https%3A%2F%2Fdl.dropbox.com%2Fs%2F48pqmqq7mpg8l3h%2FrssRefugee.opml) - A comprehensive list of Google Reader alternatives
