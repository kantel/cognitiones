#title "YAML"
#breadcrumbs "<a href=\"Startseite\">Start</a> → <a href=\"Webworking\">Webworking</a> → <a href=\"Web Protokolle\">Protokolle</a> → <a href=\"Auszeichnungssprachen\">Auszeichnungssprache</a> → "

**YAML** ist eine vereinfachte [Auszeichnungssprache](Auszeichnungssprachen) zur Datenserialisierung. YAML ist ein rekursives Akronym für *»YAML Ain't Markup Language«* (ursprünglich *»Yet Another Markup Language«*).

## Links ##

  * Offizielle Website [YAML.org](http://www.yaml.org/) mit der [YAML-Spezifikation](http://www.yaml.org/spec/)
  * [YAML-Kochbuch für Ruby](http://www.nt.ntnu.no/users/haugwarb/Programming/YAML/YAML_for_ruby.html)
  * [YAML](http://de.wikipedia.org/wiki/YAML) in der Wikipedia
