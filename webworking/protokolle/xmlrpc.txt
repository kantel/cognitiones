#title "XML-RPC"

**XML-RPC** (Extensible Markup Language Remote Procedure Call) ist eine Definition zum Methodenaufruf (oder auch Funktionsaufruf) durch verteilte Systeme. Bei der Spezifikation wurde Wert auf grötmöglichste Einfachheit gelegt.

## Links ##

  * [XML-RPC Homepage](http://xmlrpc.scripting.com/) und [Spezifikation](http://xmlrpc.scripting.com/spec)
  * [XML-RPC](http://de.wikipedia.org/wiki/XML-RPC) in der Wikipedia

