#title "Hadoop"

<%= imageref("hadoop-logo") %>

**Hadoop** ist ein freies (Apache-Lizenz), in [Java](Java) geschriebenes Framework für skalierbare, verteilt arbeitende Software. Es basiert auf dem bekannten [MapReduce](MapReduce)-Algorithmus von [Google Inc.](Google) sowie auf Vorschlägen des Google-Dateisystems und ermöglicht es, intensive Rechenprozesse mit großen Datenmengen (Big Data, Petabyte-Bereich) auf Computerclustern durchzuführen.

## Literatur

<div style="float:right; margin-left: 8px;"><iframe src="http://rcm-de.amazon.de/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=derschockwell-21&o=3&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=1935182196" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe>&nbsp;<iframe src="http://rcm-de.amazon.de/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=derschockwell-21&o=3&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=1449311520" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe></div>

  * Oliver Fischer: *[Verarbeiten großer verteilter Datenmengen mit Hadoop, Teil 1](http://www.heise.de/developer/artikel/Verarbeiten-grosser-verteilter-Datenmengen-mit-Hadoop-964755.html)*, heise developer news, 1. April 2010
  * Oliver Fischer: *[Verarbeiten großer verteilter Datenmengen mit Hadoop, Teil 2](http://www.heise.de/developer/artikel/Verarbeiten-grosser-verteilter-Datenmengen-mit-Hadoop-ein-Beispiel-1001492.html)*, heise developer news, 18. Mai 2010

## Links

  * [Hadoop Home](http://hadoop.apache.org/)
  * <%= wpde("Hadoop") %> in der Wikipedia