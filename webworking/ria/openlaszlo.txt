#title "OpenLaszlo"

<%= imageref("openlaszlo-screenshot") %>

**OpenLaszl**o (CPL) ist eine Open-Source-Plattform zur Erstellung von [Rich Internet Applications](Rich Internet Applications). Die Plattform ist serverbasiert und verlangt einen [Tomcat](Tomcat) auf dem Entwicklungsrechner (in den Binaries für [MacOS X](MacOS X) schon im Installationspaket enthalten).

Die Ausgabe der fertigen Applikation erfolgt als [Flash](Flash) oder DHTML. Weitere Ausgabeformate ([Java](Java), [JavaME](JavaME) oder [SVG](SVG)) sind in Planung.

## Links

  * [OpenLaszlo Homepage](http://www.openlaszlo.org/)
  * [OpenLaszlo](http://de.wikipedia.org/wiki/OpenLaszlo) in der Wikipedia
  
