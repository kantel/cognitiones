#title "Telegram Messenger"

<%= imageref("telegram") %>

**Telegram Messenger** ist ein kostenloser Instant-Messaging-Dienst zur Nutzung auf Smartphones, Tablets und Dekstop-Computer. Benutzer von Telegram können verschlüsselte Nachrichten, Photos, Videos und Dokumente austauschen. Die Telegram-Anwendungssoftware ist teilweise quelloffen (meist unter der GPL) und für die Betriebssysteme Android, iOS, Windows Phone 7.8 und 8.1, Firefox OS, Sailfish OS, Ubuntu Touch, Windows, Linux, MacOS X sowie als Webanwendung verfügbar. Die serverseitige Infrastruktur Telegrams hingegen ist proprietär.

Telegram wurde 2013 von den Brüdern Nikolai und Pawel Durow gegründet, die bereits das meistgenutzte russische soziale Netzwerk *Vk.com* gegründet hatten. Telegram Messenger LLP ist ein unabhängiges Non-Profit-Unternehmen mit Sitz in Berlin, das nicht im Zusammenhang mit *Vk.com* oder Russland steht.

Der Dienst steht in Konkurrenz zu anderen mobilen Instant-Messengern wie WhatsApp, Threema, TextSecure, KakaoTalk, oder WeChat. Infolge der WhatsApp-Übernahme durch [Facebook](Facebook) etablierte sich Telegram zudem auch als eine der erfolgreichsten kostenlosen Instant-Messenger-Alternativen für alle gängigen Plattformen.

Ein Vorzug von Telegram gegenüber den meisten Konkurrenten ist die -- zumindest clientseitig -- nichtproprietäre Lizenzierung unter GNU GPL, d. h. dadurch wird Dritten ohne rechtliche und technische Hürden ermöglicht, mittels einer offenen API voll funktionstüchtige eigene Telegram-Clientanwendungen für jede erdenkliche Plattform zu programmieren.

## Links

  * [Telegram Messenger Home](https://telegram.org/)
  * [Telegram APIs](https://core.telegram.org/api)
  * <%= wpde("Telegram Messenger") %> in der Wikipedia