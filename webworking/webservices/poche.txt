#title "Poche"

<%= imageref("poche") %>

**Poche** (sprich [pɔʃ]), französisch für »Hosentasche«, ist eine Webapp, mit der man Webseiten zum späteren Lesen speichern kann. Im Gegensatz zu vergleichbaren anderen Diensten ist Poche kostenlos und quelloffen (WTFPL). Man kann Poche sowohl selber auf eigenem Webspace hosten (allerdings ist die Installation wegen diverser Abhängigkeiten nicht gerade trivial) als auch kostenlos auf den Seiten der Machern mitmachen.

## Poche is Dead, Welcome Wallabag

Nach [Rechtsstreitigkeiten](http://www.wallabag.org/2014/01/27/lets-go/) mit der Firma, die *Pocket* (eine kommerzielle »Read Later« App) betreibt, mußte *Poche* seinen Namen ändern und heißt jetzt [Wallabag](Wallabag).

## Links

  * [Poche Home](http://www.inthepoche.com/)
  * [Poche Download](http://www.inthepoche.com/downloads-de/)
  * [Poche Hoster](http://app.inthepoche.com/) -- kostenlos
  * [Poche @ GitHub](https://github.com/inthepoche/poche)
  * [Poche Wiki](http://doc.inthepoche.com/doku.php?id=start)
