#title "OpenShift"

<%= imageref("openshift-logo") %>

**OpenShift** ist das *Platform as a Service* (PaaS) Produkt auf der Basis und von Red Hat. Es gibt eine kostenlose *Public PaaS* (mit bis zu drei *»Gears«*, öffentlich für alle), eine kommerzielle Enterprise Edition *(Private PaaS)* und auch ein OpenShift zum Selberhosten *(OpenShift Origin, Community PaaS)*.

Die Plattform unterstützt [Java](Java), [PHP](PHP), [Ruby](Ruby), [Node.js](nodejs), [Python](Python), [Perl](Perl), [MySQL](MySQL), [MongoDB](MongoDB), [PostgreSQL](PostgreSQL) und Jenkins.

## Links

  * [OpenShift Home](https://www.openshift.com/)
  * [OpenShift Origin @ GitHub](https://github.com/openshift)
  * <%= wp("OpenShift") %> in der Wikipedia

<%= imageref("redhat") %>
