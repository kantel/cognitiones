#title "Zope-Hoster"

Hier möchte ich Links zu erreich- und bezahlbaren [Zope](Zope)- und [Plone](Plone)-Hostern sammeln. 

  * [Elsewhere.de](http://www.elseware.de/), Berlin
  * [Hostsharing.net](http://www.hostsharing.net/), eine Hamburger Genossenschaft
  * Hier ein Schweizer Zopehoster: [Zopehosting.ch](http://www.zopehosting.ch/)
  * [Objectis](http://www.objectis.org/?set_language=de&cl=de): kostenloser Zope/Plone-Hoster aus Frankreich
  * [opensource-consult](http://www.opensource-consult.de/), Dortmund
  * [ZOPE-Hosting von Syndicat](http://www.syndicat.com/aktuell/zope_hosting/index_ger.html)
  