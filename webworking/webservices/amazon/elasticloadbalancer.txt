#title "Elastic Load Balancer"

**Amazons Elastic Load Balancer** verteilt automatisch eingehenden Anwendungsverkehr über mehrere [EC2-Instanzen](Amazon EC2). Somit kann eine noch höhere Fehlertoleranz erreicht werden: Die Lastverteilungs-Kapazität wird nahtlos an den Anwendungsverkehr angepaßt.

Elastic Load Balancing ermittelt fehlerhafte Instanzen innerhalb eines Pools und leitet den Verkehr automatisch an fehlerfreie Instanzen im Pool weiter, bis der Fehler behoben wurde.

## Links

  * [Elastic Load Balancer Home](http://aws.amazon.com/de/elasticloadbalancing/)


