#title "Lavish"

<%= imageref("lavish", {:width => "604", :height => "274"}) %>

**Lavish** ist ein Color-Scheme-Generator für [Bootstrap](Bootstrap). Einfach ein Photo auswählen und dies dann dort hochladen -- Lavish generiert ein Farbschema daraus und präsentiert auch gleichzeitig den notwendigen CSS- oder LESS-Code. Kann man natürlich auch für andere Zwecke bis hin zu handgestricktem CSS nutzen.

## Links

  * [Lavish Home](http://www.lavishbootstrap.com/)

