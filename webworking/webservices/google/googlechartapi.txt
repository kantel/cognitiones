#title "Google Chart API"

<%= imageref("chart") %>

Mit dem **Google Chart API** kann man auf seinen Webseiten dynamisch Infographiken erzeugen, wie zum Beispiel Linien-, Balken- und Tortendiagramme. Aber auch komliziertere Dinge, wie z.B. Scatter-Plots, Radar-Charts oder auch Karten sind damit möglich.

## In diesem Wiki

  * [Mathematischer Formelsatz mit Google](Mathematischer Formelsatz mit Google)

## Ähnliche Programme

  * [MathJax](MathJax)
