#title "Mathematischer Formelsatz mit Google"

[Da hat jemand herausgefunden](http://moultano.blogspot.com/2009/11/google-can-generate-your-equations-for.html), daß der [mathematische Formelsatz](http://googleblog.blogspot.com/2009/09/back-to-school-with-google-docs.html) in [Googles Texte und Tabellen](docs) eine undokumentierte Funktion der [Google Chart API](Google Chart API) ist. Wenn man sie z.B. wie folgt aufruft, gibt es (Klick!) eine nette Formel zu sehen:

<br /><a href="http://chart.apis.google.com/chart?cht=tx&chf=bg,s,FFFFFF00&chco=000000&chl=i\hbar\frac{\partial}{\partial t} \Psi(\mathbf{r},\,t) = \hat H \Psi(\mathbf{r},t)" target="_blank">http://chart.apis.google.com/chart?cht=tx&chf=bg,s,FFFFFF00&chco=000000&chl=i\hbar\frac{\partial}{\partial t} \Psi(\mathbf{r},\,t) = \hat H \Psi(\mathbf{r},t)</a><br />

Packt man diesen Aufruf in ein Image-Tag bekommt man unten stehendes Bildchen zurückgeliefert, das man — wie die anderen Google-Charts-Anwendungen auch — ohne Umweg in seine Webseite einbinden kann:

<img src="http://chart.apis.google.com/chart?cht=tx&chf=bg,s,FFFFFF00&chco=000000&chl=i\hbar\frac{\partial}{\partial t} \Psi(\mathbf{r},\,t) = \hat H \Psi(\mathbf{r},t)">

Dabei sind `cht=tx` der Chart-Type TeX, hinter `chf` verbirgt sich die Farbe (die Hintergrundfarbe in RGBA, die Vordergrundfarbe in RGB) und `chco` nimmt Eure Formel in [LaTeX](LaTeX)-Notation auf.

Damit erspart man sich alle LaTeX-to-GIF/PNG-Programme

## Links

  * [Google can render your equations for you!](http://moultano.blogspot.de/2009/11/google-can-generate-your-equations-for.html)
  * [Back to School with Google Docs](http://googleblog.blogspot.de/2009/09/back-to-school-with-google-docs.html)
