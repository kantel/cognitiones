#title "Google Cloud Storage"

<%= imageref("googlestorage-logo") %>

**Google Cloud Storage** (hieß früher einmal [Google Storage](Google Storage)) ist ein [RESTful](REST) Speicherservice des Internetriesen, der [Amazons S3](Amazon S3) Konkurrenz machen soll.

## Video

<iframe width="560" height="315" src="//www.youtube.com/embed/cI35BAdKrr4?rel=0" frameborder="0" allowfullscreen></iframe>

Google I/O 2012 - Powering Your Application's Data using Google Cloud Storage

## Links

  * [Google Cloud Storage Home](https://cloud.google.com/products/cloud-storage)
  * [Google Cloud Storage](https://developers.google.com/storage/) auf Google Developer
  * <%= wp("Google Storage") %> in der Wikipedia
  * [Google Storage for Developers](http://code.google.com/intl/de-DE/apis/storage/) auf Google Labs
  * [Getting Started With Google Storage](http://code.google.com/intl/de-DE/apis/storage/docs/getting-started.html)
  * [GSUtil](http://code.google.com/apis/storage/docs/gsutil.html), ein in [Python](Python) geschriebenes *Command Line Tool* für Google Storage
  * [Storage Manager](http://code.google.com/intl/de-DE/apis/storage/docs/gsmanager.html), ein webbasiertes Interface


