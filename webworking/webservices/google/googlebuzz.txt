#title "Google Buzz"

**Google Buzz** ist die im Februar 2010 von Google vorgestellte Version ihres sozialen Netzwerkes. Es kombiniert Elemente von Facebook und Twitter, läuft in Google Mail und soll wohl langfristig daß zumindest in Europa und Nordamerika erfolglose Orkut ablösen.

Google Buzz wurde wegen Erfolglosigkeit zugunsten von [Google+](Google+) eingestellt.

## Links

  * [Google Buzz Home](http://www.google.com/buzz)
  * Google Code Blog: [Introducing the Google Buzz API](http://googlecode.blogspot.de/2010/05/introducing-google-buzz-api.html)