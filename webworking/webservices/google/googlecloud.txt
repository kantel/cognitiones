#title "Google Cloud Platform"

Die **Google Cloud Platform** ist Googles Antwort auf [Amazons Webservices](Amazon Webservices). Unter diesen Namen hat der Internetriese mehrere Dienste zusammengefaßt, unter anderm

  * [Google App Engine](Google App Engine)
  * [Google Compute Engine](Google Compute Engine)
  * [Google Cloud Storage](Google Cloud Storage)
  * [Google Cloud Datastore](Google Cloud Datastore)

## Links

  * [Google Cloud Platform Home](https://cloud.google.com/)
  * [Google Cloud Platform Blog](http://googlecloudplatform.blogspot.de/)
