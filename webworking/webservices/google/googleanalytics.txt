#title "Google Analytics"

Mit **Google Analytics** können Webmaster das Verhalten der Besucher ihrer Webseiten tracken und analysieren. Da der Dienst von Google (auf Servern in den USA) betrieben wird, ist er datenschutzrechtlich umstritten. Daher wird empfohlen, auf die Dienste selbstgehosteter Alternativen wie z.B. [Piwik](Piwik) auszuweichen.

## Links

  * [Google Analytics Home](http://www.google.de/intl/de/analytics/) (deutschsprachig)
  * <%= wpde("Google Analytics") %> in der Wikipedia
  * Datenschutz-Blog: [Google Analytics datenschutzkonform einsetzen](http://www.datenschutzbeauftragter-info.de/fachbeitraege/google-analytics-datenschutzkonform-einsetzen/)

