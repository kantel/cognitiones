#title "O3D"

<iframe width="560" height="315" src="http://www.youtube.com/embed/uofWfXOzX-g?rel=0" frameborder="0" allowfullscreen></iframe>

**Googles O3D** war eine Open Source [JavaScript](JavaScript) API, mit der [3D](3D)-Szenen interaktiv im Browser in Echtzeit gerendert und dargestellt werden können. Das dafür benötigte Plug-In gibt es zur Zeit für Windows und für [MacOS X](MacOS X). O3D wurde von [Google](Google) zugunsten von [WebGL](WebGL) eingestellt.

## Links

  * [O3D API Blog](http://o3d.blogspot.com/)