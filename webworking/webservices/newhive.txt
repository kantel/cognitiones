#title "NewHive"

<iframe src="https://player.vimeo.com/video/77183542?title=0&byline=0&portrait=0" width="560" height="315" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

**NewHive** ist eine freie (frei wie Freibier) *Multimedia Publishing Platform*, die es ermöglichen will, neben (formatierten) Texten Multimedia-Inhalte wie Bilder, Audio-Beiträge, Vektorgraphiken und Videodateien einfach zu Beiträgen zusammenzufügen, zu gruppieren und zu publizieren (ich vermisse dabei Karten, aber da Iframes gehen, sollte das ein Workaround sein). Auch [JavaScript](JavaScript)-Code ist möglich – es gibt dazu einen Online-Code-Editor –, um zum Beispiel Animationen oder interaktive Graphiken zu präsentieren.

Das Teil bietet auch *Social Media Features* und will so eine Art »Facebook für Online-Kreative« (so die FAQ) werden.

## Links

  * [NewHive Home](http://newhive.com/newhive/about)
  * [NewHive User Guide](http://newhive.com/newhive/about)