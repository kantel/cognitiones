#title "Apache Stratos"

**Apache Stratos** ist ein freies, erweiterbares Platform-as-a-Service (PaaS) Framework, das unter anderem helfen soll, Apache- [Tomcat](Tomcat)-, [PHP](PHP)- und [MySQL](MySQL)-Anwendungen in [Cloud](Cloud Computing)-Infrastrukturen laufen zu lassen und Entwicklern Umgebungen zum Programmieren, Testen und Ausführen skalierbarer Anwendungen in einer Cloud anbietet.

## Literatur

  * *[PaaS: Apache Stratos ist Top-Level-Projekt](http://www.heise.de/open/meldung/PaaS-Apache-Stratos-ist-Top-Level-Projekt-2215275.html)*, heise Open Source vom 3. Juni 2014

## Links

  * [Apache Stratos Home](https://cwiki.apache.org/confluence/display/STRATOS/Home)
  * [Apache Stratos Website](http://stratos.incubator.apache.org/)