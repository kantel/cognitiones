#title "Juvia"

<%= imageref("juvia-logo") %>

**Juvia** ist ein freies (GPL) Kommentarsystem zum Selberhosten, das ähnlich wie [Disqus](Disqus) auch in statische Seiten eingebunden werden kann. Es ist in [Ruby](Ruby) geschrieben und benötigt daher einen Webserver mit Ruby-Unterstützung.

## Links

  * [Juvia @ GitHub](https://github.com/phusion/juvia)
