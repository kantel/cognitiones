#title "Prose"

<%= imageref("prose", {:width => "480", :height => "334"}) %>

**Prose** ist ein [Webservice](Webservices), mit dem man [Markdown](Markdown)-Texte auf [GitHub](GitHub) editieren und im Web präsentieren kann. Prose kann mit [Jekyll](Jekyll) zusammenarbeiten.

Da Prose browserbasiert funktioniert, kann man den Service gut als Editor im iPad/iPhone für seine auf GitHub liegende Texte verwenden.

## Links

  * [Prose Home](http://prose.io/)
  * [Prose Getting Started](http://prose.io/help/getting-started.html)
  * [Prose Handbook](http://prose.io/help/handbook.html)
  * Michael Aufreiter: *[Introducing Prose: A Content Editor for GitHub](http://developmentseed.org/blog/2012/june/25/prose-a-content-editor-for-github/)*, Development Seed vom 25. Juni 2012


