#title "CamelCase"

<%= imageref("camelcase") %>

[Ward Cunningham](Ward Cunningham), unter anderem Erfinder der [Wiki-Systeme](Wiki), verwendete in seinem System <%= wpde("Binnenmajuskel") %>, d.h. Großbuchstaben innerhalb eines Wortes, um Links im Wiki auszuzeichen. Der Name **CamelCase** rührte daher, daß die *Binnenmajuskeln* aus dem Wort hervorstachen, wie die Höcker eines Kamels.

Heutzutage werden sie in Wikis kaum noch verwendet, da sie zwar das Auszeichnen von Links vereinfachen, aber der Text für Menschen schwerer zu lesen ist.