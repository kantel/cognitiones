#title "Wiki"

## Wiki-Software

  * [DokuWiki](http://www.dokuwiki.org/)
  * [MoinMoin](MoinMoin)
  * [TiddlyWiki](TiddlyWiki)

## Wiki-Hoster

## Desktop-Wiki

  * [Org-mode](Org-mode)
  * [WikiPad](WikiPad)

## Personen

  * [Ward Cunningham](Ward Cunningham), der Erbauer des ersten Wikis.
