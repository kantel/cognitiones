#title "Xataface"

<iframe width="560" height="315" src="//www.youtube.com/embed/apMpoMIr6gw?rel=0" frameborder="0" allowfullscreen></iframe>

**Xataface** (GPL) ist ein Framework und eine einfache Schnittstelle zur Erstellung von Webanwendungen auf Basis von PHP und einer [MySQL](MySQL)-Datenbank.

## Voraussetzungen

  * Apache
  * PHP
  * MySQL
  * PHPMyAdmin (optional)

## Tutorials

  * [Xataface: Einrichtung einer Applikation](http://www.it-muecke.de/node/708)

## Links

  * [Xataface Home](http://www.xataface.com/)
  * [Xataface @ GitHub](https://github.com/shannah/xataface)
  * [Xataface YouTube Channel](http://www.youtube.com/channel/UCEUW1dkQn-q3IL6W5c5LV9g)

