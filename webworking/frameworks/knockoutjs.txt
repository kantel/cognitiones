#title "Knockout.js"

<iframe src="//channel9.msdn.com/Events/MIX/MIX11/FRM08/player" width="560" height="315" allowFullScreen frameBorder="0"></iframe>

**Knockout.js** ist eine freie (MIT-Lizenz) JavaScript-Bibliothek für die Erstellung von agilen und interaktiven Web-Anwendungen. Hierbei bildet Knockout.js das Rückgrad einer Web-Applikation und übernimmt sowohl einen großen Teil der Logik als auch die Ausgabe auf dem Bildschirm. Hierfür kommt MVVM (Model-View-ViewModel) zum Einsatz, was für eine enge Verknüpfung zwischen Daten und Ausgabe steht.

## Tutorials

[[dr]]<iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//ws-eu.amazon-adsystem.com/widgets/q?ServiceVersion=20070822&OneJS=1&Operation=GetAdHtml&MarketPlace=DE&source=ss&ref=ss_til&ad_type=product_link&tracking_id=derschockwell-21&marketplace=amazon&region=DE&placement=1491914319&asins=1491914319&linkId=ADZAW77XA6GAZUTZ&show_border=true&link_opens_in_new_window=true"></iframe>[[dd]]

  * [Knockout.js: Performante Web-Apps – einfach wie nie](http://t3n.de/news/knockout-js-einfuehrung-556171/)
  * Golo Roden: *[Ab in den Ring, Model View ViewModel mit Knockout.js](http://www.heise.de/developer/artikel/Model-View-ViewModel-mit-Knockout-js-1928690.html)*, heise Developer vom 2. August 2013

## Links

  * [Knockout.js Home](http://knockoutjs.com/)
  * [Knockout.js @ GitHub](https://github.com/knockout/knockout)
  * <%= wp("KnockoutJS") %> in der Wikipedia