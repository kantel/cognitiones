#title "Bootstrap"

<%= imageref("bootstrap", {:width => "640", :height => "298"}) %>

[[dr]]<iframe src="http://rcm-eu.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=derschockwell-21&o=3&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=1449343910" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe>[[dd]]

**Bootstrap** (Apache-Lizenz) ist das Open-Source-Toolkit von [Twitter](Twitter) für die schnelle und elegante Entwicklung von Webseiten mit [HTML5](HTML5), CSS und [JavaScript](JavaScript). Das Web-Toolkit Bootstrap wurde erstmals Mitte 2011 von dem Entwickler Jacob Thornton und dem Designer Mark Otto, die beide bei Twitter arbeiten, vorgestellt. Anfang Februar 2012 wurde Bootstrap in Version 2 mit einigen neuen Features freigegeben.

## Tutorials

  * Jörg Kantel: *[Erste Schritte mit Bootstrap 2.0](http://blog.schockwellenreiter.de/essays/bootstrap2.html)*, Der Schockwellenreiter vom 4. Juni 2012

## Links

  * [Bootstrap 2.0 auf Github](http://twitter.github.com/bootstrap/index.html)
  * [Bootstrap Showcase](http://builtwithbootstrap.com/)
  * [Bootswatch](http://bootswatch.com/) (Apache-Lizenz) -- Saving the web from default Bootstrap
  * [StyleBootstrap.info](http://stylebootstrap.info/) -- Create unique design with Bootstrap v2
  * [Bootstrap Xtra](http://lightglitch.github.com/bootstrap-xtra/) extends Bootstrap
  * [Building Twitter Bootstrap](http://www.alistapart.com/articles/building-twitter-bootstrap/)
  * [Bootstrap 2 von Twitter: Webentwicklung mit CSS und HTML5 leicht gemacht](http://t3n.de/news/bootstrap-2-twitter-364716/)
  * Dave Winer: [Moving to Bootstrap 2.0](http://scripting.com/stories/2012/03/14/movingToBootstrap20.html)
  * [Stepping Out With Bootstrap from Twitter](http://webdesign.tutsplus.com/tutorials/htmlcss-tutorials/stepping-out-with-bootstrap-from-twitter/)
  * A List Apart: [Building Twitter Bootstrap](http://www.alistapart.com/articles/building-twitter-bootstrap/)
  * w3resource: [Twitter Bootstrap Tutorial](http://www.w3resource.com/twitter-bootstrap/tutorial.php)
  * [BootstrapCDN](http://www.bootstrapcdn.com/): Let us serve your Bootstrap

<%= imageref("boot") %>
