#title "Radio2"

<iframe src="http://player.vimeo.com/video/43993557" width="480" height="300" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>

**Radio2** ist ein [OPML Editor](OPML Editor)-Tool, das [RSS](RSS)-Feeds herausschreibt. Es ist ein minimalistisches Blogging-Tool, das unterschiedliche Kanäle bedienen kann. Auch zum Verteilen von [Podcasts](Podcasting) kann es eingesetzt werden. Radio2 ist eine Art [Radio UserLand](Radio UserLand) lite.

## Links

  * [Dave Winer](Dave Winer): [Upcoming: The minimal blogging tool](http://scripting.com/stories/2011/01/05/upcomingTheMinimalBlogging.html), Scripting News vom 5. Januar 2011
  * How To: [Using Radio2](http://docs.reallysimple.org/)
  * [Setting up a Radio2 server](http://serversetup.reallysimple.org/)
  * How To: [Downloading your Radio2 archive](http://worknotes.scripting.com/november2011/112311ByDw/downloadingYourRadio2Archive)