#title "nodeStorage"

**nodeStorage** ist ein freier (MIT-Lizenz) einfacher [S3](Amazon S3)-basierter Datenspeicher. Er nutzt Twitters Authenifizierung und ist in [Node.js](nodejs) implementiert. Autor von nodeStorage ist [Dave Winer](Dave Winer).

## Tutorials

[[dr]]<%= imageref("cowboy") %>[[dd]]

  * Andrew Shell: [Installing Storage on a VPS](https://github.com/scripting/nodeStorage/wiki/Installing-Storage-on-a-VPS)
  * Marco Fabbri: [Installing nodeStorage on Heroku](https://github.com/scripting/nodeStorage/wiki/Installing-nodeStorage-on-Heroku)

## Links

  * [nodeStorage @ GitHub](https://github.com/scripting/nodeStorage)
  * [nodeStorage Demo App](http://macwrite.org/) -- [Quellcoode auf GitHub](https://github.com/scripting/macwrite)
  * Dave Winer: [What is nodeStorage?](http://nodestorage.smallpict.com/2015/01/19/whatIsNodestorage.html)
  * Dave Winer: [Why nodeStorage is a big deal](http://scripting.com/2015/02/05/whyNodestorageIsABigDeal.html)
  * Schockwellenreiter: [All Web Apps need Storage](http://blog.schockwellenreiter.de/2015/01/20150126.html#p04)