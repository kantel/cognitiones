#title "AngularJS"

<iframe width="560" height="295" src="http://www.youtube.com/embed/uFTFsKmkQnQ" frameborder="0" allowfullscreen></iframe>

**AngularJS** (MIT-Lizenz) ist ein [JavaScript](JavaScript)-Framework von [Google](Google), das die Erstellung dynamischer HTML-Seiten vereinfachen will. Es verfolgt dabei einen eher unüblichen Ansatz. Statt das DOM zu parsen, wird HTML direkt als erweiterbares Template genutzt.


## Tutorials

  * Sascha Brink: *[AngularJS-Tutorial für Einsteiger](http://angularjs.de/artikel/angularjs-tutorial-deutsch)*
  * Sascha Brink: *[AngularJS Online-Buch](http://angularjs.de/buch)*
  * Golo Roden: *[Ecken und Kanten](http://www.heise.de/developer/artikel/Webanwendungen-mit-AngularJS-1955101.html) -- Webanwendungen mit AngularJS*, heise Developer vom 13. September 2013
  * Miško Hevery: *[Better Web Templating with AngularJS 1.0](http://googledevelopers.blogspot.nl/2012/06/better-web-templating-with-angularjs-10.html)*, Google Developers Blog vom 14. Juni 2012

## Links

  * [AngularJS Home](http://angularjs.org/)
  * <%= wpde("AngularJS") %> in der Wikipedia
  * [AngularJS.de](http://angularjs.de/) -- deutschsprachiges Webportal zu AngularJS
  * [YouTube-Kanal von John Lindquist](http://www.youtube.com/user/johnlindquist) mit weiteren Videos zu AngularJS
