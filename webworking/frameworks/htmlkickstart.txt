#title "HTML KickStart"

HTML KickStart ist ein frei verfügbares HTML-Framework von Joshua Gatcke für die schnelle Erstellung von Websites. Das Framework verfügt über viele vorgefertigte Komponenten und basiert auf [HTML5](HTML5), [JavaScript](JavaScript) und [jQuery](jQuery).

## Getting Started

<%= imageref("htmlkickstart", {:width => "480", :height => "331"}) %>

Im Download findet man obige Dummy-Seite `blank.html`. Sie bildet das Grundgerüst für jedes neue Projekt und kann beliebig erweitert und angepaßt werden.

## Links

  * [HTML KickStart Home](http://www.99lime.com/)
  * [HTML KickStart auf Github](https://github.com/joshuagatcke/HTML-KickStart)
  * Monika Steinberg: *[HTML KickStart: Schlanker Baukasten für HTML5, CSS, jQuery & Co.](http://t3n.de/news/html-kickstart-schlanker-363251/)*, t3n.de, 31. Januar 2012
  * Tutorial: [Jumpstart Your Web Project With HTML KickStart](http://designshack.net/articles/css/htmlkickstart/)