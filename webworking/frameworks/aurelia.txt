#title "Aurelia"

**Aurelia** ist ein freies (MIT-Lizenz) [JavaScript](JavaScript)-Frontend-Webframework für *Single Page Application* (SPA) mit einem Fokus auf modernes JavaScript und auf den Entwickler. Hinter diesem Projekt steckt der Ex-[Angular](angularjs)-Entwickler *Rob Eisenberg*, der im Juli 2016 die Version 1.0 herausbrachte und damit die Produktionsreife des Frameworks erklärte.

<iframe src="https://player.vimeo.com/video/117778145?title=0&byline=0&portrait=0" width="560" height="315" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

Eisenberg hatte zur initialen Vorstellung des JavaScript-Framework zur Entwicklung von Mobil-, Web- und Desktopanwendungen unter anderem mit dessen modularen Aufbau, dem Verzicht auf externe Abhängigkeiten und der Kompatibilität zu Sprachen wie [CoffeeScript](CoffeeScript) und [TypeScript](TypeScript) geworben.

## Literatur

[[dr]]<iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//ws-eu.amazon-adsystem.com/widgets/q?ServiceVersion=20070822&OneJS=1&Operation=GetAdHtml&MarketPlace=DE&source=ss&ref=as_ss_li_til&ad_type=product_link&tracking_id=derschockwell-21&marketplace=amazon&region=DE&placement=1785889672&asins=1785889672&linkId=4acee42c309023d11b611a6694284f62&show_border=true&link_opens_in_new_window=true"></iframe>[[dd]]

- Vildan Softic: *[Vorstellung des JavaScript-Frameworks Aurelia](https://www.heise.de/developer/artikel/Vorstellung-des-JavaScript-Frameworks-Aurelia-2633070.html)*, heise Developer vom 19. Mai 2015
- Julia Schmidt: *[JavaScript: Aurelia 1.0 steht samt virtueller Trainings bereit](https://www.heise.de/developer/meldung/JavaScript-Aurelia-1-0-steht-samt-virtueller-Trainings-bereit-3280514.html)*, heise Developer vom 28. Juli 2017
- Dwayne Charrington: *[Aurelia vs AngularJS – Round One: FIGHT!](https://ilikekillnerds.com/2015/01/aurelia-vs-angularjs-round-one-fight/)*, I Like Kill Nerds vom 4. Februar 2015

## Tutorials

- Chris Nwamba: [Build a Mini Instagram App with Aurelia](https://scotch.io/tutorials/build-a-mini-instagram-app-with-aurelia)

## Links

- [Aurelia Home](http://aurelia.io/)
- [Aurelia @ GitHub](https://github.com/aurelia/framework)