#title "MEAN"

<iframe width="560" height="315" src="//www.youtube.com/embed/AEE7DY2AYvI" frameborder="0" allowfullscreen></iframe>

Der **MEAN**-Stack ist ein reine [JavaScript](JavaScript)-Lösung für Web-Applikationen, die auf [MongoDB](MongoDB) als NoSQL-Datenbank, [Express](Express) als Server, [AngularJS](AngularJS) als Web-Framework und [Node.js](nodejs) als JavaScript-Engine aufsetzt.

## Video-Tutorial

<iframe width="560" height="315" src="//www.youtube.com/embed/PH_5lXxSpww" frameborder="0" allowfullscreen></iframe>

Building Your First MEAN Application

## Links

  * [MEAN.JS Home](http://meanjs.org/)
  * Artikelserie: [Einführung in den MEAN Stack](http://www.senaeh.de/seriesenaeh/einfuhrung-in-den-mean-stack/)