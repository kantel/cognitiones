#title "Backbone.js"

**Backbone.js** (MIT-Lizenz) ist das mit Abstand am weitesten verbreitete [JavaScript](JavaScript)-MVC-Framework für die Entwicklung von Single-Page-Anwendungen.

## Links

[[dr]]<iframe src="http://rcm-eu.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=derschockwell-21&o=3&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=1449328253" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe>&nbsp;<iframe src="http://rcm-eu.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=derschockwell-21&o=3&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=1449337392" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe>[[dd]]

  * [Backbone.js Home](http://backbonejs.org/)
  * Golo Roden: *[Model View Controler mit Backbone.js](http://www.heise.de/developer/artikel/Model-View-Controller-mit-Backbone-js-1938069.html)*, heise developer vom 19. August 2013
