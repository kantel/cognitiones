#title "Flask"

**Flask** (BSD-Lizenz) ist ein in [Python](Python) geschriebenes *Web Application Micro Framework*, das *Jinja2* und *Werkzeug* nutzt.

## Links

  * [Flask Website](http://flask.pocoo.org/)
  * [Flask Dokumentation](http://flask.pocoo.org/docs)
  * <%= wp("Flask (programming)") %> in der Wikipedia
