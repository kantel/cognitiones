#title "RunJS"

Die kostenlose Mac-Anwendung **RunJS** richtet sich an Entwickler, die im Alltag regelmäßig mit [JavaScript](JavaScript) zu tun haben und drückt diesen einen kleinen Schmierzettel in die Hand, der sich zum schnellen Test fast beliebiger JavaScript-Eingaben nutzen läßt.

<%= imageref("runjs-m") %>

*RunJS* wertet die Eingaben seines Nutzers dabei in Echtzeit aus und zeigt in der rechten Hälfte des Programmfensters sofort die entsprechende Code-Ausgabe an.

Die Mini-App ist in der Lage [Node](nodejs)-Module zu importieren, unterstützt [TypeScript](TypeScript) und [Babel](Babel) und läßt sch auf der Webseite des Anbieters als etwa 60 MB großer [Electron](Electron)-Download aus dem Netz laden.

**Caveat**: Die Software sollte nicht mit [runjs](runjs Build Tool) verwechselt werden, ein (minimalistisches) Build-Tool für JavaScript-Anwendungen ähnlich [Grunt](Grunt) und [Gulp](Gulp).

## Literatur

- *John Maeda*: [RunJS: JavaScript scratchpad](https://maeda.pm/2019/01/13/runjs-javascript-scratchpad/)

## Links

- [RunJS Home](https://projects.lukehaas.me/runjs/) (hier kann die Software auch heruntergeladen werden)
- [RunJS @ GitHub](https://github.com/lukehaas/runjs)