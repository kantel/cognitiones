#title "Grunt"

<%= imageref("grunt-first-steps", {:width => "560", :height => "339"}) %>

**Grunt** ist ein Tool, das hilft, wiederkehrende Prozesse zu automatisieren. Es setzt dabei komplett auf [Node.js](nodejs) und [JavaScript](JavaScript), damit Webentwickler nicht noch eine zusätzliche Sprache lernen müssen.

## Grunt JavaScript Automation for the Lazy Developer

<iframe width="560" height="315" src="//www.youtube.com/embed/bntNYzCrzvE" frameborder="0" allowfullscreen></iframe>

## Grunt-Tools

  * [Yeoman](Yeoman)
  * [Assemble](assemble)

[[dr]]<iframe src="http://rcm-eu.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=derschockwell-21&o=3&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=1941222110" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe>[[dd]]

## Literatur

  * Frederic Hemberger: *[(Bessere) Projekt-Pakete schnüren mit Grunt](http://webkrauts.de/artikel/2012/build-tool-grunt)*, Webkrauts vom 3. Dezember 2012

## Links

  * [Grunt Home](http://gruntjs.com/)
  * Grunt Dokumentation: [Getting Started](http://gruntjs.com/getting-started)