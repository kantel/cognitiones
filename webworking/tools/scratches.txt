#title "Scratches"

**Scratches** ist ein kleiner JavaScript-Editor mit einer Read-Eval-Print-Loop (REPL), der das Skizzieren von JavaScript-Programmen ähnlich wie [Processing](Processing) oder [P5.js](p5js) erlaubt. Das Teil kommt mit einem *Document Window*, so daß man wie in einem Browser spielen kann.

<%= imageref("scratches-b") %>

Das Teil ist eine [Electron](Electron)-Anwendung, so daß es auch die üblichen, aus Chrom bekannten Developer-Tools wie Elemente-Inspektor oder die JavaScript-Console und sein eigenes [Node.js](nodejs) mitbringt.

Scratches ist auf [GitHub zu finden](https://github.com/0x00A/scratches) ([Download](https://github.com/0x00A/scratches/releases)), über die Lizenz ist mir allerdings nichts bekannt.

## Links

- [Scratches @ GitHub](https://github.com/0x00A/scratches)
- [Download](https://github.com/0x00A/scratches/releases)