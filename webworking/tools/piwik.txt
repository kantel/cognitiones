#title "Piwik"

<iframe width="560" height="315" src="//www.youtube.com/embed/OslfF_EH81g?rel=0" frameborder="0" allowfullscreen></iframe>

**Piwik** ist ein Open-Source-Programm (GPL) für Webanalytik. Die Softweare ist eine Alternative zu *Google Analytics*. Auch wenn der Funktionsumfang geringer ist, hat es den Vorteil, die datenschutzrechtlich sensiblen Logdaten auf dem eigenen Server zu speichern.

## Links

  * [Deutschsprachige Piwik Homepage](http://de.piwik.org/)
  * <%= wpde("Piwik") %> in der Wikipedia

<%= imageref("piwik-logo") %>
