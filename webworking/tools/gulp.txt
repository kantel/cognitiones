#title "Gulp"

<%= imageref("gulp") %>

**Gulp** ist ein weiterer, freier (MIT-Lizenz) Taskrunner für [JavaScript](JavaScript)-basierte Webprojekte, der sich als leichtgewichtige Alternative zu [Grunt](Grunt) versteht.

## Tutorials

  * [Durchstarten mit Gulp.js – Websites optimieren, Arbeitsabläufe automatisieren](http://magazin.phlow.de/webdesign/gulp/) -- mit vielen Links zu weiteren Tutorials
  * [Introduction to Gulp.js](http://stefanimhoff.de/2014/gulp-tutorial-1-intro-setup/): 16 Artikel, die unter anderem auch zeigen, wie man Gulp in einem [Ruby](Ruby)-Umfeld nutzen kann.

## Links

  * [Gulp Home](http://gulpjs.com/)
  * [Gulp @ GitHub](https://github.com/gulpjs/gulp)
  * [Gulp Dokumentation](https://github.com/gulpjs/gulp/blob/master/docs/README.md)