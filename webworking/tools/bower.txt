#title "Bower"

<%= imageref("tool-bower") %>

**Bower** ist ein in [JavaScript](JavaScript) geschriebener Paketmanager für das Web. Bower sucht seine Pakete via [Git](Git) und benötigt [Node.js](nodejs) mit `npm`.

## Links

  * [Bower Home](http://bower.io/)
  * [Bower @ GitHub](https://github.com/bower/bower)