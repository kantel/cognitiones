#title "Obsidian"

**Obsidian** ist nicht nur eine elektronische [Zettelkasten](Zettelkasten)-Anwendung (nach [Niklas Luhmann](Niklas Luhmann)), sondern auch ein [Markdown](Markdown)-Editor, denn die Software speichert alle Notizen als Markdown-Dateien. Dadurch sind die Notizen nicht in proprietäre Formate begraben, sondern es sind reine, für Menschen lesbare Textdateien. Das sichert hoffentlich eine Nachhaltigkeit, die Notizen auch nach Jahren noch lesbar aufbewahrt.

<%= imageref("obsidian-b") %>

Obsidian ist eine [Electron](Electron)-Anwendung, dadurch läuft die Software plattformübergreifend mindestens unter MacOS X, Windows und Linux. Für Privatanwender ist die Nutzung kostenlos, kommerzielle Nutzer zahlen $50/Jahr.

Die Software unterstützt [Common Mark](CommonMark) wie auch [GitHub Flavoured Markdown](GFM) (GFM) und etliche Erweiterungen wie *Backlinks*, Fußnoten und mathematische LaTex-Notation.

## Literatur

- [Obsidian: Ein Markdown-Zettelkasten für den Mac](https://www.ifun.de/obsidian-ein-markdown-zettelkasten-fuer-den-mac-154573/), iFund.de vom 29. Mai 2020
- *Hause Lin*: [Take Better Notes With This Free Note-Taking App That Wants to Be Your Second Brain](https://medium.com/swlh/take-better-notes-with-this-free-note-taking-app-that-wants-to-be-your-second-brain-1a97909a677b), The Startup vom 6. Juli 2020 (Medium.com-Link)
- *Alice Camilla*: [Your Beginner's Guide to Obsidian](https://www.keepproductive.com/blog/obsidian-beginners-guide), Keep Productive vom 27. August 2020 (mit vielen Video-Tutorials)
- *Jan Schaller*: [Obsidian – mein zweites Gehirn](https://papierlos-studieren.net/2020/08/19/obsidian-mein-zweites-gehirn/)? Papierlos studieren vom 19. August 2020

## Links

- [Obsidian Home](https://obsidian.md/)