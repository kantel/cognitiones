#title "Markdown"

**Markdown** ist eine einfache [Auszeichnungssprache](Auszeichnungssprachen), die von John Gruber und Aaron Swartz entworfen wurde. Ein Ziel von Markdown ist, daß schon die Ausgangsform ohne weitere Konvertierung als leicht lesbare Textdatei verwendet werden kann. Die Auszeichnungselemente wurden daher bereits übliche Auszeichnungsarten in Plaintext und E-Mails einbezogen. Auszeichnungssprachen mit ähnlichen Zielen zur Lesbarkeit – wie [reStructuredText](reStructuredText) oder [Textile](Textile) – hatten ebenfalls Einfluß auf die Syntax.

Die Markdown-Konvertierungssoftware wandelt Text in gültiges und W3C-konformes XHTML um. Die Referenzimplementierung in [Perl](Perl) steht unter einer BSD-artigen Lizenz. Daneben sind inzwischen etliche weitere Implementierungen in verschiedenen Programmiersprachen verfügbar. Als Auszeichnungssprache wird Markdown in diversen [Content-Management-Systemen](Content Management Systeme) verwendet.

Autoren können bei Bedarf für komplexere Auszeichnungen XHTML-Blockelemente verwenden. Diese Elemente werden von der Konvertierungssoftware ohne Änderung in die Zielform übertragen. Dadurch ist es möglich, gewisse Bereiche in gewöhnlichem XHTML zu formatieren.

## Anwendungen

  * [RubyFrontier](RubyFrontier) beherrscht Markdown
  * Und für [TextMate](TextMate) gibt es ein Bundle (im Lieferumfang enthalten), das Markdown nach HTML wandelt und auch [Multimarkdown](Multimarkdown) unterstützt.
  * [MacDown](MacDown) ist ein Markdown-Editor für den Mac mit Live-Preview
  * [Haroopad](Haroopad) -- ein weiter Markdown-Editor mit Live-Preview (plattformübergreifend)
  * [Logseq](Logseq), ein Outliner, der sowohl Markdown wie auch [Org mode](Org-mode) kann
  * [MarkText](Marktext)  ist ein einfacher, eleganter, freier (MIT-Lizenz) Markdown-Editor.
  * [Obsidian](Obsidian) (für den privaten Gebrauch frei wie Freibier) ist nicht nur ein komfortabler Markdown-Editor, sondern auch ein elektronischer [Zettelkasten](Zettelkasten).
  * [Typora](Typora) noch ein plattformübergreifender Markdown-Editor mit nur einem Editor- **und** Preview-Fenster (leider kommerziell)
  * [Zettlr](Zettlr), ein freier Markdown-Editor für wissenschaftliches Arbeiten (ohne Preview-Fenster, zeigt aber Bilder und Formeln im Editor an)

## Notizbuch-Apps

- [Boostnote](Boostnote)
- [Joplin](Joplin)
- [Notable](Notable)

## Implementierungen und Tools

  * [WMD](WMD) (Markdown in [JavaScript](JavaScript))

## Markdown Supersets

  * [Multimarkdown](Multimarkdown)
  * [Hoedown](Hoedown)
  * [kramdown](kramdown)
  * [CommonMark](CommonMark)
  * [Python Markdown](Python Markdown)
  * [PHP Markdown](PHP Markdown Extra)
  * [Pandoc](Pandoc)

## Siehe auch ...

  * [reStructuredText](reStructuredText)
  * [Textile](Textile)

## Texte und Tutorials

  * TJ Luoma: [Markdown Primer](http://www.tuaw.com/markdown-primer)
  Brett Terpstra: [Why Markdown? A two-minute explanation](http://brettterpstra.com/why-markdown-a-two-minute-explanation/)

## Links

  * [Markdown](http://de.wikipedia.org/wiki/Markdown) in der Wikipedia
  * [Markdown bei John Gruber](http://daringfireball.net/projects/markdown/)
  * [Deutsches Syntax-Manual](http://markdown.de/syntax/)
  * [Markdown Testreihe](http://six.pairlist.net/pipermail/markdown-discuss/2004-December/000909.html) für Entwickler
  * [Markdown Wiki](http://markdown.infogami.com/) mit vorgeschlagenen Erweiterungen und Implementierungen in verschiedenen Sprachen
  * [Markdown in Python](http://www.freewisdom.org/projects/python-markdown/)
  * [Markdown2](http://code.google.com/p/python-markdown2/) -- eine weitere Python-Markdown-Implementierung
  * [PHP Markdown](http://michelf.com/projects/php-markdown/)
  * [BlueCloth](http://deveiate.org/projects/BlueCloth) ist eine [Ruby](Ruby)-Markdown-Implementierung
  
  