#title "CriticMarkup"

<iframe src="http://player.vimeo.com/video/59450609?portrait=0&amp;color=c8b3df" width="560" height="315" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>

**CriticMarkup** ist eine an [Markdown](Markdown) angelehnte Auszeichnungssprache, mit der Änderungen an Texten dokumentiert und nachvollzogen werden können.

## Links

  * [CriticMarkup Home](http://criticmarkup.com/)
  * [Everyone's a Critic: The Critic Markup Language Proposal](http://www.macdrifter.com/2013/02/everyones-a-critic-the-critic-markup-language-proposal.html)