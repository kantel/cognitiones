#title "reStrucutredText"

**reStructuredText** (kurz: *ReST*, *reST* oder *RST*) ist eine vereinfachte [Auszeichnungssprache](Auszeichnungssprachen) (Markup) mit dem Ziel, in der reinen Textform besonders lesbar zu sein. Weiterhin soll reStructuredText leicht in andere Formate umwandelbar sein.

Der reStructuredText-Parser ist eine Komponente der Docutils, einer in der Programmiersprache [Python](Python) entwickelten Textverarbeitungsbibliothek.

## Links

  * [reStructuredText Homepage](http://docutils.sourceforge.net/rst.html)
  * [Kurzreferenz](http://docutils.sourceforge.net/docs/user/rst/quickref.html)
  * [reStructuredText](http://de.wikipedia.org/wiki/ReStructuredText) in der Wikipedia