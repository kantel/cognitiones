#title "SourceTree"


<iframe width="480" height="296" src="http://www.youtube.com/embed/lDUOvLE8T4o" frameborder="0" allowfullscreen></iframe>

**SourceTree** ist ein freier [MacOS X](MacOS X)-Client für [Git](Git), [Mercurial](Mercurial) und [SVN](SVN). Das Teil spielt auch nett mit [BitBucket](BitBucket) zusammen.

## Links

  * [SourceTree Home](http://www.sourcetreeapp.com/)
  * [SourceTree im App Store](http://itunes.apple.com/de/app/sourcetree-git-hg/id411678673)

