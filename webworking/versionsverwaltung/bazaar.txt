#title "Bazaar"

<%= imageref("bazaar") %>

**Bazaar** (GPL) ist ein verteiltes [Versionskontrollsystem](Versionsverwaltung). Es ist in [Python](Python) geschrieben, Teil des GNU-Projekts und läuft unter [Linux](Linux), Windows und [MacOS X](MacOS X).

Bazaar kann sowohl von einem einzelnen Nutzer, der mehrere Branches verwalten möchte, wie auch von einem Team von Nutzern, die gemeinsam an einem Projekt arbeiten, genutzt werden.

Im Gegensatz zu »reinen« verteilten Versionskontrollsystemen (wie z.B. [Git](Git)), die ohne einen zentralen Server auskommen, unterstützt Bazaar auch das Arbeiten mit einem zentralen Server, kann aber auch ohne. Es kann mit [Subversion](Apache Subversion) in beiden Richtungen zusammenarbeiten und hat Importfunktionen für [Mercurial](Mercurial) und Git.

## Online-Repositorien

Bazaar unterstützt diverse Online Repositorien, wie zum Beispiel:

  * [Launchpad](Launchpad)
  * GNU Savannah
  * Sourceforge

## Links

  * [Bazaar Hompage](http://bazaar.canonical.com/)
  * <%= wp("Bazaar (software)") %> in der Wikipedia