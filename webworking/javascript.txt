#title "JavaScript"

**JavaScript** ist eine Skriptsprache, die ursprünglich für einfache Scripting-Aufgaben in Web-Browsern eingesetzt wurde, zunehmend – vor allem durch [HTML5](HTML5) – aber auch weitere Bedeutung gewinnt.

Der als **ECMAScript** (ECMA 262) standardisierte Sprachkern von JavaScript beschreibt eine dynamisch typisierte, objektorientierte, aber klassenlose Skriptsprache. Sie wird allen objektorientierten Programmierparadigmen unter anderem auf der Basis von Prototypen gerecht. In JavaScript läßt sich objektorientiert und sowohl prozedural als auch funktional programmieren.

## In diesem Wiki

  * [AngularJS](AngularJS)
  * [Backbone.js](backbonejs)
  * [CoffeeScript](CoffeeScript)
  * [Bower](Bower)
  * [Ember.js](Ember)
  * [Espruino](Espruino)
  * [Grunt](Grunt)
  * [Gulp](Gulp)
  * [JavaScript OSA](JavaScript OSA)
  * [Knockout.js](knockoutjs)
  * [NativeScript](NativeScript)
  * [Node.js](Node) und [io.js](iojs)
  * [SpiderMonkey](SpiderMonkey)
  * [Scratches](Scratches)
  * [Yeoman](Yeoman)
  * [Webpack](Webpack)

## Spieleprogrammierung in HTML5 und JavaScript

  * In diesem Wiki: [Spieleprogrammierung mit HTML5 und JavaScript](Spieleprogrammierung mit HTML5 und JavaScript)
  * [Kiwi.js](kiwijs)
  * [p5.js](p5js)
  * [Processing.js](processingjs)

## Weitere populäre JavaScriptBibliotheken

  * [PhotoSwipe](PhotoSwipe)
  * [PhosphorJS](PhosphorJS)

## Bücher

<div style="float:right; margin-left:8px;"><iframe src="http://rcm-de.amazon.de/e/cm?lt1=_blank&#038;bc1=000000&#038;IS2=1&#038;bg1=FFFFFF&#038;fc1=000000&#038;lc1=0000FF&#038;t=derschockwell-21&#038;o=3&#038;p=8&#038;l=as4&#038;m=amazon&#038;f=ifr&#038;ref=ss_til&#038;asins=3897218615" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe></div>

  * [JavaScript von Kopf bis Fuß](JavaScript von Kopf bis Fuß)

## JavaScript Frameworks

  * Ext JS
  * [jQuery](jQuery)
  * Yahoo! User Interface Library ([YUI](YUI))

## JavaScript-Alternativen

  * [CoffeScript](CoffeeScript)
  * [Dart](Dart)

## Links

  * <%= wpde("JavaScript") %> in der Wikipedia