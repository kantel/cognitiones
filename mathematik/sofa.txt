#title "SOFA"

<iframe width="560" height="315" src="http://www.youtube.com/embed/w84DraHwAfk?rel=0" frameborder="0" allowfullscreen></iframe>

**SOFA** (Statistic Open For All) ist eine freie (APGL3) Statistik-Software für Windows, Linux und MacOS X. Sie ist in [Python](Python) geschrieben und nutzt [wxPython](wxPython) für das GUI.

## Ähnliche Software

  * [GNU R](GNU R)

## Links

  * [SOFA Homepage](http://www.sofastatistics.com/)
  * [SOFA User Guide](http://www.sofastatistics.com/userguide.php)
  * [SOFA Blog](http://www.sofastatistics.com/blog/)
  * [SOFA Channel auf YouTube](http://www.youtube.com/user/pykiwi)


