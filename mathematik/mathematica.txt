#title "Mathematica"

**Mathematica** ist ein kommerzielles, von [Stephen Wolfram](Stephen Wolfram) konzipiertes Softwarepaket und stellt eines der meistbenutzten mathematisch-naturwissenschaftlichen Programmpakete dar.

## Links

  * [Mathematica Homepage](http://www.wolfram.com/mathematica/)
  * [Mathematica](http://de.wikipedia.org/wiki/Mathematica) in der Wikipedia