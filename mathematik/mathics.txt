#title "Mathics"

**Mathics** (GPL) ist ein freies Computeralgebra-System mit einer [Mathematica](Mathematica)-kompatiblen Syntax. Das Backend ist in [Python](Python) geschrieben und nutzt [SymPy](SymPy) als Engine. Außerdem kann Mathics mit [Sage](Sage) kombiniert werden. Mathics kann als Service online genutzt, aber auch als [Webserver auf dem eigenen Desktop](Webserver auf dem Desktop) betrieben werden.

Natürlich kann Mathics nicht mit dem Boliden Mathematica mithalten, insbesondere ist das Teil signifikant langsamer und etliche der High End-Funktionen von Mathematica sind nicht implementiert. Mathics läuft entweder in der Konsole oder im Browser, besitzt dort aber keinerlei Notebook-Funktionalität. Aber wichtige Features sind implementiert:

- Wolfram Language als funktionale Programmiersprache.
- Rationale und komplexe Zahlen und eine beliebig präzise Arithmetik.
- Eine große Anzahl von Funktionen zur Listen- und Strukturen-Manipulation.
- Im Webbrowser eine interaktive, graphische Benutzerschnittstelle, die auf MathML basiert.
- 2D-Plots werden als [SVG](SVG)-Graphiken herausgeschrieben, 3D-Plots nutzen [WebGL](WebGL).
- Ein Export nach [LaTeX](LaTeX).
- Eine einfache Möglichkeit, neue Funktionen in Python zu implementieren.

Der lokale Mathics-Server basiert auf [Django](Django), es gibt jedoch Überlegungen, ihn durch eine leichtgewichtigere Alternative zu ersetzen.

Mathics kann mit

~~~bash
pip install mathics
~~~

installert werden. Einmal installiert wird die Konsolenversion mit `mathics` aufgerufen, der lokale Server mit `mathicsserver`. Dieser lauscht dann auf `localhost:8000` auf Eingaben seines Benutzers (den Port kann man bei Bedarf umbiegen).

## Links

  * [Mathics Home](http://mathics.github.io/)
  * [Mathics @ GitHub](https://github.com/mathics/Mathics)
  * [Mathics Wiki](https://github.com/mathics/Mathics/wiki), darin eine [Installationsanleitung](https://github.com/mathics/Mathics/wiki/Installing) (darin steht noch, daß Mathics auch mit Python 2.7 läuft, diese Unterstützung wurde jedoch vor wenigen Monaten entfernt)
  * [Mathics-Dokumentation](http://mathics.github.io/docs/mathics-1.0.pdf) (<%= imageref("pdf") %>)
