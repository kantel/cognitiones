#title "MathJax"

<object classid='clsid:d27cdb6e-ae6d-11cf-96b8-444553540000' codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,115,0' width='480' height='300'><param name='movie' value='http://screenr.com/Content/assets/screenr_1116090935.swf' ></param><param name='flashvars' value='i=95470' ></param><param name='allowFullScreen' value='true' ></param><embed src='http://screenr.com/Content/assets/screenr_1116090935.swf' flashvars='i=95470' allowFullScreen='true' width='480' height='300' pluginspage='http://www.macromedia.com/go/getflashplayer' ></embed></object>

**MathJax** (Apache-Lizenz) ist eine freie [JavaScript](JavaScript)-Bibliothek, die [LaTeX](LaTeX)- oder *MathML*-Formeln in jedem Web-Browser rendert. Sie nutzt eigene Webfonts für [HTML5](HTML5)-fähige Browser, hat aber bei Bedarf einen Fallback zu Bildern.

## Integrate MathJax into your web pages

<object classid='clsid:d27cdb6e-ae6d-11cf-96b8-444553540000' codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,115,0' width='480' height='300'><param name='movie' value='http://screenr.com/Content/assets/screenr_1116090935.swf' ></param><param name='flashvars' value='i=116461' ></param><param name='allowFullScreen' value='true' ></param><embed src='http://screenr.com/Content/assets/screenr_1116090935.swf' flashvars='i=116461' allowFullScreen='true' width='480' height='300' pluginspage='http://www.macromedia.com/go/getflashplayer' ></embed></object>

## Copy and paste math content using MathJax

<object classid='clsid:d27cdb6e-ae6d-11cf-96b8-444553540000' codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,115,0' width='480' height='300'><param name='movie' value='http://screenr.com/Content/assets/screenr_1116090935.swf' ></param><param name='flashvars' value='i=93445' ></param><param name='allowFullScreen' value='true' ></param><embed src='http://screenr.com/Content/assets/screenr_1116090935.swf' flashvars='i=93445' allowFullScreen='true' width='480' height='300' pluginspage='http://www.macromedia.com/go/getflashplayer' ></embed></object>

## Links

  * [MathJax Home Page](http://www.mathjax.org/)
  * [Download at Sourceforge](http://sourceforge.net/projects/mathjax/)
  * <%= wpde("MathJax") %> in der Wikipedia


