#title "Shiny"

**Shiny** ist ein freies (GPLv3) Web-Applikation-Framework, um mit [R](GNU R), [knitr](knitr) und [R Markdown](R Markdown) interaktive Dokumente zu erzeugen.

Shiny wurde von den Machern von [RStudio](RStudio) entwickelt.

## Tutorials

  * [Teach yourself Shiny](http://shiny.rstudio.com/tutorial/), mit einem eingebetteten, zweieinhalbstündigen Lehrvideo.
  * Dean Attali: *[Persistent data storage in Shiny apps](http://deanattali.com/blog/shiny-persistent-data-storage/)*, 1. Juli 2015

## Links

  * [Shiny Home](http://shiny.rstudio.com/)
  * [Shiny @ GitHub](https://github.com/rstudio/shiny)
  * [Shiny im ipub-Blog](http://ipub.com/shiny/), 22. Oktober 2015
