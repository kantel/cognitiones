#title "Sierpinski-Dreieck"

Das **Sierpinski-Dreieck** ist ein 1915 von *Wacław Sierpiński* beschriebenes Fraktal – mitunter auch Sierpinski-Fläche oder -Dichtung genannt, welches eine selbstähnliche Teilmenge eines (meist gleichseitig dargestellten) Dreiecks ist. Teilt man das Dreieck in vier zueinander kongruente und zum Ausgangsdreieck ähnliche Dreiecke, deren Eckpunkte die Seitenmittelpunkte des Ausgangsdreiecks sind, dann sind die Teilmengen des Fraktals in den drei äußeren Dreiecken skalierte Kopien des gesamten Fraktals, während das mittlere Teildreieck nicht zum Fraktal gehört. Diese Aufteilung des Fraktals in skalierte Kopien kann in den äußeren Teildreiecken rekursiv fortgesetzt werden.

## Links

  * [the sierpinski triangle page to end most sierpinski triangle pages](http://www.oftenpaper.net/sierpinski.htm) (lange Ladezeit!)
  * Das <%= wpde("Sierpinski-Dreieck") %> in der Wikipedia