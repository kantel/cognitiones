#title "SymPy"
#updated "20. Mai 2023"

<%= imageref("sympy-logo") %>

**SymPy** ist eine freie (BSD-Lizenz) [Python](Python)-Bibliothek für symbolische Mathematik. Sie ist komplett in Python geschrieben und besitzt keine weiteren Abhängigkeiten. SymPy wird unter anderem von [Mathics](Mathics) genutzt.

## Links

  * [SymPy Home](http://sympy.org/en/)
  * [SymPy auf Google Code](http://code.google.com/p/sympy/)

