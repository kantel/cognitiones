#title "WebR"
#created "12. April 2023"
#updated "12. April 2023"

<%= imageref("webr") %>

**WebR** ist eine freie (MIT-Lizenz) Version der Statistiksprache [R](R), die für den Browser unter [Node.js](Node) mithilfe von [WebAssembly](WebAssembly) über [Emscripten](Emscripten) kompiliert wurde.

WebR ermöglicht es, R-Code im Browser auszuführen, ohne daß ein R-Server zur Ausführung des Codes erforderlich ist: Der R-Interpreter wird direkt auf dem Computer des Benutzers ausgeführt. Einige R-Pakete wurden auch für die Verwendung mit webR portiert und können auf die übliche Weise mit der Funktion `library()` geladen werden.

Siehe auch [WebRIDEr](WebRIDEr)

## Literatur

- George Stagg: *[webR 0.1.0 has been released](https://www.tidyverse.org/blog/2023/03/webr-0-1-0/)*, Tidyverse vom 9. März 2023

## Links

- [WebR Home und Dokumentation](https://docs.r-wasm.org/webr/latest/)
- [WebR @ GitHub](https://github.com/r-wasm/webr)