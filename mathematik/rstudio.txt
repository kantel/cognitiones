#title "RStudio"

<%= imageref("rstudio", {:width => "480", :height => "374"}) %>

**RStudio** ist eine freie (AGPL) Entwicklungsumgebung für [R](GNU R). Es gibt sie für [Linux](Linux), Windows und [MacOS X](MacOS X). Sie besitzt Syntax-Coloring, Code-Vervollständigung, eine Schnittstelle zu [Git](Git) oder [Subversion](Apache Subversion), kann Bilder und PDFs komfortabel exportieren und arbeitet mit [LaTeX](LaTeX) zusammen.

<div style="float:right; margin-left: 8px;"><iframe src="http://rcm-de.amazon.de/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=derschockwell-21&o=3&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=1449309038" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe></div>

Von den Machern von RStudio gibt es auch [Shiny](Shiny), ein Web-Applikation-Framework für interaktive Dokumente mit [R](GNU R), [knitr](knitr) und [R Markdown](R Markdown).

## In diesem Wiki

  * [R Markdown](R Markdown): *Literate Programming* mit [R](Gnu R) und RStudio

## Links

  * [RStudio Home](http://rstudio.org/)