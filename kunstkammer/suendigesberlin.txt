#title "Sündiges Berlin"

<%= imageref("strafraum-iii", {:width => "560px", :height => "400px"}) %>

<div style="float:right; margin-Left: 8px;"><iframe src="http://rcm-de.amazon.de/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=derschockwell-21&o=3&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=3936878226" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe></div>

## Literatur

  * Peter Pichtermann: *[»Sündiges Berlin«-Party: Verrucht mit Rauchverbot](http://www.spiegel.de/kultur/gesellschaft/suendiges-berlin-party-verrucht-mit-rauchverbot-a-817639.html)*, Spiegel Online vom 26. Februar 2012

  * Mel Gordon: *<a href="http://www.amazon.de/gp/product/3936878226/ref=as_li_ss_tl?ie=UTF8&tag=derschockwell-21&linkCode=as2&camp=1638&creative=19454&creativeASIN=3936878226">Sündiges Berlin: Die zwanziger Jahre: Sex, Rausch, Untergang</a><img src="http://www.assoc-amazon.de/e/ir?t=derschockwell-21&l=as2&o=3&a=3936878226" width="1" height="1" border="0" alt="" style="border:none !important; margin:0px !important;" />*, Index-Verlag, 2011