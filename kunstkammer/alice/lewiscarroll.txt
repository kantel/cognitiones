#title "Lewis Carroll"


**Lewis Carroll** (* 27. Januar 1832 in Daresbury im County Cheshire; † 14. Januar 1898 in Guildford im County Surrey; eigentlich *Charles Lutwidge Dodgson*) war ein britischer Schriftsteller des viktorianischen Zeitalters, Photograph, Mathematiker und Diakon.

Er ist der Autor der berühmten Bücher *Alice im Wunderland*, *Alice hinter den Spiegeln* (oder *Alice im Spiegelland*), *The Hunting of the Snark* und seines einzigen Romans *Sylvie und Bruno*.

## Links

  * Ein sehr ausführlicher und wunderbarer Artikel zu <%= wpde("Lewis Carroll") %> in der Wikipedia
