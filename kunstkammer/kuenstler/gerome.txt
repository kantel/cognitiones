#title "Jean-Léon Gérôme"

<%= imageref("gerome-portrait", {:height => "628", :width => "480"}) %>

*Jean-Léon Gérôme: Selbstportrait, 1886*

**Jean-Léon Gérôme** (* 11. Mai 1824 in Vesoul, Haute-Saône; † 10. Januar 1904 in Paris), war ein französischer Historienmaler und Bildhauer. Sein Werk ist dem akademischen Klassizismus zuzuschreiben.

## In diesem Wiki

  * [Der Sklavenmarkt in Rom](Der Sklavenmarkt in Rom)

## Links

  * <%= wpde("Jean-Léon Gérôme") %> in der Wikipedia