#title "Jean Henri Gaston Giraud"

**Jean Henri Gaston Giraud** (* 8. Mai 1938 in Nogent-sur-Marne nahe Paris; † 10. März 2012 in Paris; Pseudonyme: **Gir**, **Mœbius**, auch **Moebius**) war ein bedeutender französischer Comiczeichner und -scenarist, der den frankobelgischen Comic stark beeinflußt hat.

## Literatur

  * Jörg Böckem: *[Moebius-Comics: Meister der Seltsamkeit](http://www.spiegel.de/kultur/literatur/neu-aufgelegte-werke-von-moebius-a-878399.html)*, Spiegel Online vom 21. Januar 2013

## Links

  * <%= wpde("Jean Giraud") %> in derWikipedia

