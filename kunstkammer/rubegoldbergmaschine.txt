#title "Rube-Goldberg-Maschine"

<iframe width="480" height="296" src="http://www.youtube.com/embed/46phqVTO2jc" frameborder="0" allowfullscreen></iframe>

Eine **Rube-Goldberg-Maschine** ist ein komplexer Apparat, der eine sehr einfache Aufgabe unnötig langsam, indirekt und umständlich erledigt. Dabei werden verschiedene Teile (beispielsweise Konservendosen, Kugeln oder Bindfäden) so aufgebaut, daß sie eine Kettenreaktion ergeben, welche ganz zum Schluß den eigentlichen Zweck der Maschine erfüllt.

Der Ausdruck Rube-Goldberg-Maschine geht auf *Professor Lucifer Gorgonzola Butts*, einem Comic des US-amerikanischen Cartoonisten [Rube Goldberg](Rube Goldberg) zurück.

## In diesem Wiki

  * Der [Lauf der Dinge](Der Lauf der Dinge)

## Links

  * Die <%= wpde("Rube-Goldberg-Maschine") %> in der Wikipedia
  * [Offizielle Rube Goldberg Website](http://www.rubegoldberg.com/)
  * Karin Seethaler: *[Verrückte Maschinen: Klick, ratter, boing - Überraschung!](http://einestages.spiegel.de/static/topicalbumbackground-xxl/24722/rube_goldberg.html)*, Spiegel Online (einestages) vom 24. April 2012
  * Wolf-Dieter Roth über Rube-Goldberg-Maschinen: *[Warum einfach, wenn’s viel schöner auch kompliziert geht?](http://neuerdings.com/2012/06/26/rube-goldberg-maschinen/)*, neuerdings.com vom 26. Juni 2012