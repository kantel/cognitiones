#title "Bücherzelle"

<iframe src="http://player.vimeo.com/video/21754886?title=0&amp;byline=0&amp;portrait=0" width="480" height="318" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>

Die **Bücherzelle** von *James Econs* steht in Horsley, Surrey, UK und ist offen für alle. Der Macher nennt diese und ähnliche Aktionen *»Socially Beneficial Creative Vandalism«*. Auch für Deutschland zur Nachahmung empfohlen.

## Links

  * [The PhoneBoox book exchange](https://vimeo.com/21754886) auf Vimeo
  * <%= wpde("Öffentlicher Bücherschrank") %> in der Wikipedia
  * <%= wpde("Liste öffentlicher Bücherschränke") %> in Deutschland, Österreich und der Schweiz
  * [Öffentliche Bücherschränke](http://www.tauschgnom.de/offene-buecherschraenke) auf Tauschgnom.de
  * [Blog der Bücherboxx](http://buecherboxx.wordpress.com/) in Berlin
