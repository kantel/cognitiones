#title "Regionalgeld"

<%= imageref("woerglgeldrueckseite", {:height => "278", :width => "480"}) %>

*Rückseite eines Arbeitswertscheins aus Wörgl, Österreich 1932/33*

## Literatur

  * Norbert Rost: *[Regionales Geld für ein Europa der Regionen](http://www.heise.de/tp/artikel/17/17289/1.html)*: Regionalwährungen könnten vor den negativen Globalisierungsfolgen schützen und die regionale Wirtschaft stärken, Telepolis News vom 3. Mai 2004 (siehe auch: [Europa der Regionen](Europa der Regionen))
  * Ulrich Steudel: *[Regionalgeld: Zahlungsmittel ohne Zinsen](http://www.deutsche-handwerks-zeitung.de/zahlungsmittel-ohne-zinsen/150/3093/174545/)*, Deutsche HandwerksZeitung vom 3. Juli 2012
  * Norbert Rost: *[Euro-Krise: Redundanz für das System](http://www.heise.de/tp/artikel/37/37376/1.html). Ökonomen fordern Diskussion über Parallelwährungen*, Telepolis News vom 2. August 2012

## Links

  * [Regionalgeld-Initiativen in Deutschland](http://www.regiogeld.de/home.html)
  * <%= wpde("Regiogeld") %> in der Wikipedia