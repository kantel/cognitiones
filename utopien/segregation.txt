#title "Segregation"

**Segregation** bezeichnet den Vorgang der Entmischung von unterschiedlichen Elementen in einem Beobachtungsgebiet. Man spricht dann von Segregation, wenn sich die Tendenz zu einer Polarisierung und räumlichen Aufteilung der Elemente gemäß bestimmter Eigenschaften beobachten läßt. Häufiges Beispiel bzw. Beobachtungsfeld in soziologischen, geographischen oder wirtschaftlichen Untersuchungen sind Segregationsprozesse innerhalb der Städte. Hier treten diese Prozesse deutlich zutage, da in der Stadt bezüglich bestimmter Merkmale (z. B. Einkommen, Ethnizität, Religion) sehr unterschiedliche Bevölkerungsgruppen zusammenleben. Auffällig ist nun, daß sich der städtische Raum entlang dieser Merkmale segregiert.

## Links

  * <%= wpde("Segregation (Soziologie)") %> in der Wikipedia

