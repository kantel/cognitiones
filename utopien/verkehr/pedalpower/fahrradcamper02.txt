#title "Camper Bike"

<%= imageref("dreirad-camper") %>

Aus der Ideenschmiede des Designers Kevin Cyr, der auch einen [Camper-Einkaufswagen](http://www.kevincyr.net/index.php?/ongoing/camper-kart/) entwickelt hat, kommt der Entwurf dieses **Camper Bikes**.

## Links

  * [Camper Bike Home](http://www.kevincyr.net/index.php?/project/camper-bike/)
  * [Small Mobile Homes: Bike Trailers & Shopping Cart Campers](http://dornob.com/small-mobile-homes-bike-trailers-shopping-cart-campers/)
  * [Das Dach über dem Kopf immer dabei](http://tiny-houses.de/minihaus-immer-dabei/) (Hier wird auch ein Camper auf der Basis des Piaggio Ape 50 vorgestellt)