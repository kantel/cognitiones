#title "Elektra One Solar"

<iframe width="560" height="315" src="http://www.youtube.com/embed/N1Xm_ii6pSE" frameborder="0" allowfullscreen></iframe>

Die **Elektra One Solar** ist ein Flugzeug der Ultra-Light-Klasse, das von einem Elektromotor angetrieben wird.

Das Flugzeug hat eine Spannweite von 11 Metern und bietet Platz für eine Person. Es besteht komplett aus einem Kohlefaserverbundstoff und ist entsprechend leicht: Es wiegt gerade mal 100 Kilogramm. Dazu kommt das Gewicht der Akkus, die noch einmal so viel wiegen. Die Nutzlast beträgt weitere 100 Kilogramm, das Maximalgewicht also 300 Kilogramm.

Doch anders als Bertrand Piccards [Solar Impulse](Solar Impulse), das allein mit dem Strom fliege, den die Solarzellen auf den Tragflächen erzeugen, sei Elektra One Solar auf Akkus angewiesen und soll eine Reichweite von 1.000 Kilometern haben.

## Links

  * [Homepage beim Hersteller PC-Aero](http://www.aircraft-certification.de/index.php/elektra-one-solar-28.html)
  * Golem.de: [Flugzeug mit Elektroantrieb soll tausend Kilometer fliegen](http://www.golem.de/news/elektra-one-solar-flugzeug-mit-elektroantrieb-soll-tausend-kilometer-fliegen-1209-94501.html)