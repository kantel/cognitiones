#title "Zukunft der Arbeit"

## In diesem Wiki

  * [Alternative Wirtschaftspolitik](Alternative Wirtschaftspolitik)
  * [Bedingungsloses Grundeinkommen](Bedingungsloses Grundeinkommen)

## Literatur

[[dr]]<iframe src="http://rcm-eu.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=derschockwell-21&o=3&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=3570501558" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe>[[dd]]

  * Ralf Grötker: *[Opfer der Automatisierung](http://www.heise.de/tp/artikel/40/40241/1.html), Studie prophezeit: 47 Prozent aller Jobs stehen vor dem Aus*, Telepolis vom 7. November 2013
