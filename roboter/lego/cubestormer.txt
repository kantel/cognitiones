#title "CubeStormer"

<iframe width="480" height="295" src="http://www.youtube.com/embed/eaRcWB3jwMo" frameborder="0" allowfullscreen></iframe>

Der **CubeStormer** will der schnellste [Lego Mindstorms](Lego Mindstorms) basierte Cube-lösender Roboter der Welt sein.

**[Update]**: Neuer Rekordhalter ist der [CubeStormer II](CubeStormer II) vom gleichen Entwicklerteam.

## In diesem Wiki

  * [LEGO Mindstorms Rubik's Cube Solver](LEGO Mindstorms Rubik's Cube Solver)
  * [CubeStormer II](CubeStormer II)
  