#title "CubeStormer II"


<iframe width="480" height="295" src="http://www.youtube.com/embed/_d0LfkIut2M" frameborder="0" allowfullscreen></iframe>

Ernö Rubiks Zauberwürfel hat seit den 1980er Jahren schon viele Tüftler zu Höchstleistungen angeregt. Nun haben Mike Dobson und David Gilday nach dem [CubeStormer](CubeStormer) wieder einen [Roboter](Roboter) konstruiert, den **CubeStormer II**, der die Lösung schneller findet und zusammendreht als der derzeitige menschliche Weltrekord (5,66 Sekunden). Er ist komplett mit Lego-Elementen gebaut, inklusive vierer [Mindstorms NXT-Kits](Lego Mindstorms). Das »Hirn« des Roboters ist ein Samsung Galaxy S II mit einem 1,2 GH Dual-Core ARM-Prozessor und dem neuen Android 2.3 Gingerbread Betriebssystem. Die Kommunikation zwischen dem Smartphone und den NXT erfolgt via Bluetooth. 