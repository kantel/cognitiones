#title "Lagerroboter von Kiva Systems"

<iframe width="560" height="315" src="//www.youtube.com/embed/6KRjuuEVEZs?rel=0" frameborder="0" allowfullscreen></iframe>

Die orangen Kiva-Roboter finden selbständig die bestellten Waren im Lager, holen die passende Verpackung und bringen beides zum Lagerarbeiter. Die Herstellerfirma *Kiva Systems* wurde im März 2012 [von Amazon gekauft](http://www.golem.de/news/amazon-kauft-kiva-systems-roboter-statt-menschen-im-lagerhaus-1203-90622.html).

## Links

  * [Kiva Systems Home](http://www.kivasystems.com/)
  * <%= wp("Kiva Systems") %> in der Wikipedia

