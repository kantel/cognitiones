#title "Raspberry Pi Pico"

Der **Raspberry Pi Pico** ist ein winziges Microcontroller-Board und basiert auf dem sparsamen 32-Bit RP2040-Chip der Raspberry Pi Foundation. Das Board läßt sich mit [Python](Python), [MicroPython](MicroPython) und (in [Visual Studio Code](Visual Studio Code)) mit [C](C) programmieren.

<iframe width="560" height="315" src="https://www.youtube.com/embed/o-tRJPCv0GA" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Der Raspberry Pi Pico zielt auf Projekte, für die ein Raspberry Pi zu groß, zu stromdurstig, zu kompliziert oder zu teuer ist. Der 5,1 Zentimeter lange und 2,1 Zentimeter breite Pico paßt auf Steckbretter zum einfachen Schaltungsaufbau. Er läßt sich per Micro-USB-Buchse programmieren und mit Energie versorgen, läuft sonst aber mit Spannungen ab 1,8 Volt -- also auch an einem Akku oder einer Batterie.

Das Pico-Platinchen mit 40 Anschlüssen stellt 26 GPIO-Pins mit 3,3 Volt bereit, darunter die üblichen Funktionen wie I²C, SPI, UART, PWM und ADC sowie Timer.

## Video: Was ist ein Microcontroller?

<iframe width="560" height="315" src="https://www.youtube.com/embed/ZY-HrRGCQ4w" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Tutorials

- *Gareth Halfacree*: »[Programming Raspberry Pi Pico with Python and MicroPython](https://magpi.raspberrypi.org/articles/programming-raspberry-pi-pico-with-python-and-micropython)«

- *Gareth Halfacree*: »[How to solder GPIO pin headers to Raspberry Pi Pico](https://magpi.raspberrypi.org/articles/how-to-solder-gpio-pin-headers-to-raspberry-pi-pico)«.

## Links

- [Ankündigung des Raspberry Pi Pico](https://www.raspberrypi.org/blog/raspberry-pi-silicon-pico-now-on-sale/) (und Spezifikation) im Raspberry Pi Blog.