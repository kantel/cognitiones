#title "Ars Magna Lucis et Umbrae"

[Athanasius Kircher](Athanasius Kircher) über die *Große Kunst von Licht und Schatten*:

<a href="http://www.flickriver.com/photos/bibliodyssey/sets/72157607045788386/"><img src="http://www.flickriver.com/badge/user/set-72157607045788386/recent/shuffle/medium-horiz/ffffff/333333/85009674@N00.jpg" border="0" alt="peacay - View my 'Ars Magna Lucis et Umbrae (Kircher)' set on Flickriver" title="peacay - View my 'Ars Magna Lucis et Umbrae (Kircher)' set on Flickriver"/></a>

## Links

  * BibliOdyssey: [The Great Art of Light and Shadow](http://bibliodyssey.blogspot.com/2008/09/ars-magna-lucis-et-umbrae.html)