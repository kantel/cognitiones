#title "Museum Kircherianum"

Das Museum **Kircherianum** wurde vor dreieinhalb Jahrhunderten in Rom eingerichtet. Es trägt den Namen des Universalisten [Athanasius Kircher](Athanasius Kircher). Dieser war zwar nicht sein Gründer, aber sein erster Kustos.

<%= imageref("museum-kircherianum", {:width => "480", :height => "525"}) %>

Das Museum erwuchs aus der 1650 testamentarisch dem Collegium Romanum vermachten umfangreichen Sammlung ethnologischer und antiker Funde des römischen Senatssekretärs Alfonso Donnino. Für die Unterbringung der Objekte wurde 1651 ein bis dahin offener Arkadengang im Collegium Romanum zugemauert und Kircher übernahm die Aufgabe, die Sammlung zu ordnen und zu verwalten.

Das Museum wurde 1870 vom italienischen Staat übernommen und 1915 aufgelöst, wobei die einzelnen Bestände auf verschiedene Museen Roms aufgeteilt wurden.

Die Seiten in diesem Wiki tragen den Namen *Museum Kircherianum*, weil sie ein Sammlung der Werke des großen Universalgelehrten Athanasius Kircher werden sollen.

## In diesem Wiki

[[dr]]<iframe src="http://rcm-de.amazon.de/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=derschockwell-21&o=3&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=0226924149" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe>[[dd]]

  * [Ars Magna Lucis et Umbrae](Ars Magna Lucis et Umbrae)
  * [Insulae Atlantis](Insulae Atlantis)
  * [Katzenklavier](Katzenklavier), 1650
  * [Mundus Subterraneus](Mundus Subterraneus)
  * [Panacusticon](Panacusticon)
  * [Turris Babel](Turris Babel)

## Literatur

  * [Athanasius Kircher and the Hyroglyphic Sphinx](http://publicdomainreview.org/2013/05/16/athanasius-kircher-and-the-hieroglyphic-sphinx/)

## Links

  * Das <%= wpde("Museum Kircherianum") %> in der Wikipedia