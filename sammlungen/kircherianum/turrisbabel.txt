#title "Turris Babel"

<%= imageref("babel", {:width => "480", :height => "681"}) %>

[Kirchers](Athanasius Kircher) Erklärung, warum der Turmbau zu Babel nicht den Mond erreichen konnte.

## Quelle

  * Athanasius Kircher: *Turris Babel*, Amsterdam, 1679

## Links

  * Deutsches Museum: [Turris Babel](http://www.deutsches-museum.de/bibliothek/unsere-schaetze/architektur/kircher/) war im Oktober 2000 »Buch des Monats«