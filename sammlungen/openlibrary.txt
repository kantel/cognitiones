#title "Open Library"

<%= imageref("logoOL") %>

Die **Open Library** ist ein Projekt zur kollaborativen Erstellung einer auf einer bibliographischen Datenbank basierenden Online-Bibliothek. Selbsterklärtes Ziel der Open Library ist es, eine eigene Webseite für jedes aller bislang veröffentlichen Bücher zu schaffen. In vielen Fällen wird dabei über den bibliographischen Nachweis hinaus auch der Zugang zum Digitalisat des jeweiligen Buchtitels mit hinterlegtem Volltext ermöglicht. Dazu hat die Open Library einen eigenen Viewer, den [Internet Archive BookReader](Internet Archive BookReader) entwickelt und unter einer freien Lizenz zur Verfügung gestellt.

Als Teilprojekt des [Internet Archive](Internet Archive) wurde die Open Library maßgeblich von *Brewster Kahle* initiiert und ist am 16. Juli 2007 offiziell ins Leben gerufen worden.

## Links

  * [Open Library Home](http://openlibrary.org/)
  * Die <%= wpde("Open Library") %> in der Wikipedia