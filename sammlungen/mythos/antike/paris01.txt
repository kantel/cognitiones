#title "Claude Lorrain: Das Urteil des Paris"

<%= imageref("lorrain-paris", {:width => "560", :height => "373"}) %>

<%= wpde("Claude Lorrain") %> (um 1600 bis 1682), eigentlich *Claude Gellée*, wurde nach seiner geographischen Herkunft *Lorrain* (*Lothringer*) genannt. Er war einer der bedeutendsten Landschaftsmaler des Barocks. 2012 widmete ihm das Frankfurter [Städel-Museum](http://www.staedelmuseum.de/sm/) eine umfangreiche Ausstellung: [Die verzauberte Landschaft](http://erhard-metz.de/2012/02/03/ein-meister-und-einzelganger-in-rom-claude-lorrain/).

