#title "François-Édouard Picot: Amor und Psyche"

<%= imageref("cupid-psyche-1817", {:width => "560", :height =>"447"}) %>

<%= wpde("François-Édouard Picot") %> (* 17. Oktober 1786 in Paris; † 15. März 1868 ebenda) war ein französischer Historien- und Porträtmaler und Lithograph. Er begründete seinen Ruf 1819 mit obigem großen Gemälde *Amor und Psyche*.