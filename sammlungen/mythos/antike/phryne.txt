#title "Jean-Léon Gérôme: Phryne vor dem Areopag (1861)"

<%= imageref("phryne", {:width => "560", :height => "350"}) %>

<%= wpde("Phryne") %> war eine berühmte griechische <%= wpde("Hetäre") %>. Angeblich konnte kein Mann ihren Reizen widerstehen. Aufgrund ihrer Anmaßung, ihre Schönheit könne mit derjenigen der Göttin <%= wpde("Aphrodite") %> mithalten, wurde Phryne der <%= wpde("Asebie") %> (Gottlosigkeit) angeklagt. Der Legende nach soll Phryne vor dem <%= wpde("Areopag") %> von ihrem Liebhaber und Anwalt, dem Politiker <%= wpde("Hypereides") %> entkleidet und daraufhin freigesprochen worden sein. Diese Szene stellte der französische Maler [Jean-Léon Gérôme](Jean-Léon Gérôme) 1861 in seinem Bild *Phryne vor dem Areopag* (*»Phryné devant l’Aréopage«*) dar.
