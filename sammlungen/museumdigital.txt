#title "Museum-Digital"

Die Plattform **Museum-Digital** startete 2009 als Pilotprojekt und beinhaltet mittlerweile (April 2014) über 33.000 Exponate. Die Objekte werden in der Regel mit Photo und einer ausführlichen Beschreibung vorgestellt.

## Links

  * [Museum-Digital Startseite](http://www.museum-digital.de/)
