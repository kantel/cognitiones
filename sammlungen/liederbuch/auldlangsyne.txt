#title "Auld Lang Syne"

<%= imageref("auldlangsyne", {:width => 560, :height => "286"}) %>

1.  
Should auld acquaintance be forgot  
And never brought to min'?  
Should auld acquaintance be forgot  
And days of auld lang syne?  
**Refrain**:  
For auld lang syne, my dear  
For auld lang syne  
We'll tak' ae cup o'kindness yet  
For auld lang syne

2.  
We twa hae rin aboot the braes  
And pud the gowans fine  
But we've wander'd mony a weary step  
Sin' auld lang syne.

3.  
We twa hae paidl'd in the burn  
Frae morning sun till dine  
But seas between us braid hae roar'd  
Sin' auld lang syne.

4.  
And surely ye'll be your pint-stowp  
And surely I'll be mine  
And we'll tak ae cup o'kindness yet  
For auld lang syne.

5.  
And there's a haund, my trusty fiere  
And gie's a haund o' thine  
And we'll tak' ae richt guid wullie- waught  
For auld lang syne.

## Spielhinweis

Wem G#<sup>7</sup> zu schwierig zu greifen ist, der kann an diesen beiden Stellen auch einfach H<sup>7</sup> weiterspielen.

## Herkunft

Das Lied **Auld Lang Syne** wird traditionsgemäß in allen anglophilen Ländern zum Ende des Jahres gesungen, um an die Verstorbenen des zu Ende gegangenen Jahres zu erinnern. Der Text schrieb der schottische Dichter *Robert Burns* (1759-1796), die Melodie basiert auf einem alten schottischen Volkslied, das von Burns an seinen Text angepaßt wurde.

Eine deutsche Übersetzung von Claus Ludwig Laue (*Nehmt Abschied, Brüder …*) entstand für die Pfadfinderschaft St. Georg. Dieser deutsche Text unterliegt noch dem Urheberrecht.

In der [Wikibooks-Liederbuchsammlung](http://de.wikibooks.org/wiki/Liederbuch:_Auld_Lang_Syne) gibt es eine Notenfassung, in der seltsamerweise das Lied im 3/4-Takt gesetzt ist.

## Links

  * <%= wpde("Auld Lang Syne") %> in der Wikipedia
  * [Auld Lang Syne im Wikibooks-Liederbuch](http://de.wikibooks.org/wiki/Liederbuch:_Auld_Lang_Syne)
  * <%= wpde("Robert Burns") %> in der Wikipedia

---

Gesetzt mit [LilyPond](LilyPond) von Jörg Kantel.
