#title "Lied der Petroleure"

*(Melodie: Mamsell Angôt)*

Wir sind die Petroleure,  
Da weiß ja jedermann;  
Drum tun wir alle Ehre  
Dem Petroleum an.  
Und weil's so schön zum Brennen ist  
Und uns viel Licht verschafft,  
Sei auch Petrol zu dieser Frist  
Uns dieser Gerstensaft!

Hier Petroleum, da Petroleum,  
Petroléum um und um!  
Laßt die Humpen frisch voll pumpen:  
Dreimal hoch — Petroleum!

Philister rümpft die Nase  
Und meint, es riecht nicht gut,  
Schimpft hinter seinem Glase  
Uns »Sozialistenbrut«.  
Er liest im Geldsacksblatt sich dumm,  
Glaubt, was er liest, sei wahr —  
Brenn heller, lieb Petroleum,  
Mach ihm den Standpunkt klar!

Hier Petroleum, da Petroleum,  
Petroléum um und um!  
Laßt die Humpen frisch voll pumpen:  
Dreimal hoch — Petroleum!

Schon brennt es in den Städten  
So licht und frank und frei;  
Man spürt, daß es vonnöten  
Auch auf den Dörfern sei;  
Selbst im Verborgnen leuchtet schon,  
Man ist vor Staunen stumm,  
Trotz Sub- und Ordination,  
Hell das Petroleum!

Hier Petroleum, da Petroleum,  
Petroléum um und um!  
Laßt die Humpen frisch voll pumpen:  
Dreimal hoch — Petroleum!

Und ob auch trüb die Zeiten,  
Wir wollen treu vereint  
Stets mutig vorwärts schreiten,  
Ist mächtig auch der Feind.  
Und sperrt der Bruder Staatsanwalt  
Auch einmal einen ein,  
Kriegt's Petroléum mehr Gehalt  
Und brennt noch mal so rein!

Hier Petroleum, da Petroleum,  
Petroléum um und um!  
Laßt die Humpen frisch voll pumpen:  
Dreimal hoch — Petroleum!

Petroleum-Genossen,  
Ihr Brüder, wanket nicht!  
Tut jeder unverdrossen  
Der Petroleuren-Pflicht!  
Wir kümmern uns den Kuckuck um  
Die Liberalerei,  
Das Wahlrecht und Petroleum  
Ist unser Feldgeschrei

Hier Petroleum, da Petroleum,  
Petroléum um und um!  
Laßt die Humpen frisch voll pumpen:  
Dreimal hoch — Petroleum!

---

*Text: [Jakob Audorf](http://de.wikipedia.org/wiki/Jacob_Audorf) (1835 - 1898), Maschinenbauer, Aktivist der Arbeiterbewegung und Redakteur, unter anderem im »Hamburg-Altonaer Volksfreund« und im »Hamburger Echo«. Quelle: »Des Morgens erste Röte«. Frühe sozialistische deutsche Literatur 1860 - 1918, herausgegeben vom Zentralinstitut für Literaturgeschichte der Akademie der Wissenschaften der DDR, Leipzig (Verlag Philipp Reclam jun.) 1982.*