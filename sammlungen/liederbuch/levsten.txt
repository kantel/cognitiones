#title "Dat du min Levsten büst"

<%= imageref("levsten", {:width => "560", :height => "186"}) %>

2\.  
Kumm du um Mitternacht,  
kumm du Klock een!  
Vader slöpt,  
Moder slöpt,  
ick slaap alleen.  
Vader slöpt …

3\.  
Klopp an de Kammerdör,  
fat an de Klink!  
Vader meent,  
Moder meent,  
dat deit de Wind.  
Vader meent …

Text: *Klaus Groth*, (1819-1899), Melodie: Plattdeutsche Volksweise.

---

Gesetzt mit [LilyPond](LilyPond) von Jörg Kantel.

