#title "Rally Obedience"

<iframe width="560" height="315" src="https://www.youtube.com/embed/Erh7lkNbn8Y?rel=0" frameborder="0" allowfullscreen></iframe>

*Joey und ich auf dem Rally Obedience Turnier bei Jugend und Hund e.V. am 21. Juni 2015, Start in der Klasse 2, Richterin Angelika Just, 82 Punkte und damit ein SG (Sehr Gut).* *[Aufnahme: Walter Ohnesorge]*

**Rally Obedience** (RO) ist eine relativ junge Sportart. Die Prüfungsordnung ist seit 01.10.2012 gültig und wurde zum 01.01.2014 überarbeitet. Rally Obedience ist für fast jeden Hund und Hundeführer geeignet. Spaß für Mensch und Hund stehen hier eindeutig im Vordergrund sowie die perfekte Kommunikation und die partnerschaftliche Zusammenarbeit des Mensch-Hund-Teams. RO kommt aus Amerika und wurde dort vor einigen Jahren entwickelt. Während diese Sportart dort schon eine große Anhängerschaft erobert hat, ist sie in Deutschland noch relativ unbekannt. 

Ein RO-Parcours besteht aus mehreren Stationen, diese bestehen aus Schildern, die dem jeweiligen Team angeben, was zu tun ist und in welche Richtung es weitergeht. Der Hundeführer nimmt nun seinen Hund bei Fuß und arbeitet den Parcours möglichst schnell und präzise ab.

Hund und Mensch dürfen während ihrer Arbeit ständig miteinander kommunizieren. Der Hund darf jederzeit angesprochen, motiviert und verbal gelobt werden.

Parcours-Übungen sind zum Beispiel Sitz, Platz, Steh, aber auch Kombinationen aus diesen Elementen. Es gibt Richtungsänderungen um 90°, 180° und 270° nach rechts und links, außerdem 360° Kreise nach rechts und links. Andere Stationen fordern auf, einen Slalom um Pylonen zu machen oder den Hund über eine Hürde voraus zu senden oder abzurufen. Es fehlen natürlich auch nicht die Bleib- oder Abrufübungen. Eine Besonderheit dürfte auch die Übung zur Futterverweigerung sein, bei der das Team an gefüllten Futternäpfen vorbei gehen muß, ohne daß der Hund sich davon ablenken läßt.

Mein kleiner Sheltie Joey und ich betreiben Rally Obedience seit Anfang 2014, wir starten zur Zeit (Sommer 2015) in der Klasse RO 2. Seit April 2014 biete ich zusammen mit einer Co-Trainerin auch Rally-Obedience-Training beim HSV Plänterwald an.

[[dr]]<iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//ws-eu.amazon-adsystem.com/widgets/q?ServiceVersion=20070822&OneJS=1&Operation=GetAdHtml&MarketPlace=DE&source=ss&ref=ss_til&ad_type=product_link&tracking_id=derschockwell-21&marketplace=amazon&region=DE&placement=3840420245&asins=3840420245&linkId=LJR5NAM2ZRA7MRGB&show_border=true&link_opens_in_new_window=true"></iframe>[[dd]]

## Links

  * [Rally Obedience beim SGSV Berlin-Brandenburg](http://sgsv-lvbb.de/index.php/rally-obedience/was-ist-rally-obedience)
  * [Rally Obedience beim VDH](http://www.vdh.de/hundesport/rally-obedience/)
  * [Rally Obedience beim HSV Plänterwald](http://hsv-plaenterwald.jimdo.com/unsere-sportarten/rally-obedience/)
  * [TrainerInnen-Seite des HSV Plänterwald](http://hsv-plaenterwald.jimdo.com/über-uns/trainer/)
  * [Rally Obedience Turnierkarte](http://www.vdh.de/tl_files/media/pdf/ag_aktuell/RO_Turnierkarte.pdf) (<%= imageref("pdf") %>)
  * <%= wp("Rally Obedience") %> in der englischsprachigen Wikipedia