#title "Grup Yorum"

<iframe width="560" height="315" src="http://www.youtube.com/embed/hM9ueSJcKH0?rel=0" frameborder="0" allowfullscreen></iframe>

**Grup Yorum** ist eine türkische Band, die vor allem für ihre politischen Liedtexte bekannt ist. Sie wurde von politischen Musikbands wie Inti Illimani aus Chile oder Ruhi Su aus der Türkei inspiriert und 1985 gegründet. Die Musikgruppe gehört zum sozialistisch-revolutionären Spektrum und war vielfacher staatlicher Verfolgung ausgesetzt. Mitglieder wurden festgenommen, verhaftet und vor Gericht gestellt und Alben wurden verboten.

## Links

  * [Offizielle Website der Gruppe](http://www.grupyorum.net/)
  * <%= wpde("Grup Yorum") %> in der Wikipedia
  * [Türkei verhaftet Musiker](http://www.jungewelt.de/2013/01-21/053.php), jungeWelt vom 21. Januar 2013
