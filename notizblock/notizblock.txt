#title "Notizblock"

<%= imageref("moleskine") %>

Hier kommt (vorübergehend) alles rein, was in keine der (bisher) angelegten Kategorien paßt, zum Beispiel dieses: [Horse Agility](Horse Agility)

## Interna

  * [Localhost-Konfiguration](Localhost-Konfiguration)

## Bücher

  * [Buchbesprechungen](Bücher), die sonst nirgendwo hinpaßten.

## (Politische) Musik

  * Hat eine [eigene Seite](Musik) bekommen

## Verschwörungstheorien und anderer esoterischer Blödsinn

  * [Chemtrails](Chemtrails)
  * [Kopp Verlag](Kopp Verlag)

## Religionskritik

  * Die [wunderbaren Seiten](http://www.payer.de/relkritiklink.htm) von Alois Payer

## Texte und Gereimtes

  * [Ode an George Bryan Brummell](Ode an George Bryan Brummell)

## Vermischtes

  * [Agility](Agility)
  * [Rally Obedience](Rally Obedience)
  * [Nilpferd](Nilpferd)

## (Link-) Sammelsurium zu Berlin und Brandenburg

  * Klaus Kurpjuweit, Ralf Schönball: *[Berliner Flughäfen - einst und jetzt: Herausforderung für Stadtplaner](http://www.tagesspiegel.de/berlin/berliner-flughaefen-einst-und-jetzt-herausforderung-fuer-stadtplaner/6300112.html)*, Tagesspiegel vom 7. März 2012
  * Jens Blankennagel: *[Zeit, dass sich was dreht](http://www.berliner-zeitung.de/berlin/denkmalschutz-zeit--dass-sich-was-dreht,10809148,14637988.html)*, Berliner Zeitung vom 3. April 2012, Webseite der Mühle: [Förderverein Historische Mönchmühle e.V.](http://www.mönchmühle.de/)
