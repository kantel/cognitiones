#title "Localhost-Konfiguration"

Da ich langsam die Übersicht verliere, hier die Ports meiner lokalen Webserver:

## Auf meinem Laptop

  * [Zope](Zope)/[Plone](Plone): Port 8080
  * [Railo](Railo): Port 8088
  * [MAMP](MAMP)/Apache: Port 8888
  * [MAMP](MAMP)/MySQL: Port 8889
  * [web2py](web2py): Port 8000
  * [OPML Editor](OPML Editor): Port 5337
  * [Processing.js](ProcessingJS): Port > 50000