#title "Curriculum Vitae"
#updated "15. Januar 2023"

<%= imageref("kantel") %>

*(Photo: Norbert Spitzer)*

## Lebenslauf

**Jörg Kantel**, Jahrgang 1953, verheiratet. Berufe (in zeitlicher Reihenfolge): Speditionskaufmann, Gitarrist, Schüler (Zweiter Bildungsweg), Student ([Mathematik](Mathematik), Philosophie, Informatik), Programmierer, Kabarettist, Systembetreuer, Systemanalytiker, Unternehmensberater, EDV-Leiter, Dozent für EDV (Kids und Erwachsene). Seit Mai 1994 ist er EDV-Leiter am [Max-Planck-Institut für Wissenschaftsgeschichte](http://www.mpiwg-berlin.mpg.de/) und war von 2006 bis 2009 Lehrbeauftragter für Multimedia im Fachbereich »Angewandte Informatik« an der Fachhochschule für Technik und Wirtschaft (FHTW) Berlin.

[Mein Profil auf XING](https://www.xing.com/app/profile/?name=Joerg_Kantel&tab=4)

## Ehrungen

<a class="imagelink" href="http://www.computerwoche.de/top-100/"><%= imageref("top-100-it-koepfe", :align => "right") %></a>

Im August 2011 wurde Jörg Kantel von der Redaktion der [Computerwoche](http://www.computerwoche.de/top-100/) zu den [100 bedeutendsten Persönlichkeiten der IT](http://www.computerwoche.de/top-100/40268/) gewählt.

Im Februar 2012 wurde Jörg Kantel von der Jury des Vereins »[Die Wiener Atheisten, gegr. 1993 e.V.](http://www.wiener-atheisten.at/)« zum Ehrenmitglied ernannt. Es sollte damit sein spezielles Engagement -- besonders im Zusammenhang mit der *»[Giordano Bruno](Giordano Bruno) Party 2012«* -- gegen die gewaltsame Verbreitung von Gottesvorstellungen gebührend gewürdigt werden.

## Motto

*Sei Sand im Getriebe der Zeit.*

## Web-Aktivitäten (Auswahl)

### Aktive Seiten

  * Der [Schockwellenreiter](http://www.schockwellenreiter.de/), mein Weblog
  * [Cognitiones Publicae](Startseite), dieses Wiki
  * Mein [Testbett](http://www.testbett.de/), hier teste ich, was im Web alles möglich ist
  * Soziales: [Facebook](http://www.facebook.com/joerg.kantel), [Twitter](http://twitter.com/jkantel), [Xing](https://www.xing.com/app/profile/?name=Joerg_Kantel&tab=4), [Flickr](http://www.flickr.com/photos/schockwellenreiter/) und [YouTube](http://www.youtube.com/kantel) 
  * Mein [Bilderbuch auf Posterous](http://kantel.posterous.com/)

### Zur Zeit im Hiatus oder im Testbetrieb

  * Der [Rollberg](http://www.rollberg.de/), eine Website zur Ehrenrettung der »Bronx von Berlin« (Spiegel)
  * Die [Nigeria-Connection](http://www.nigeria-connection.de/), eine Website über *Nigerian Fraud Scam*
  * [Pythonmania.de](http://www.pythonmania.de/) (über Python-Programmierung)
  * Das [Agility-Blog](http://www.agilityblog.de/), meine fröhlichen Hundesportseiten
  * [Frontierweb.de](http://www.frontierweb.de/)
  * Und last but not least habe ich immer noch [Spaß-mit-Mathematik](http://www.spass-mit-mathematik.de/).

Eine (fast) vollständige Liste meiner Webaktivitäten [gibt es hier](http://www.cognitiones.de/doku.php/unsere_arbeiten).

## Veröffentlichungen (Auswahl)

<%= imageref("strukturtapete", {:border => "0", :width => "480", :height => "360", :alt => "Strukturtapete"}) %>

*(Photo: Gabriele Kantel)*

  * *[Programmieren in Hypertalk](http://www.kantel.de/hc/index.html)*, MacOpen 4/96 bis 11/96
  * MacMuPAD (Review), MacOpen 11/96
  * *Zur Verwaltung großer Datenmengen im WWW. The Virtual Laboratory for Physiology* in: *Internet- und Intranet-Technologien in der wissenschaftlichen Datenverarbeitung*, eds., Friedbert Kaspar & Hans-Ullrich Zimmermann, (Göttingen: Gesellschaft für wissenschaftliche Datenverarbeitung, 1999), pp. 3-25 (mit [Sven Dierig](http://vlp.mpiwg-berlin.mpg.de/exp/dierig/index.html))
  * *The Virtual Laboratory for Physiology. A Project in Digitalising the History of Experimentalisation of Nineteenth-Century Life Sciences*,  Max-Planck-Institut für Wissenschaftsgeschichte, ed., Preprint Nr. 140, Berlin 2000 (mit [Sven Dierig](http://vlp.mpiwg-berlin.mpg.de/exp/dierig/index.html) & [Henning Schmidgen](http://vlp.mpiwg-berlin.mpg.de/exp/schmidgen/index.html)) [Download PDF-File](http://www.mpiwg-berlin.mpg.de/Preprints/P140.PDF) (6,0 MB)
  * [Der Schockwellenreiter im Interview](http://www.krit.de/apfel-jk.shtml), [KriT-Journal](http://www.krit.de/), August 2000
  * *Radio UserLand*, Bloghaus, März 2002
  * *[Archäologie des Bloggens](http://www.schockwellenreiter.de/categories/medien/artikel/archaeologie.html). Ein erster und unvollständiger Versuch, dem »Phänomen Weblog« mittels einer Geschichte seiner einzelnen Teile näher zu kommen*, in: Texte zur Wirtschaft, 25.09.2002
  * *[Liebesbrief an Neukölln](http://www.zeit.de/online/2006/14/Neukoelln?page=all)*, ZEIT online, 4. April 2006
  * *Ping, Tags und Social Software -- Communitybildung und Medienkonvergenz durch neue Publikationsformen im Internet*, in: Prof. Dr. Ing. Klaus Rebensburg: *Workshop »Informationsgesellschaft« der Stiftungskollegiaten des SVK Berlin, 12. - 14. Mai 2006, Reichenow bei Berlin*, Stuttgart (Alcatel SEL Stiftung für Kommunikationsforschung) 2006, S. 219 - 234 ([Download als PDF](http://www.schockwellenreiter.de/gems/socsoft.pdf), 1,2 MB)
  * *Werblogs und Journalismus -- 5 Thesen zu einem Mißverständnis*, Wiesbaden, 8. September 2006 ([Download als PDF](http://www.schockwellenreiter.de/gems/5thesen.pdf), 96 KB)
  * *Web 2.0: Werkzeuge für die Wissenschaft*, in: Klaus Hermann, Jörg Kantel (Hrsg.): *23. DV-Treffen der Max-Planck-Institute, 15. - 17. November 2006 in Berlin*, GWDG-Bericht Nr. 71, Göttingen (GWDG) 2007, S. 3 - 39 ([Download als PDF](http://www.schockwellenreiter.de/gems/web2science.pdf), 3,0 MB)
  * *RSS und Atom kurz & gut*, Köln (O'Reilly) 2007
  * *Ping, Tags und Social Software -- Communitybildung und Medienkonvergenz durch neue Publikationsformen im Internet*, aktualisierte Fassung, in: Jürgen Ertelt/Franz Josef Röll (Hrsg.): *Web 2.0: Jugend Online als pädagogische Herausforderung*, München (kopaed) 2008, S. 21 - 38
  * *Local.tv is here — Entwuf eines unabhängigen, lokalen Internet-TV*, in: Jürgen Ertelt/Franz Josef Röll (Hrsg.): *Web 2.0: Jugend Online als pädagogische Herausforderung*, München (kopaed) 2008, S.219 - 243
  * *Per Anhalter durch das Mitmach-Web. Publizieren im Web 2.0: Von Social Networks über Weblogs und Wikis zum eigenen Internet-Fernsehsender*, Heidelberg (mitp) 2009
  * *[Das iPad ist nur eine Fernbedienung](http://www.faz.net/s/RubCEB3712D41B64C3094E31BDC1446D18E/Doc~EE59A1D3A35D848BC99794C961B9F5D73~ATpl~Ecommon~Scontent.html)*, Frankfurter Allgemeine Zeitung (FAZ) vom 3. Februar 2010, Nr. 28, Seite 32
  * *[3D-Scans von Keilschrifttafeln – ein Werkstattbericht](http://www.scribd.com/doc/22990162/3D-Scans-von-Keilschrifttafeln-Ein-Werkstattbericht)* (zusammen mit [Peter Damerow](Peter Damerow), Sarah Köhler und Christina Tsouparopoulou), in: Wolfgang Assmann, Christa Hausmann-Jamin, Frank Malisius (Hrsg.): *26. DV-Treffen der Max-Planck-Institute, 22. - 24. September 2009 in Berlin*, GWDG-Bericht Nr. 76, Göttingen (GWDG), 2010, S. 41 - 62 
  * *[Lernt programmieren, sonst werdet ihr programmiert! Über die Emanzipation von der Software-Industrie](http://www.dradio.de/dkultur/sendungen/politischesfeuilleton/1304444/)*, Deutschlandradio Kultur - Politisches Feuilleton, gesendet am 27. Oktober 2010
  * *[Geld schafft sich ab im Netz! Digitalisiertes Wissen wird zum Gemeingut](http://www.dradio.de/dkultur/sendungen/politischesfeuilleton/1325130/)*, Deutschlandradio Kultur - Politisches Feuilleton, gesendet am 23. November 2010
  * *[Schafft viele Wikileaks!](http://www.fr-online.de/politik/meinung/schafft-viele-wikileaks-/-/1472602/4917202/-/index.html)*, Frankfurter Rundschau vom 13. Dezember 2010
  * *[Der Cyber-War findet nicht statt](http://www.dradio.de/dkultur/sendungen/politischesfeuilleton/1358936/)*, Deutschlandradio Kultur - Politisches Feuilleton, gesendet am 7. Januar 2011
  * *[Publizier Dein Buch selbst. Texte gehören frei verfügbar ins Netz!](http://www.dradio.de/dkultur/sendungen/politischesfeuilleton/1388242/)*, Deutschlandradio Kultur - Politisches Feuilleton, gesendet am 15. Februar 2011
  * *[Versenkte Notebooks. Der Bundestrojaner: Polizeiliche Ermittlungen auf Kosten des Vertrauens der Bürger](http://www.dradio.de/dkultur/sendungen/politischesfeuilleton/1410106/)*, Deutschlandradio Kultur - Politisches Feuilleton, gesendet am 16. März 2011
  * *[Open Access: Wissenschaftliche Erkenntnisse müssen im Internet frei verfügbar sein!](http://www.dradio.de/dkultur/sendungen/politischesfeuilleton/1450138/)* Deutschlandradio Kultur - Politisches Feuilleton, gesendet am 5. Mai 2011
  * *[Digitaler Wissensbetrieb: Nomaden des Wissens](http://www.theeuropean.de/joerg-kantel/6833-web-des-wissens)*, The European vom 5. Juni 2011
  * *www.maxmuellerluedenschuh oder: Wem gehört das Internet?*, Lunapark21, [Heft 14](http://www.lunapark21.de/archiv/archiv_lp21.html#14/2011), Sommer 2011, Seite 68-70
  * *[Wem gehört das Internet?](http://www.dradio.de/dkultur/sendungen/politischesfeuilleton/1497834/)* Deutschlandradio Kultur - Politisches Feuilleton, gesendet am 6. Juli 2011
  * *[Klischee, ick hör dir trapsen!](http://www.dradio.de/dkultur/sendungen/lesart/1533711/)* Rezension zu *Richard A. Clarke: World Wide War. Angriff aus dem Internet, Hoffmann und Campe Verlag, 2011*, Deutschlandradio Kultur - Lesart, gesendet am 21. August 2011

## Vorträge (Auswahl)

<%= imageref("dsc05280") %>

*(Photo: Heiko Hebig, Mai 2003)*


  * *»Experimental Systems in 19th Century Life Sciences: The Virtual Institute for Physiology«*, Max-Planck-Institut für Wissenschaftsgeschichte, März 1998 (mit [Sven Dierig](http://vlp.mpiwg-berlin.mpg.de/exp/dierig/index.html))
  * *Hypermediale Wissensanordnung*, Tagung HyperKult VII, Computer als Medium, Universität Lüneburg, Lehrstuhl für Kulturinformatik, 18. Juli 1998 (mit [Sven Dierig](http://vlp.mpiwg-berlin.mpg.de/exp/dierig/index.html))
  * *Texte, Bilder, Animationen. Zur Verwaltung großer Datenmengen im WWW*, Gesellschaft für wissenschaftliche Datenverarbeitung, Göttingen, 19. November 1998 (mit [Sven Dierig](http://vlp.mpiwg-berlin.mpg.de/exp/dierig/index.html))
  * [The Virtual Lab](http://sloan.stanford.edu/SloanConference/papers/mpi/index.html), Conference of the Alfred P. Sloan Foundation on *[Using the World Wide Web for Historical Research in Science and Technology](http://sloan.stanford.edu/SloanConference/)*, Stanford University, 21. August 1999 (mit [Sven Dierig](http://vlp.mpiwg-berlin.mpg.de/exp/dierig/index.html) & [H. Schmidgen](http://vlp.mpiwg-berlin.mpg.de/exp/schmidgen/index.html))
  * *»Virtuelles Laboratorium, virtuelle Institute - was bringt die Zukunft der Web-Technologie?«*, Fachtag Informations- und Kommunikationstechnologien, Landesinstitut für Schule, Bremen, 18. - 19. Oktober 1999
  * *»XML - Die neue Sprache des Web?«*, Gesellschaft für wissenschaftliche Datenverarbeitung, Göttingen, 17. - 19. November 1999
  * *Ein Blick zurück: HyperCard - Start ins Multimedia-Zeitalter*, 16C3 Chaos Communication Congress des Chaos Computer Clubs (CCC), Berlin, 27. - 29. Dezember 1999
  * *»HTML und XML - Eine Einführung«*, XXIII. Bibliotheksleiter/innenTagung der Max-Planck-Gesellschaft, München, 8. - 10. Mai 2000
  * *Webtechniken für Historiker* -- Gastvortrag am [Helmholtz-Zentrum für Kulturtechnik](http://www2.hu-berlin.de/kulturtechnik/sammlungen.php), 16. Mai 2002.
  * *Das virtuelle Laboratorium am MPIWG*, Vortrag am [Multimedia Lehr- und Lernzentrum im CSM der HU Berlin](http://www.cms.hu-berlin.de/dl/multimedia/bereiche/mlz), 4. Dezember 2002.
  * *»Vom Weblog lernen... Community, Peer-to-Peer und Eigenständigkeit als ein Modell für zukünftige Wissenssammlungen«*. Keynote, gehalten auf der [BlogTalk 2003](http://2003.blogtalk.net/) in Wien, veranstaltet von der Donau-Universität Krems, 23. - 24. Mai 2003.
  * *Modell für Wissenssammlungen*, Gastvortrag im Rahmen der Verbundvorlesung ::collabor:: Kooperatives Lernen und Publizieren der FHTW-Berlin, der Universität Linz und der Universität Salzburg, 15. Dezember 2003.
  * *Zope und Plone — Open Source Content Management for the Rest of Us*, Vortrag, gehalten auf dem [20. Chaos Communication Congress](http://www.ccc.de/congress/2003/index.de.html), Berlin, 27. - 29. Dezember 2003.
  * *Weblogs und Journalismus -- 5 Thesen zu einem Mißverständnis*, Vortrag zur Konferenz »Chefsache Issues Management: Die neuen Meinungsmacher« am 8. September 2006 in der Hessischen Staatskanzlei Wiesbaden ([Download als PDF](http://www.schockwellenreiter.de/gems/5thesen.pdf), ca. 100 KB)

<div style="float:right; margin-left: 8px;"><%= imageref("mg_6222", {:border => "0", :width => "300", :height => "450", :alt => "Screenshot", :title => "Auf der SourceTalk 2009 in Göttingen, Photo: Roland Velten"}) %></div>

  * *[Partizipation 2.0: Medientheorie und das Mitmach-Web](http://www.kantel.de/gauting/index.html)*, Vortrag auf dem [8. Gautinger Internet-Treffen](http://gauting.twoday.net/topics/8.Gautinger+Internet-Treffen/), März 2007
  * *[Partizipation 2.0. Vom Web 1.0 über das Web 2.0 zum Mitmach-Web (?)](http://www.kantel.de/generation20/index.html)* Vortrag auf dem [Fachtag Jugendinformation »Generation 2.0](http://www.jsnds.de/index.php?id=3268&tx_ttnews[backPid]=36&tx_ttnews[tt_news]=1076&tx_ttnews[showimage]=0), eine Veranstaltung des Deutschen Bundesjugendrings (DBJR) und des Landesjugendrings Niedersachsen e.V. im Kommunikations- und Veranstaltungszentrum Pavillon in Hannover am 31. Mai 2007.
  * *[Vom Web 2.0 zur Partizipation 2.0](http://www.cognitiones.de/doku.php/vom_web_2.0_zur_partizipation_2.0)* — Vortrag auf dem Webkongreß 2008 der Universität Erlangen, 5. September 2008
  * *[Die Wissenschaft auf Wolke Google](http://www.cognitiones.de/doku.php/die_wissenschaft_auf_wolke_google) — Chancen und Risiken von Cloud- und Utility-Computing, Vortrag zum [Technologie-Tag der MPIs](http://www.cpfs.mpg.de/~bs/ttdd09/Technologie-Tag_Dresden_2009/Startseite.html) am 19. Mai 2009 in Dresden
  * *[Cloud- und Utilitycomputing](http://www.cognitiones.de/doku.php/cloud-_und_utilitycomputing) -- Aktueller Stand und mögliche Trends*, Vortrag auf dem [26. DV-Treffen der Max-Planck-Institute](http://dv-treffen.mpg.de/) am 23. September 2009 in Berlin
  * *[3D-Scans von Keilschrifttafeln](http://www.cognitiones.de/doku.php/3d-scans_von_keilschrifttafeln) – Ein Werkstattbericht*, Vortrag zum [26. DV-Treffen der Max-Planck-Institute](http://dv-treffen.mpg.de/) am 24. September 2009
  * *[Fürs Leben lernen](http://www.cognitiones.de/doku.php/fuers_leben_lernen) -- Bausteine für ein gelingendes Leben in der Netzwerkgesellschaft*, Vortrag zu den MaC*_days 2009 am 30. September 2009 in Josefst(h)al
  * *[Die Wissenschaft auf Wolke Google 2.0](http://www.cognitiones.de/doku.php/die_wissenschaft_auf_wolke_google_2.0)*, Vortrag zur [SourceTalk 2009](http://www.sourcetalk.de/2009/programm.php) am 1. Oktober 2009 in Göttingen
  * *[Das NLTK - eine Einführung](http://www.cognitiones.de/doku.php/das_nltk_-_eine_einfuehrung)* - Kurzvortrag zum [Natural Language Toolkit](Natural Language Toolkit) (NLTK) am 5. November 2009 am MPIWG.
  * *[3D Data - What comes next?](http://www.cognitiones.de/doku.php/3d_data_-_what_comes_next) - Problems of presenting three-dimensional data in the web. Work in progress*, Vortrag (zusammen mit Sebastian Schröder auf dem [3D-Workshop 2009](http://www.cognitiones.de/doku.php/3d-workshop_2009) am 12. November 2009 in Jena.
  * *[Das Urheberrecht im Zeitalter der technischen Reproduzierbarkeit](http://www.cognitiones.de/doku.php/das_urheberrecht_im_zeitalter_der_technischen_reproduzierbarkeit)*, Vortrag auf der [re:publica 2010](http://re-publica.de/10/) am 15. April 2010
  * *[Open Source - offene Protokolle - Open Access](http://www.cognitiones.de/doku.php/open_source_-_offene_protokolle_-_open_access). Baukasten für ein zukünftiges Web des Wissens*, Vortrag auf dem [Lotus JamCamp 2010](http://www-01.ibm.com/software/de/jamcamp/programm.html) am 23. April 2010 in Ehningen bei Stuttgart.
  * *[3D Scans of Cuneiform Tablets](http://www.cognitiones.de/doku.php/3d_scans_of_cuneiform_tablets)*. Vortrag, gehalten auf dem Workshop *Middle Eastern Cuneiform Tablets Collections: Target Collections and Imaging Strategies*, Oxford, Wolfson College, September, 6th, 2010
  * *[Vertriebskanal vs. Web des Wissens](http://www.cognitiones.de/doku.php/vertriebskanal_vs._web_des_wissens) – Warum Geräte wie das iPad uns in eine Sackgasse führen*, Vortrag auf dem *[eco Kongress 2010 »Roads for Tomorrow«](http://www.eco.de/veranstaltungen/7988.htm)* am 29. September 2010 in Köln
  * *[[3D-Scans von Keilschrifttafeln - ein Zwischenbericht](http://www.cognitiones.de/doku.php/3d-scans_von_keilschrifttafeln_-_ein_zwischenbericht)* – Vortrag (zusammen mit [Peter Damerow](Peter Damerow), Sarah Köhler und Juliane Eule) auf dem *Breuckmann-Anwender-Forum*, 28. und 29. Oktober 2010, Frankfurt/Main
  * *[Die Geburt des Wikis aus der Wade der Wunderkammer](http://www.cognitiones.de/doku.php/die_geburt_des_wikis_aus_der_wade_der_wunderkammer). Wege zu einem Web des Wissens.* Vortrag für die re:publica 2011, gehalten am 13. April 2011
  * *[Das Urheberrecht im Zeitalter der technischen Reproduzierbarkeit 2.0](http://www.cognitiones.de/doku.php/das_urheberrecht_im_zeitalter_der_technischen_reproduzierbarkeit_2.0)*, Vortrag zur CityTalk am 24. Mai 2011 in Berlin

  * <iframe src="http://player.vimeo.com/video/24877076?title=0&amp;byline=0&amp;portrait=0" width="480" height="270" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>

  * *[Jedermann ein Wissensproduzent](http://www.cognitiones.de/doku.php/jedermann_ein_wissensproduzent) -- Verteilte soziale Netzwerke als Basis für ein freies Web des Wissens*, Vortrag zum [edenspiekermann_ nach mittag in stuttgart](http://edenspiekermann.com/en/blog/nach-mittag-in-stuttgart--2) am 27. Mai 2011

## Steckenpferde

Klavierspielen, Schreiben, [Mathematik](http://www.spass-mit-mathematik.de/), [Hundesportler](http://hsv-plaenterwald.jimdo.com/) und [-Trainer](http://hsv-plaenterwald.jimdo.com/über-uns/trainer/) ([Agility](http://www.agilityblog.de/), Obedience) und das [Sammeln von Krawatten](Ode an George Bryan Brummell).

## Maskottchen

[Nilpferd](Nilpferd)

## Disclaimer

*This is a personal site published by the author. The ideas and information expressed on it have not been approved or authorised by the [Max Planck Institute for the History of Science](http://www.mpiwg-berlin.mpg.de/) (MPIWG) either explicitly or implicitly. In no event shall the MPI be liable for any damages whatsoever resulting from any action arising in connection with the use of this information or its publication, including any action for infringement of copyright or defamation.*