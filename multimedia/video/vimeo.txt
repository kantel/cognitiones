#title "Vimeo"

**Vimeo** ist ein Videohoster, der sich vorwiegend an Kreative wendet, die ihre selbstgedrehten Videos dort einstellen können.

## Neue Features

<iframe src="http://player.vimeo.com/video/48046038?byline=0&amp;color=5e52ab" width="480" height="270" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>

*Video vom 30. August 2012*

## Links

  * [Vimeo Home](https://vimeo.com/)
  * [Vimeo Blog](https://vimeo.com/blog/)
  * [Vimeo developer()](https://developer.vimeo.com/)
  * [Mein Account](https://vimeo.com/kantel) bei Vimeo