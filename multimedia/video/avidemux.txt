#title "Avidemux"

**Avidemux** (GPL) ist eine freie Videoschnittsoftware für [Linux](Linux), BSD, [MacOS X](MacOS X) und Windows. Das Programm unterstützt [MPEG-4](MPEG-4)-Dateien native, hat aber noch leichte Probleme mit dem Codec [H.264](H.264) (kann ihn im Safe-Mode bearbeiten, verliert aber an Frame-Genauigkeit beim Schneiden). Das Augenmerk des Projekts richtet sich auf Plattformunabhängigkeit und eine vergleichsweise hohe Anzahl an standardmäßig unterstützten Dateiformaten, Video- und Audiocodecs sowie Filtern. Avidemux verfügt über eine graphische Benutzeroberfläche, kann aber auch per Skript auf Kommandozeilenebene bedient werden. Bei letzterem kommt der ECMAScript-Interpreter SpiderMonkey zum Einsatz.

<%= imageref("avidemux") %>

Unter MacOS X gibt es das Programm seit der Version 2.4 mit zwei verschiedenen GUI-Toolkits (beide als Universal Binary). Einmal als [Qt](Qt)-Version mit der Menüleiste Mac-typisch oben am Desktop und den Aqua-GUI-Elmenten (rechter Screenshot) und dann als Gtk+-Version, die die Menüleiste im Fenster integriert hat (linker Screenshot). Obwohl das Gtk-GUI mit einer Unix-Anmutung kommt, gefällt es mir persönlich besser. Der Funktionsumfang beider Versionen scheint identisch zu sein.  

## Links

  * [Avidemux Home](http://avidemux.sourceforge.net/)
  * [Avidemux Downlaod](http://sourceforge.net/projects/avidemux/)
  * [Avidemux Wiki](http://www.avidemux.org/admWiki/doku.php)
  * <%= wpde("Avidemux") %> in der Wikipedia
  * Linux User: [Cut and Pack](http://www.linux-user.de/ausgabe/2005/11/077-avidemux/index.html) (Avidemux Kurzanleitung)