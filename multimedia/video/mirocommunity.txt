#title "Miro Community"

<%= imageref("screenshot-miro-community") %>

*[Schockwellenreiter-TV](http://kantel.mirocommunity.org/), meine Miro-Community-Testseite*

**Miro Community** ist gleichzeitig ein Onlinedienst wie auch Open-Source-Software für Portale mit eingebetteten Videos. Hinter Miro Community steht die gemeinnützige *Participatory Culture Foundation* (PCF). Die Videoportale lassen sich mit dem Miro-Community-Hostingdienst kostenlos einrichten, für größere Organisationen oder Unternehmenes gibt aber auch eine kostenpflichtige Premium-Version mit einigen zusätzlichen Features (die »freie« Version läßt leider nur maximal 500 Videos zu). Alternativ kann die eigene Miro Community mit der zum Download angebotenen gleichnamigen Open-Source-Software (GPL) selbst betrieben werden. Miro Community basiert auf [Python](Python) und [Django](Django). 

Die Videos der Miro Community Site oder einzelne Kanäle daraus lassen sich als [Podcast](Podcasting) im [Miro Media Player](Miro) abonnieren.

## Mehr in diesem Wiki

  * [Miro](Miro)
  * [Miro Video Converter](Miro Video Converter)

## Ähnliche Software

  * [MediaCore](MediaCore)
  * [Plumi](Plumi)

## Links 

  * [Miro Community Homepage](http://www.mirocommunity.org/)
  * [Miro Community Manual](https://develop.participatoryculture.org/index.php/MiroCommunityManual)
  * [Installing Miro Community](https://develop.participatoryculture.org/index.php/Run_your_own_Miro_Community_site) (Run your own Miro Community site)
  * [Miro Community Wiki und Download](https://develop.participatoryculture.org/trac/democracy/wiki/MiroCommunity)
  * [Schockwellenreiter-TV](http://kantel.mirocommunity.org/), meine Miro Community Site
  * [Golem.de über Miro Community](http://www.golem.de/1001/72683.html)
  

