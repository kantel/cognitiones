#title "VLC Media Player"

<%= imageref("vlcicon") %>

Der **VLC Media Player** (GPL) ist ein auf vielen Betriebssystemen und Plattformen laufender Multimedia-Player, der nahezu alle Audio. und Videoformate und Codecs beherrscht und sich nicht um DRM schert. 

<%= imageref("vlcscreenshot") %>

Außerdem kann der VLC Media Player mir Streaming-Protokollen umgehen und auch als ein *Streaming Server* (entsprechende Bandbreite vorausgesetzt) genutzt werden.

<%= imageref("vlc092") %>

*Screenshot VLC 0.9.2*

Der VLC Media Player benötigt keine weiteren externen Codecs um zu funktionieren.

Mit dem *VLCKit* gibt es ein neues Framework für MacOS X, mit dem Entwickler eigene Applikationen rund um VLC entwickeln können.

## Links

  * [VideoLAN-Homepage](http://www.videolan.org/)
  * [Download-Seite](http://www.videolan.org/vlc/)
  * [VideoLAN Streaming Solution](http://www.videolan.org/vlc/streaming.html)
  
