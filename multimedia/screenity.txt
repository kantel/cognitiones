#title "Screenity"

**Screenity** ist eine freie (MIT-Lizenz) Erweiterung für Google Chrome, mit der Ihr entweder einen Chrome-Tab oder Euren ganzen Desktop aufnehmen könnt. Außerdem könnt ihr auch Euch über eine Kamera aufnehmen und es gibt es diverse Werkzeuge wie Zeiger und Stifte, um Euren Screencast zu verschönern. Und last but not least könnt Ihr zum Schluß Euren Screencast auch noch zurechtschneiden, was den Streß bei der Aufnahme verringert 🤓.

Den fertigen Cast könnt Ihr entweder als MP4, GIF oder WebM exportieren oder das Video gleich nach Google Drive hochladen. Die Software steht im Chrome Webstore zum (kostenlosen) Download bereit.

## Video

<iframe width="560" height="315" src="https://www.youtube.com/embed/7vNaJnH-pj0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Literatur

- Carsten Knobloch (Caschy): *[Screenity: Screenrecorder (Open Source) mit Annotationswerkzeugen](https://stadt-bremerhaven.de/screenity-screenrecorder-open-source-mit-annotationswerkzeugen/)*, Cashys Blog vom 27. November 2020

## Links

- [Screenity Home @ GitHub](https://github.com/alyssaxuu/screenity)
- [Download im Chrome Webstore](https://chrome.google.com/webstore/detail/screenity-screen-recorder/kbbdabhdfibnancpjfhlkhafgdilcnji)