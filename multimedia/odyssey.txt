#title "Odyssey"

<iframe src="//player.vimeo.com/video/97968118?title=0&amp;byline=0&amp;portrait=0" width="560" height="299" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

**Odyssey.js** ist ein Open-Source-Werkzeug (BSD-Lizenz) für Blogger und Journalisten, mit dem man Karten und andere multimediale Inhalte in interaktive Stories verwandeln kann.

## Ähnliche Software in diesem Wiki

  * [Mozilla Popcorn](Mozilla Popcorn)
  * [Virtual Spaces](Virtual Spaces)

## Literatur

  * CartoDB Blog: *[Odyssey.js: examples in the wild](http://blog.cartodb.com/odyssey-js-examples-in-the-wild/)*, 10. Juli 2014

## Links

  * [Odyssey.js Home](http://cartodb.github.io/odyssey.js/index.html)
  * [Odyssey.js Dokumentation](http://cartodb.github.io/odyssey.js/documentation/)
  * [Download @ GitHub](https://github.com/CartoDB/odyssey.js)