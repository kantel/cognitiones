#title "New Moon"

<%= imageref("newmoon") %>

**New Moon** ist ursprünglich als Farbpalette für Code-Editoren *(Dark Mode)* gedacht:

<table>
	<tr style="border: 1px solid; padding: 10px;">
		<td style="border: 1px solid; padding: 10px;">&nbsp;#2d2d2d:&nbsp;(45,&nbsp;45,&nbsp;45)&nbsp;</td><td style="border: 1px solid; padding: 10px;background-color:#2d2d2d; width:40em;"></td>
	</tr>
	<tr style="border: 1px solid; padding: 10px;">
		<td style="border: 1px solid; padding: 10px;">&nbsp;#777c85:&nbsp;119,&nbsp;124,&nbsp;133)&nbsp;</td><td style="border: 1px solid; padding: 10px;background-color:#777c85; width:40em;"></td>
	</tr>
	<tr style="border: 1px solid; padding: 10px;">
		<td style="border: 1px solid; padding: 10px;">&nbsp;#b3b9c5:&nbsp;(179,&nbsp;185,&nbsp;192)&nbsp;</td><td style="border: 1px solid; padding: 10px;background-color:#b3b9c5; width:40em;"></td>
	</tr>
	<tr style="border: 1px solid; padding: 10px;">
		<td style="border: 1px solid; padding: 10px;">&nbsp;#ffffff:&nbsp;(255,&nbsp;255,&nbsp;255)&nbsp;</td><td style="border: 1px solid; padding: 10px;background-color:#ffffff; width:40em;"></td>
	</tr>
	<tr style="border: 1px solid; padding: 10px;">
		<td style="border: 1px solid; padding: 10px;">&nbsp;#f2777a:&nbsp;(242,&nbsp;119,&nbsp;122)&nbsp;</td><td style="border: 1px solid; padding: 10px;background-color:#f2777a; width:40em;"></td>
	</tr>
	<tr style="border: 1px solid; padding: 10px;">
		<td style="border: 1px solid; padding: 10px;">&nbsp;#fca369:&nbsp;(252,&nbsp;163,&nbsp;105)&nbsp;</td><td style="border: 1px solid; padding: 10px;background-color:#fca369; width:40em;"></td>
	</tr>
	<tr style="border: 1px solid; padding: 10px;">
		<td style="border: 1px solid; padding: 10px;">&nbsp;#ffd479:&nbsp;(255,&nbsp;212,&nbsp;121)&nbsp;</td><td style="border: 1px solid; padding: 10px;background-color:#ffd479; width:40em;"></td>
	</tr>
	<tr style="border: 1px solid; padding: 10px;">
		<td style="border: 1px solid; padding: 10px;">&nbsp;#ffeea6:&nbsp;(255,&nbsp;238,&nbsp;166)&nbsp;</td><td style="border: 1px solid; padding: 10px;background-color:#ffeea6; width:40em;"></td>
	</tr>
	<tr style="border: 1px solid; padding: 10px;">
		<td style="border: 1px solid; padding: 10px;">&nbsp;#92d192:&nbsp;(146,&nbsp;209,&nbsp;146)&nbsp;</td><td style="border: 1px solid; padding: 10px;background-color:#92d192; width:40em;"></td>
	</tr>
	<tr style="border: 1px solid; padding: 10px;">
		<td style="border: 1px solid; padding: 10px;">&nbsp;#6ab0f3:&nbsp;(106,&nbsp;176,&nbsp;243)&nbsp;</td><td style="border: 1px solid; padding: 10px;background-color:#6ab0f3; width:40em;"></td>
	</tr>
	<tr style="border: 1px solid; padding: 10px;">
		<td style="border: 1px solid; padding: 10px;">&nbsp;#76d4d6:&nbsp;(118,&nbsp;212,&nbsp;214)&nbsp;</td><td style="border: 1px solid; padding: 10px;background-color:#76d4d6; width:40em;"></td>
	</tr>
    <tr style="border: 1px solid; padding: 10px;">
		<td style="border: 1px solid; padding: 10px;">&nbsp;#e1a6f2:&nbsp;(225,&nbsp;166,&nbsp;242)&nbsp;</td><td style="border: 1px solid; padding: 10px;background-color:#e1a6f2; width:40em;"></td>
	</tr>
	<tr style="border: 1px solid; padding: 10px;">
		<td style="border: 1px solid; padding: 10px;">&nbsp;#ac8d58:&nbsp;(172,&nbsp;141,&nbsp;88)&nbsp;</td><td style="border: 1px solid; padding: 10px;background-color:#ac8d58; width:40em;"></td>
	</tr>
</table>

~~~python
newmoon = [color(45, 45, 45), color(119, 124, 133), color(179, 185, 192),
           color(255, 255, 255), color(242, 119, 122), color(252, 163, 105),
           color(255, 212, 121), color(255, 238, 166), color(146, 209, 146),
           color(106, 176, 243), color(118, 212, 214), color(225, 166, 242),
           color(172, 141, 88)]
~~~

**Quelle**: Tania Rascia: *[New Moon Theme](https://taniarascia.github.io/new-moon/)* (MIT-Lizenz)
