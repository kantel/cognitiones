#title "Genuary 24"
#created "18. Januar 2024"
#updated "18. Januar 2024"

<%= imageref("g24_1") %>&nbsp;<%= imageref("g24_2") %>

<table>
	<tr style="border: 1px solid; padding: 10px;">
		<td style="border: 1px solid; padding: 10px;">&nbsp;#F6340C</td><td style="background-color:#F6340C; width:40em;"></td>
	</tr>
	<tr style="border: 1px solid; padding: 10px;">
		<td style="border: 1px solid; padding: 10px;">&nbsp;#156CD2</td><td style="background-color:#156CD2; width:40em;"></td>
	</tr>
	<tr style="border: 1px solid; padding: 10px;">
		<td style="border: 1px solid; padding: 10px;">&nbsp;#0D0B1B</td><td style="background-color:#0D0B1B; width:40em;"></td>
	</tr>
	<tr style="border: 1px solid; padding: 10px;">
		<td style="border: 1px solid; padding: 10px;">&nbsp;#F1E4E8</td><td style="background-color:#F1E4E8; width:40em;"></td>
	</tr>
	<tr style="border: 1px solid; padding: 10px;">
		<td style="border: 1px solid; padding: 10px;">&nbsp;#ECCA2D</td><td style="background-color:#ECCA2D; width:40em;"></td>
	</tr>
</table>

~~~python
genuary24 = ["#F6340C", "#156CD2", "#0D0B1B", "#F1E4E8", "#ECCA2D"]
~~~

[Quelle](https://twitter.com/gorillasu/status/1747793292312416740?t=HrbKVgmphkZs-tU-odOLNw).
