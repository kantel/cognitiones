#title "Darktable"

<%= imageref("darktableslighttable-cp") %>

**Darktable** versteht sich als digitale Dunkelkammer und als digitaler Leuchttisch, entwickelt von Photographen für Photographen. Die Software ist frei (GPL 3) und verspricht, daß die Originalquellen niemals angetastet werden. Sie versteht eine große Menge an Bildformaten und verspricht durch die GPU-Unterstützung eine schnelle Bildverarbeitung auch im Batch (Darktable kann via [Lua](Lua) gescripted werden). Darktable versteht neben Windows und macOS so ziemlich jede *NIX- und Linux-Variante.

## Literatur und Tutorials

  * Dr. Web: [Darktable – Kostenloser Lightroom-Konkurrent jetzt auch für Mac OS X verfügbar](http://www.drweb.de/magazin/fotografie-darktable-kostenloser-lightroom-konkurrent-jetzt-auch-fur-mac-os-x-verfugbar-34480/) (veraltet)

## Links

  * [Darktable Home](http://www.darktable.org/)
  * [Darktable User Manual](https://darktable-org.github.io/dtdocs/) (auch in [deutscher Übersetzung)(https://darktable-org.github.io/dtdocs/de/)])
  * [Darktable Installation](https://www.darktable.org/install/) (Binaries und Pakete für viele Betriebssysteme)
  * [Darktable @ GitHub](https://github.com/darktable-org/darktable)
