#title "PhotoSwipe"

**PhotoSwipe** ist eine freie (MIT-Lizenz), moderne [JavaScript](JavaScript)-Bildergalerie, die besonders für mobile Geräte mit Touchscreens optimiert ist. Denn auf Touchscreen-Geräten wie Tablet PCs und Smartphones können die Bilder mit dem beliebten *Swipe*-Effekt nacheinander »weitergeblättert« werden. Alternativ (und auch auf Desktop-Monitoren nutzbar) gibt es eine Diashow-Funktion und einen Vor- und Zurück-Button. Auch die Navigation über die Pfeiltasten der Tastatur ist möglich.

## Tutorials

  * Ellen Bauer: *[Die Photoswipe Bildergalerie (mit Touchscreen Swipe-Effekt) nutzen](http://www.elmastudio.de/tutorials/die-photoswipe-bildergalerie-mit-touchscreen-swipe-effekt-nutzen/)*, elma studio vom 24. August 2011

  * PhotoSwipe: *[Getting Started](http://photoswipe.com/documentation/getting-started.html)*

## Links

  * [PhotoSwipe Home](http://photoswipe.com/)
  * [PhotoSwipe @ GitHub](https://github.com/dimsemenov/PhotoSwipe)