#title "Photo"

## Software zur Photobearbeitung

  * [Darktable](Darktable)
  * [digiKam](digiKam)
  * [Flickr](Flickr)
  * [FFXporter](FFXporter)
  * [Gimp](Gimp)
  * [iPhoto](iPhoto)
  * [Picasa](Picasa) und die [Picasa Web Albums](Picasa Web Albums)
  * [qView](qView)

## Bibliotheken und Galerien

  * [PhotoSwipe](PhotoSwipe)