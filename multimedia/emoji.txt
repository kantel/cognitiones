#title "Emoji"

<%= imageref("emojiaffe") %>

Ein **Emoji** (jap. 絵文字 bzw. えもじ dt. *Bildschriftzeichen*) ist ein Ideogramm, das insbesondere in SMS und Chats längere Begriffe ersetzt. Im Okotber 2010 wurden die Emojis in Unicode 6.0 aufgenommen und damit die Grundlage für eine weltweit einheitliche Kodierung geschaffen.  Insgesamt enthält Unicode 6.3 somit 722 Emojis.

Von den 2.834 mit Unicode 7.0 neu eingeführten zusätzlichen Zeichen können etwa 250 als Emojis verwendet werden, ohne daß dies im Einzelnen gesondert spezifiziert wäre. Eine Zählung der Washington Post kommt auf 240 neue Emojis. Die genaue Gesamtzahl aller in Unicode enthaltenen Emojis bleibt jedoch unklar und ist einem gewissen Ermessen unterworfen.

## In diesem Wiki

  * [Twemoji](Twemoji) ist ein Satz freier Emojis von Twitter

## Tools

  * [emojify.js](http://hassankhan.me/emojify.js/) ist eine [JavaScript](JavaScript)-Bibliothek, die Emoji-Schlüsselwörter in Bilder konvertiert
  * Eine [Liste der Emoji-Schlüsselwörter](http://www.emoji-cheat-sheet.com), wie sie in diversen Diensten Verwendung finden
  * [gemoji](https://github.com/github/gemoji) ist eine Emoji-Bibliothek für [Ruby](Ruby)
  * [Emoji](https://github.com/wpeterson/emoji) ist eine weitere Ruby-Gem für Emojis

## Links

  * <%= wpde("Emoji") %> in der Wikipedia
  * [Komplette Liste aller Unicode-Emoji mit deutschen Beschreibungen](http://www.typografie.info/3/page/artikel.htm/_/wissen/unicode-emoji-deutsch)
