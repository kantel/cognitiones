#title "IPU-Chart"

**IPU-Chart** (Donationware) ist ein Plugin für WordPress (es soll auch mit HTML pur und [EPUB3](EPUB) gehen, aber das *wie* habe ich nicht herausgefunden), um Datenvisualisierungen in [SVG](SVG) zu erstellen. Es basiert auf [D3](Data Driven Documents).

## Ähnliche Software in diesem Wiki

  * [Data Driven Documents](Data Driven Documents) (D3)

<div style="float: right; margin-left: 18px;"><iframe src="http://rcm-de.amazon.de/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=derschockwell-21&o=3&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=1449339735" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe>&nbsp;<iframe src="http://rcm-de.amazon.de/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=derschockwell-21&o=3&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=1449328792" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe></div>

## Links

  * [IPU-Chart Home](https://www.ipublia.com/products/ipu-chart-svg-chart-library/)
  * [Blog des Entwicklers](https://www.ipublia.com/)
  * [IPU-Chart Online Editor](https://www.ipublia.com/support/ipu-chart-editor-online/)
  * [IPU-Chart for WordPress User Guide](https://www.ipublia.com/support/docs/ipu-chart-for-wordpress-user-guide/)