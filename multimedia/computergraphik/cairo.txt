#title "Cairo"

<%= imageref("cairo-banner") %>

**Cairo** ist eine 2D-Graphikbibliothek mit Unterstützung für mehrere Ausgabeziele. Zu den derzeit unterstützten Ausgabezielen zählen das X Window System (über Xlib und XCB), Quartz, Win32, Bildpuffer, PostScript-, [PDF](PDF)- und [SVG](SVG)-Dateiausgaben. Zu den experimentellen Backends gehören [OpenGL](OpenGL), BeOS, OS/2 und DirectFB.

## Links

- [Cairo Home](https://www.cairographics.org/)