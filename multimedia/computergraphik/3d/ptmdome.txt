#title "PTM Dome"

<%= imageref("ptmdome") %>

Der **PTM Dome** ist ein Gerät, mit dem Scans im *[Polynomial Texture Mapping (PTM)](http://www.southampton.ac.uk/archaeology/acrg/acrg_research_herculaneum_PTM.html)* und *Reflectance Transformation Imaging (RTI)* erstellt werden. In der Kuppel sind in regelmäßigen Abständen Lichter angebracht, die nacheinander je eine Aufnahme des zu scannenden Objektes erstellen. Die dazugehörende Software überlagert diese Aufnahmen und macht es mit Hilfe eines (virtuellen) Trackballs möglich, das Objekt nahezu stufenlos mit wechselnder Beleuchtung darzustellen. So können besonders archäologische Objekte wie etwa Keilschrifttafeln durch die wechselnden Schlagschatten besser analysiert werden.

Das abgebildete Gerät ist von der [Archaeological Computing Research Group, School of Humanities, University of Southampton, Southampton, UK](http://www.soton.ac.uk/archaeology/acrg/).

## In diesem Wiki

  * [Reflectance Transformation Imaging](Reflectance Transformation Imaging)

## Links

  * [Polynomal Texture Mapping](http://www.soton.ac.uk/archaeology/acrg/acrg_research_herculaneum_PTM.html) an der [Archaeological Computing Research Group](http://www.soton.ac.uk/archaeology/acrg/) der [School of Humanities](http://www.humanities.soton.ac.uk/), [University of Southampton](http://www.soton.ac.uk/).
  * Graeme Earl, Gareth Beale, Kirk Martinez, Hembo Pagi: [Polynormal Texture Mapping and related Imaging Technologies for the Recording, Analysis and Presentation of Archaeological Materials](http://www.isprs-newcastle2010.org/papers/227.pdf) (PDF), International Archives of Photogrammetry, Remote Sensing and Spatial Information Sciences, Vol. XXXVIII, Part 5 Commission V Symposium, Newcastle upon Tyne, UK. 2010
  
