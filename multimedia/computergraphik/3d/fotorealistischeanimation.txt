#title "Fotorealistische Computeranimation"

<%= imageref("fotorealistische") %>

*<a href="http://www.amazon.de/gp/product/354053234X/ref=as_li_ss_tl?ie=UTF8&tag=derschockwell-21&linkCode=as2&camp=1638&creative=19454&creativeASIN=354053234X">Buch bei Amazon kaufen</a><img src="http://www.assoc-amazon.de/e/ir?t=derschockwell-21&l=as2&o=3&a=354053234X" width="1" height="1" border="0" alt="" style="border:none !important; margin:0px !important;" />*

Das Buch zum Film *Occursus cum nuovo* (1987), eines der Meilensteine computergenerierter, photorealistischer Filme ist 1991 erschienen und trägt den Titel **Fotorealistische Computeranimation**. Es behandelt beispielhaft die Erstellung des Filmes von der Konzeption und Planung über den Entwurf und die Modellierung bis hin zur Bilderzeugung und Aufzeichnung und weiter zur Postproduktion. Auch wenn die darin beschriebenen Softwarepakete veraltet sind, das Buch ist es nicht. Es zeigt wunderbar die Vorzüge textbasierter Raytracer (wie zum Beispiel [PoVRay](PoVRay) einer ist) auf, die man per Skript (die Autoren sprechen von Einmal- oder Wegwerfprogrammen) effektiv füttern kann. Und vor allem werden die Grundlagen ausführlich und mit ihren historischen Wurzeln beleuchtet. Auch das Buch war ein Meilenstein und gehört noch heute in die Handbibliothek eines jeden an computergenerierter [3D-Graphik](3D) Interessierten.

Und da die Leistungsfähigkeit moderner Personalcomputer die der Workstation von 1987 bei weitem übersteigt, regt auch dieses Buch zum Nachbauen und Experimentieren an. Statt [Pascal](Pascal) und VERA, [Perl](Perl) und [MegaPov](MegaPoV). Wenn ich doch nur etwas mehr Zeit hätte…

<br />

----

<br />

W. Leister, H. Müller, A. Stößer: *<a href="http://www.amazon.de/gp/product/354053234X/ref=as_li_ss_tl?ie=UTF8&tag=derschockwell-21&linkCode=as2&camp=1638&creative=19454&creativeASIN=354053234X">Fotorealistische Computeranimation</a><img src="http://www.assoc-amazon.de/e/ir?t=derschockwell-21&l=as2&o=3&a=354053234X" width="1" height="1" border="0" alt="" style="border:none !important; margin:0px !important;" />*, Heidelberg (Springer) 1991 

