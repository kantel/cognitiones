#title "MeshLabJS"

<%= imageref("meshlabjs") %>

**MeshLabJS** ist ein (noch unvollständiger), freier (GPL) Port der 3D-Viewer-Software [MeshLab](MeshLab), der in [JavaScript](JavaScript) implementiert wurde und komplett clientseitig im Browser läuft.

## Links

  * [MeshLabJS Home](http://www.meshlabjs.net/)
  * [MeshLabJS @ GitHub](https://github.com/cnr-isti-vclab/meshlabjs)
  * [MeshLabJS-Dokumentation](http://www.meshlabjs.net/doc/html/)
  * [Ankündigung im MeshLab-Blog](http://meshlabstuff.blogspot.de/2016/01/we-are-proud-to-present-first-beta.html)