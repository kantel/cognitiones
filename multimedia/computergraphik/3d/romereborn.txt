#title "Rome Reborn"

<iframe src="http://player.vimeo.com/video/32038695?title=0&amp;byline=0&amp;portrait=0" width="480" height="270" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>

**Rome Reborn** ist eine internationale Initiative, gehostet bei der *University of Virginia*, die an einer digitalen, dreidimensionalen Rekonstruktion Roms von der späten Bronezzeit (ca. 1.000 Jahre vor Beginn unserer Zeitrechnung) bis ins frühe Mittelalter (etwa 550 nach Beginn unserer Zeitrechnung) arbeitet.

Das obige Video zeigt einen (möglichen) Durchgang durch die fast fertiggestellte Rekonstruktions Roms um etwa 320 nach Beginn unserer Zeitrechnung.

## Links

  * [Homepage des Projekts](http://www.romereborn.virginia.edu/)

