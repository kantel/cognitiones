#title "MegaPoV"

**MegaPOV** (Open Source, POV-Ray-Lizenz) ist eine inoffizielle Version des Persistance of Vision Raytracers [PoVRay](PoVRay), die es auch als *Universal Binary* für MacOS X gibt. Dabei fehlen (wohl aus Lizenzgründen) fast alle Include-Dateien der offiziellen POV-Ray-Distribution, so daß man sich diese noch herunterladen und sicherstellen muß, daß MegaPOV diese auch findet. Doch dabei wird man von einem GUI gut unterstützt.

<%= imageref("megapov") %>

Wie der Screenshot zeigt, besitzt MegaPOV vier wichtige Fenster: Einmal den Editor, der bei diesem textbasierten Raytracer die wichtigste Arbeitsumgebung ist, dann ein Fenster, indem man die wichtigsten Voreinstellungen für seine Datei treffen kann und das auch gegebenenfalls (zum Beispiel bei Animationen) die `ini-Datei` herausschreibt. Schließlich ein Fenster, das die Fortschritts- und Fehlermeldungen ausgibt und last but not least das gerenderte Bild selber.

Auch wenn ich die zusätzlichen Features nicht unbedingt nutze: MegaPOV ist auf Intel-Macs bedeutend schneller als die offizielle Version von POV-Ray.

## PoV-Ray 3.7

Seit der Version 3.7 vom November 2013 steht PovRay unter der AGPL und MegaPOV scheint nun **POV-Ray Unofficial** für MacOS X zu heißen und kann unter diesem Namen heruntergeladen werden.

<div style="float:right; margin-left: 8px;"><iframe src="http://rcm-de.amazon.de/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=derschockwell-21&o=3&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=3446228713" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe></div>

## Links

  * [POV-Ray Homepage](http://www.povray.org/)
  * [MegaPOV Homepage](http://megapov.inetart.net/)
  * [POV-Ray Unoffical Homepage](http://megapov.inetart.net/povrayunofficial_mac/) mit Download für MacOS X (ab 10.6 und höher)

<br /><br />

----

<br />

Kategorie: MacOS X Software -> [Arbeitsmaterial Computergraphik](Computergraphik) -> PoVRay -> [3D](3D)
