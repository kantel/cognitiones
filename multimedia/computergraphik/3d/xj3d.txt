#title "Xj3D"

<%= imageref("xj3dlogo") %>

**Xj3D** ist der »offizielle [VRML](VRML)/[X3D](X3D)-Browser und Toolkit des W3C. Die [Java](Java)-basierte Software hat allerdings seit 2006 kein Update mehr erfahren. Daher gibt es für [MacOS X](MacOS X) auch nur eine Version für PPC (no Intel).

## Links

  * [Xj3D-Projektseite](http://www.xj3d.org/)
  * [Download](http://www.web3d.org/x3d/xj3d//)
  * [Xj3D Tutorials](http://www.xj3d.org/tutorials/index.html)