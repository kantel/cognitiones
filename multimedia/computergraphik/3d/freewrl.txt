#title "FreeWRL"

<%= imageref("freewrl") %>

**FreeWRL** (GPL) ist ein [VRML](VRML)/[X3D](X3D)-Viewer für [MacOS X](MacOS X) und [Linux](Linux) (Windows-Port in Vorbereitung). Er ist meines Wissens der einzige VRML/X3D-Viewer, der noch aktiv weiterentwickelt wird.

## In diesem Wiki

  * [Xj3D](Xj3D), der offizielle VRML/X3D-Browser des W3C, letzte Aktualisierung 2006

## Links

  * [FreeWRL Homepage](http://freewrl.sourceforge.net/)