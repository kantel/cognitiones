#title "xc.js"

<%= imageref("xcjslogo") %>

**xc.js** (BSD-Lizenz) ist ein [JavaScript](JavaScript)-Framework, um einfache 2D-Spiele im [HTML5](HTML5)-Canvas zu programmieren. Es gibt zusätzlich auch ein Backend für Apples iOS. Das Design des Frameworks ist inspiriert von [cocos2d](cocos2d).

## Links

  * [xc.js Homepage](http://www.getxc.org/)
  * [Interaktive Demo](http://www.getxc.org/try.html)
  * [Sources bei github](https://github.com/fairfieldt/xcjs/)
  * [Ankündigung im Blog des Entwicklers](http://www.notenoughminerals.com/?p=19)
  