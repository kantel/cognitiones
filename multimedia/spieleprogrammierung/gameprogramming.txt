#title "Game Programming"

<div style="float:right; margin-left: 8px"><iframe src="http://rcm-de.amazon.de/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=derschockwell-21&o=3&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=0470068221" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe></div>

Das Buch **Game Programming** von *Andy Harris*, ist nach [Beginning Game Development with Python and Pygame](Beginning Game Development with Python and Pygame) das zweite hervorragende Buch, das einem die Spieleprogrammierung mit Hilfe von [Python](Python) und [PyGame](PyGame) nahebringen will. Auf 570 Seiten erfährt man wirklich alles, was man zur Programmierung von 2D-Spielen wissen muß: Von der Spriteerstellung über die Animation und Kollisions-Erkennung bis hin zu pysikalisch korrekten Bewegungen der Sprites. Und zum Schluß baut man eine eigene *Game Engine*, die einem die Programmierung eigener Spiele massiv erleichtert. Der Autor hält Vorlesungen und liebt sein Thema. Das merkt man dem Buch an. Es ist spannend, kurzweilig und mit Leidenschaft geschrieben. Die Lektüre macht Spaß, und das kann man nicht von jedem Computerbuch behaupten.

Die dritte Dimension kommt allerdings nicht vor, die wird aber in dem anderen, oben erwähnten PyGame-Buch behandelt. So schadet es nicht, beide Bücher zu besitzen. Sie überschneiden sich recht wenig und auch im Programmierstil unterscheiden sich die beiden Autoren sehr stark. Daraus kann man nur lernen …

## Links

  * [Website zum Buch](http://aharrisbooks.net/pythonGame/)

## Bibliographische Angaben

  * Andy Harris: *Game Programming*, Hoboken, NJ (Wiley) 2007