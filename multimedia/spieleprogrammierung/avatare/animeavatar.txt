#title "Anime Avatar Maker Tool"

Mit dem **Anime Avatar Maker Tool** von Avachara könnt Ihr Ganzkörperfiguren mit verschiedenen Gemütslagen erzeugen. Doch Vorsicht, die Variationsmöglichkeiten sind riesig. Will man also eine Figur mit verschiedenen Gemütslagen erzeugen, muß man schon genau Buch führen, und da die einzelnen Variationen ohne ID in einer Matrix angeordnet sind, empfehle ich folgende Buchhaltung: Hautfarbe: (1), Gesichtsform: (2, 4), Augen: (2, 3) und so weiter. Glaubt mir, ich habe es getestet, ohne solch eine präzise Buchführung verirrt Ihr Euch in den Möglichkeiten.

Herunterladen könnt Ihr das Ergebnis nur als PNG und Ihr müßt mit der Bildverarbeitung Eures Vertrauens auch noch den schwachen, blauen Hintergrund transparent machen. Aber da die Bilder eine Höhe von 800 Pixeln besitzen, dürfte das für ein durchschnittliches [Ren’Py](renpy)-Projekt ausreichen. (Bei der mittleren Bühnengröße von 1280x720 skaliere ich die Darsteller auf 600 Pixel Höhe herunter.)

Die Site [Avachara](https://avachara.com/) ist ein riesiger Gemischtwarenladen, doch nicht alles ist -- wie das *Anime Avatar Maker Tool* -- frei zu nutzen. Ich empfehle also dringend die Lektüre des Kleingedruckten, bevor Ihr Euch in weitere Abenteuer stürzt.

## Links

- [Anime Avatar Maker Tool Home](https://avachara.com/avatar/)