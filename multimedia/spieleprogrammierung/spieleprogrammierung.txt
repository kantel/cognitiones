#title "Spieleprogrammierung"
#updated "18. Oktober 2023"

<%= imageref("atari_emu") %>

## Ressourcen

### Spieleprogrammierung allgemein

  * [Soya3D](http://home.gna.org/oomadness/en/soya3d/) (GPL) ist eine High Level 3D Engine für [Python](Python), die nicht nur für die Spieleprogrammierung, sondern auch für wissenschaftliche Simulationen und Visualisierungen eingesetzt wird. Soya3D soll unter Windows, [Linux](Linux), div. Unixe und [MacOS X](MacOS X) lauffähig sein.
  * Auf [Tile based games](http://www.tonypa.pri.ee/tbw/tut00.html) findet Ihr ein wunderbares Tutorial zu diesem Thema. Und auch wenn es für [Flash](Flash) und [ActionScript](ActionScript) geschrieben ist, es ist allgemein genug gehalten, daß Ihr es auch in der Sprache Eurer Wahl nutzen könnt. Auch diese Seiten stehen unter einer CC-Lizenz.
  * [Amit's Game Programming Information](http://www-cs-students.stanford.edu/~amitp/gameprog.html) ist eine kommentierte Linksammlung zum Thema mit einem besonderen Schwerpunkt auf Algorithmen und [Künstlicher Intelligenz](Künstliche Intelligenz). Sie ist aus dem Jahre 2008 und daher ziemlich aktuell.
  * Das [Java Game Development Tutorial](http://www.javacooperation.gmxhome.de/TutorialStartDeu.html) ist eine etwas ältere, aber dennoch gute Ressource von Fabian Birzele.
  * [Spielend Programmieren](http://spielend-programmieren.at/) ist ein (Doku-) Wiki, daß sich dem »Programmieren lernen« für Kinder und Jugendliche verschrieben hat. Sie sollen dies anhand der Programmierung eines eigenen Computerspiels spielend erlernen. Programmiersprache ist [PyGame](PyGame).

### Online Game-AI-Ressourcen

  * Der [AI Guru](http://www.aiguru.com/) bringt sehr viele Neuigkeiten, scheint diese aber automatisch zu sammeln und nicht mehr redaktionell zu sichten. Und die AI hinter dieser Automatik ist nicht sehr intelligent. Trotzdem: Auch hier kann man Perlen finden.
  * [Gamasutra](http://www.gamasutra.com/) ist wohl die angesagteste Seite im Bereich Spieleprogrammierung. Doch auch hier muß man die Perlen aus dem Strom der Nachrichten selber fischen.
  * Andersherum beschäftigt sich die Site [Generation5](http://www.generation5.org/) mit »[Künstlicher Intelligenz](Künstliche Intelligenz)« im Allgemeinen. Und das seit nunmehr 10 Jahren. Macht einen ziemlich kompetenten und umfassenden Eindruck. Daher Futter für den Feedreader.

### Tilesets und Graphiken

  * [Flying Yogi's SpriteLib GPL](http://www.flyingyogi.com/fun/spritelib.html). Eine Sammlung freier (GPL) statischer und animierter Sprites. Autor: Ari Feldmann.
  * [Reiner's Tilesets](http://www.reinerstilesets.de/de/). Eine großartige Sammlung vorwiegend [isometrischer](Isometrie) Tiles, alle Figuren sind zum größten Teil voll animiert. Alle Tiles und Graphiken sind auch für die Verwendung in kommerziellen Spielen freigegeben.
  * [The RLTiles](http://rltiles.sourceforge.net/): *Public domain graphical tileset for Roguelike games*. Isometrische (64x64 Pixel)) wie auch quadratische (32x32 Pixel) [Sprites](Sprites). Keine Animationen.
  * [Tilesets for RPG Maker 95](http://rivendell.fortunecity.com/goddess/268/tilesets.html) (verschärfter Werbebefall): 32x32-Tilesets mit etwas zweifelhaftem Rechtsstatus. Obwohl explizit zum Download eingeladen wird, scheint man sie legal nur mit dem Programm [RPG Maker 95](http://de.wikipedia.org/wiki/RPG%20Maker) verwenden zu dürfen (und auch der Status dieses Programms scheint unklar zu sein). Aber ich habe sie auch schon in anderen Freeware-Games entdeckt. Dennoch: Obwohl die Figuren wirklich nett sind, Benutzung auf eigene Gefahr. Eine Besonderheit: Die Animationen sind statt der üblichen mindestens vier nur mit zwei unterschiedlichen Einzelschritten möglich. Wegen der Kleinheit der Tiles wirken die Bewegungen trotzdem ziemlich realistisch (soweit wie ein *Comic*-Stil realistisch wirken kann).
  * Die Seite [Game Graphics](http://www.molotov.nu/?page=graphics) auf *Molotov.nu* ist eine Linksammlung zu freien Tiles-Ressourcen. Das meiste überschneidet sich mit schon von mir genannten Seiten oder ist in einem unüblichen Format, bringt einen 404-Fehler oder die Rechtslage scheint mir ungeklärt. Lediglich die Tiles, die unter [Angband Graphics](http://www.molotov.nu/?page=graphics#Angband) zu finden sind, sind wirklich brauchbar, wenn auch nicht animiert, und zumindest für den nichtkommerziellen Einsatz freigegeben.
  * Lost Garden: [Free Game Graphics](http://lostgarden.com/labels/free%20game%20graphics.html). Eine Wundertüte mit schönen Tiles und Graphiken, alle vom Urheber selber freigegeben.
  * [Last Guardian Sprites](http://blogoscoped.com/archive/2006-08-08-n51.html): Über 700 animierte 32x32-Pixel-Sprites (zwei Bilder pro Animation) von Philipp Lenssen unter einer Creative-Commons-Lizenz (by-nc 2.5)
  * [Silveira Neto](http://silveiraneto.net/) hat ein umfangreiches [Tileset](http://silveiraneto.net/2009/07/31/my-free-tileset-version-10/) und drei animierte Character Sets (Plain, Nerdy und Warrior) unter einer Dreative-Commons-Lizenz (by-sa 3.0) veröffentlicht.
  * [Pokémon Fanart](http://fanart.pokefans.net/index/) ist eine Sammlung von Tiles für [Pokémon](Pokémon) von Fans für Fans. Viele der Elemente lassen sich natürlich auch für eigene Spiele verwenden. Leider steht nichts über die Lizenz der Zeichnungen auf den Seiten. Viele sind jedoch ausdrücklich unter eine CC-Lizenz gestellt (meist by-nc-sa).
  * [OpenGameArt.org](http://opengameart.org/) ist eine – ob der schieren Menge manchmal etwas unübersichtliche – Fundgrube für alle Arten von freien Pixeln.
  * [TomeTik Tiles Library](http://pousse.rapiere.free.fr/tome/): Freie Graphiken (32x32 und Iso) aus der [Angband](Angband)-Linie. Die Graphiken sind frei, der Autor/Graphiker möchte aber gerne genannt werden (die misten sind von *David E. Gervais*, einige wenige im »Xerathul's Revenge«)-Modul wurden von *Henk Brouwer* gezeichnet.
  * [Backyard Ninja Design Free Stuff](http://www.dumbmanex.com/bynd_freestuff.html): Eine Menge gutaussehender, freier Graphiken, die auch in kommerziellen Spielen verwendet werden dürfen. Auch hier möchte der Zeichner (*Brent Anderson*) erwähnt werden.
  * [Finding graphical tiles](http://www.roguebasin.com/index.php?title=Finding_graphical_tiles) ist eine Sammlung von Links für größtenteils nichtanmierte Tiles für Rogue-ähnliche Spiele.

## Programmiersprachen und Entwicklungsumgebungen

  * [Bitsy](Bitsy)
  * [Blender](Blender)
  * [Choice Script](Choice Script)
  * [cocos2d](cocos2d) und [Cocos2D-Swift](Cocos2D-Swift) mit [SpriteBuilder](SpriteBuilder)
  * [Ct.js](ctjs)
  * [Elm](Elm)
  * [Flixel](Flixel)
  * [Flutter](Flutter)
  * [Game Maker](Game Maker)
  * [GameSalad](GameSalad)
  * [Godot](Godot)
  * [Greenfoot](Greenfoot)
  * [Ink und Inkle](Ink und Inky)
  * [Java ME](Java ME)
  * [Löve](Löve)
  * [LowRes NX](LowRes NX)
  * [Magic Words](Magic Words)
  * [microStudio](microStudio)
  * [Nodebox for OpenGL](Nodebox for OpenGL)
  * [P5.play](p5play)
  * [Panda3D](Panda3D)
  * [Perlenspiel](Perlenspiel)
  * [PlayN](PlayN)
  * [Processing](Processing)
  * [PursuedByBear](ppb)
  * [PuzzleScript](PuzzleScript)
  * [Python Arcade Library](arcade)
  * [PyGame](PyGame)
  * [Pygbag](Pygbag), Pygame im Browser
  * [PyGame Zero](PyGame Zero)
  * [PyGlet](PyGlet)
  * [Pyxel](Pyxel)
  * [Ren'Py](Ren'Py)
  * [Quadplay](Quadplay)
  * [Simple Python Game Library (SPGL)](Simple Python Game Library (SPGL))
  * [TIC-80](TIC-80)
  * [Tilengine](Tilengine)
  * [Tuesday JS Visual Novel Engine](Tuesday JS Visual Novel Engine)
  * [Twine](Twine) und [Twine 2](Twine 2)
  * [Unity](Unity)
  * [Unreal Engine](Unreal Engine)
  * [V-Play](V-Play)
  * [Yarn](Yarn)

  * [Spieleprogrammierung mit HTML5 und JavaScript](Spieleprogrammierung mit HTML5 und JavaScript)
  * [Spieleprogrammierung mit dem Google Web Toolkit](Spieleprogrammierung mit dem GWT)
  * [Spieleprogrammierung mit JavaFX](Spieleprogrammierung mit JavaFX)

## Bibliotheken

  * [jMonkeyEngine](jMonkeyEngine)
  * [Python-tcod](Python-tcod)
  * [SFML](SFML)

## Physik-Engines

- [Pymunk](Pymunk)

## Tools

  * [Moore-Nachbarschaft](Moore-Nachbarschaft)
  * [Von-Neumann-Nachbarschaft](Von-Neumann-Nachbarschaft)

### Map-Editoren

  * [Tiled](Tiled)
  * [Trizbort.io](trizbortio)
  
### Avatare (für Visual Novels etc.) selber bauen

- [Avatare](Avatare)
- [Avataaars](Avataaars)
- [Anime Avatar Maker Tool](Anime Avatar Maker Tool)

## In diesem Wiki

  * Spielidee: [In den Labyrinthen von Buchhaim](In den Labyrinthen von Buchhaim)

### Retro Games

  * Haben eine [eigene Seite](Retro Games) bekommen.

## Geschichte

  * Spiegel Online: [Wie preußische Militärs den Rollenspiel-Ahnen erfanden](http://www.spiegel.de/netzwelt/spielzeug/0,1518,625745,00.html)
  * Dungeons im Rollenspiel: [Im Hobbykeller der Phantasten](http://www.spiegel.de/netzwelt/spielzeug/0,1518,648493,00.html) (Spiegel Online)
  * Felix Knoke: [Spiel mit mir, Pixelboy](http://www.spiegel.de/netzwelt/games/retro-games-spiel-mit-mir-pixelboy-a-709374.html) (8-Bit-Games bei Spiegel Online]]
  * Spiegel Online: [Die besten Spiele für das iPad. Diesmal: Gesellschaftsspiele](http://www.spiegel.de/fotostrecke/fotostrecke-75817.html)

  

## Links

  * Jörg Kantel: *[Programmieren mit HyperTalk](http://www.kantel.de/hc/), Kapitel 7: [Im Kerker des Grauens](http://www.kantel.de/hc/hypertalk7.html)*. Schon 1996 hatte ich für einen Artikel in der Zeitschrift MacOpen ein kleines Dungeon-Game in [HyperCard](HyperCard) programmiert.
  * Jan Bojaryn: *[Games entwickeln, ohne programmieren zu können](http://www.zeit.de/digital/games/2013-12/indie-games-entwickler-programmieren)*. Games werden größer und teurer. Aber im Schatten der Giganten wächst eine Nische: Simple aber schöne Spiele, geschaffen von Einzelgängern ohne technische Kenntnisse, Zeit Online vom 16. Dezember 2013
  * Wendy Despain: *[Der Quellcode ist das Herz und die Seele von Spielen!](http://www.golem.de/news/interactive-storytelling-der-quellcode-ist-das-herz-und-die-seele-von-spielen-1403-104997.html)*. Spiele können wunderbare Geschichten erzählen - oder daran scheitern. Die Autorin erklärt im Interview, warum das Verhältnis zwischen Autor und Entwicklern dafür entscheidend ist. Golem.de vom 17. März 2014

## Literatur

  * Ari Feldmann: *Designing Arcade Computer Game Graphics*, Plano (Wordware Publishing) 2001. [Kann hier kostenlos heruntergeladen werden](http://wiki.yoyogames.com/index.php/Ari_Feldman%27s_Book_on_Game_Graphics).
  * Jürgen Franke, Werner Fuchs: *Knaurs Buch der Rollenspiele*, München (Knaur TB 3795) 1985
  * Andy Harris: *[Game Programming](Game Programming)*, Hoboken, NJ (Wiley) 2007
  * Thomas Lucka: *[Mobile Games. Spieleprogrammierung für Handys mit Java ME](Mobile Games)*, München (Hanser) 2008
  * Will McGugan: *[Beginning Game Development with Python and Pygame](Beginning Game Development with Python and Pygame)*, Berkeley, CA (Apress) 2007
  * Robert Wolf: *Konfliktsimulations und Rollenspiele: Die neuen Spiele*, Köln (dumont taschenbücher 208) 1988
  