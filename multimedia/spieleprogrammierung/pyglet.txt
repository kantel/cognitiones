#title "PyGlet"

<%= imageref("pyglet") %>

**PyGlet** (BSD-Lizenz) ist ist eine auf OpenGL basierendes [Python](Python)-Framework. Ziel ist die Entwicklung von Spielen und andere graphischen Anwendungen. Im Gegensatz zu [PyGame](PyGame) werden Multi-Window-Anwendungen unterstützt.

Pyglet hat eine eingebaute Hilfe für Maus- und Tastaturereignisse. Es kann mehrere multimediale Dateiformate laden (möglich mit AVbin). OpenAL (plattformunabhängig), DirectSound (Windows) oder ALSA (Linux) können für räumliche Audiowiedergabe benutzt werden.

PyGlet bildet auch die Basis für [pyprocessing](pyprocessing), [NodeBox for OpenGL](NodeBox for OpenGL) und [cocos2d](cocos2d).

## Installation

PyGlet kommt für [MacOS X](MacOS X) mit einem Disk-Image, der einen One-Click-Installer beinhaltet.

## Video

<iframe width="480" height="296" src="http://www.youtube.com/embed/EKjzT4nEQow" frameborder="0" allowfullscreen></iframe>

Pyglet Piñata: Create space invaders with pyglet in 40 minutes

## Beispiel

	#!/usr/local/bin/python

	import pyglet

	window = pyglet.window.Window(width=320, height=240, caption="Hallo Welt")

	label = pyglet.text.Label('Hello, world', font_name='Times New Roman',
	font_size=36, x=window.width/2, y=window.height/2, anchor_x='center', anchor_y='center')

	@window.event
	def on_draw():
	   window.clear()
	   label.draw()

	pyglet.app.run()

## Tutorials

  * [2D Graphics With pyglet and OpenGL](http://tartley.com/?p=250)
  * [Creating a game with PyGlet and Python](http://www.learningpython.com/2007/11/10/creating-a-game-with-pyglet-and-python/)
  * [Game Programming Tutorial](http://www.slideshare.net/r1chardj0n3s/introduction-to-game-programming-tutorial) (mit PyGlet)
  * [PyGlet Game Programming Tutorial](http://steveasleep.appspot.com/pyglettutorial)

## Ähnliche Programm in diesem Wiki

  * [cocos2d](cocos2d)
  * [PyGame](PyGame)

## Links

  * [PyGlet Home](http://www.pyglet.org/documentation.html)
  * [PyGlet Programming Guide](http://www.pyglet.org/doc/programming_guide/)
  * Vielleicht dafür nützlich: [Rabbyt](http://matthewmarshall.org/projects/rabbyt/) - eine schnelle Sprite-Bibliothek für Python
  


  