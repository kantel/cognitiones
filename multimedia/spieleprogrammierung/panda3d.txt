#title "Panda3D"

<iframe width="480" height="295" src="http://www.youtube.com/embed/dsfXO0vr0hk" frameborder="0" allowfullscreen></iframe>

**Panda3D** ist eine freie Spiel-Engine, die ursprünglich von *Disney VR* erstellt wurde und aktuell von Disney, der *Carnegie Mellon University* (CMU) und einigen freiwilligen Programmierern weiterentwickelt wird. Die Panda3D-Engine ist für GNU/[Linux](Linux), Windows und [MacOS X](MacOS X) verfügbar. Sie wird wahlweise über [C++](C++) programmiert oder mittels [Python](Python) geskriptet und ermöglicht so auch Einsteigern das Erstellen von individuellen [3D](3D)-Programmen.

## Links

  * [Panda3D Home](http://www.panda3d.org/) an der CMU
  * [Download-Seite](https://www.panda3d.org/download.php?sdk&version=1.7.2) *(Version 1.7.2 mit One-Click-Installer für MacOS X)*
  * <%= wpde("Panda3D") %> in der Wikipedia
  * Oliver Frommel: *[3D-Welten mit Python und Panda3D](http://www.linux-community.de/Internal/Artikel/Print-Artikel/LinuxUser/2006/11/3D-Welten-mit-Python-und-Panda3D)*, LinuxUser vom 1. November 2006