#title "Pyxel"

<%= imageref("pyxellogo") %>

**Pyxel** ist eine freie (MIT-Lizenz), plattformübergreifende (macOS, Linux, Windows) 2D-Retro-Game-Bibliothek von *Takashi Kitao* für [Python3](Python). Ihre Spezifikationen orientieren sich an den Leistungsumfang alter Spielekonsolen. Vorbilder waren unter anderem die in [Lua](Lua) geschriebene virtuellen Konsolen [PICO-8](PICO-8) und [TIC-80](TIC-80). Die Farbpalette umfaßt maximal 16 Farben, es können drei 256x256 Pixel große *Image Banks* und acht ebenfalls 256x256 Pixel große *Tilemaps* genutzt werden. Die Anzahl der gleichzeitig spielbaren Soundkanäle ist auf vier beschränkt.

Pyxel verfügt je über einen eingebauten Image-, Tilemap-, Sound- und Musik-Editor.

Es gibt zwei Arten von Pyxel, eine Package-Version und eine Standalone-Version. Die Package-Version von Pyxel nutzt Pyxel als ein Python Extension Modul; die Standalone-Version von Pyxel verwendet Pyxel als eigenständiges Werkzeug, das nicht von Python abhängig ist.

## Video-Tutorials

<iframe width="560" height="315" src="https://www.youtube.com/embed/m7msykl-P9I" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

**[Pyxel - Making of a spaceship game](https://www.youtube.com/watch?v=m7msykl-P9I)** ist eine zwanzigminütige *Tour de Force* durch Pyxel, aber da es den [Quellcode auf GitHub](https://github.com/r3ap3rpy/pyxel-project/blob/master/SpaceShips.py) gib, kann man das Video ja zwischendurch anhalten, kurz luftholen und nachlesen.

<iframe width="560" height="315" src="https://www.youtube.com/embed/Qg16VhEo2Qs" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Dieses **[Python Retro Game Tutorial](https://www.youtube.com/watch?v=Qg16VhEo2Qs)** mit Pyxel ist zwar bedeutend gemütlicher, hat aber dafür auch eine Laufzeit von mehr als zwei Stunden. Auch hier findet Ihr den [Quellcode und die verwendeten Assets](https://github.com/CaffeinatedTech/Python_Nibbles) auf GitHub.

## Tutorials

Es gibt vier Tutorials zu Pyxel auf LinuxTut. Da die Site jedoch völlig unaufgeräumt ist und viele (interne) Links nicht mehr funktionieren, habe ich sie hier einzeln verlinkt, hoffentlich in der Reihenfolge, die der Autor vorgesehen hat.&nbsp;🤪

- [The first step to making a game with Pyxel](https://www.linuxtut.com/en/c187757987e37b9ba7da/)
- [Make a game with Pyxel -- Use an editor](https://www.linuxtut.com/en/2cf51f4edfbda7864929/)
- [Make a simple maze game with Pyxel](https://www.linuxtut.com/en/81464fb52e6d7504d14c/)
- [Make a simple maze game with Pyxel -- Make enemies appear](https://www.linuxtut.com/en/b20402d3d92d80edb26f/)

## Links

- [Pyxel @ GitHub](https://github.com/kitao/pyxel)
- [Pyxel Doku deutsch](https://github.com/kitao/pyxel/blob/main/doc/README.de.md)  
- [Pyxel @ PyPI](https://pypi.org/project/pyxel/)  