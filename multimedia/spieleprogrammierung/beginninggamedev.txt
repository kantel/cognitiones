#title "Beginning Game Development with Python and Pygame"

**Beginning Game Development with Python and Pygame** ist ein wirklich wunderbares Buch, das verständlich und unterhaltsam in [PyGame](PyGame) einführt. Natürlich kann auf etwa 300 Seiten nicht der gesamte Umfang des Frameworks behandelt werden, aber der Autor führt so in die Grundlagen ein, das einem die Weiterarbeit mit der leicht chaotischen Originaldokumentation und der dankenswerterweise im Anhang abgedruckten Referenz nicht mehr so schwerfällt. Behandelt werden erst einmal die Grundlagen: Wie man überhaupt etwas auf den Bildschirm bringt, wie man das dann auch noch animiert und wie man Eingaben des Benutzers erkennt und verarbeitet. Dann, wie man es bunt bekommt, wie man die dritte Dimension erobert, wo man die »Kamera« aufstellt und wie man Sound programmiert. Und als abschließenden Höhepunkt zeigt uns der Autor, wie man PyGame und OpenGL zusammen verwendet, um noch eindrucksvollere [3D](3D)-Effekte zu bekommen.

<div style="float:right; margin-left: 8px"><iframe src="http://rcm-de.amazon.de/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=derschockwell-21&o=3&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=1590598725" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe></div>

Die zur Illustration verwendeten Beispielprogramme sind vollständig, aber kurz, so daß sie sich auf das Wesentliche konzentrieren und so sowohl den Lern- wie auch den Aha-Effekt verstärken. Wer also schon immer einmal PyGame ausprobieren wollte aber ob der Dokumentationslage zurückschreckte, dem ist jetzt geholfen. Das Buch hat sich auch bei mir einen festen Platz neben dem Terminal erobert.

## Bibliographische Angaben

  * Will McGugan: *Beginning Game Development with Python and Pygame*, Berkeley, CA (Apress) 2007

