#title "TIC-80"

**TIC-80** ist eine freie (MIT-Lizenz) »Fantasy-Konsole«, inspiriert von [Pico-8](Pico-8) (proprietär). Sie funktioniert (unter anderem) mit Android, HTML5 (in WebAssembly4), Linux, macOS und Windows und kann in den Skriptsprachen [Lua](Lua), [JavaScript](JavaScript), [MoonScript](MoonScript), [Fennel](Fennel), [Wren](Wren) und [Squirrel](Squirrel) programmiert werden.

TIC-80 besitzt eingebaute Werkzeuge zum Programmieren, zum Erstellen von Sprites und Karten und von Tönen und Melodien. Die Konsole kann entweder lokal auf dem Rechner oder online im Browser programmiert werden.

Da in der Beschränkung die Kraft liegt, unterliegt die TIC-80 unter anderem folgenden Einschränkungen:

- <b>Anzeige</b>: 240x136 Bildpunkte (»Pixel«), 16 Farben in einer Palette
- <b>Sprites und Tiles</b>: 256 8x8 Sprites und 256 8x8 Hintergrundkacheln (»Tiles«)
- <b>Karte</b>: 240x136 Zellen, 1920x1088 Pixel
- <b>Code</b>: maximale Codelänge: 64 KB

## Tutorials

[[dr]]<iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//ws-eu.amazon-adsystem.com/widgets/q?ServiceVersion=20070822&OneJS=1&Operation=GetAdHtml&MarketPlace=DE&source=ss&ref=as_ss_li_til&ad_type=product_link&tracking_id=derschockwell-21&language=de_DE&marketplace=amazon&region=DE&placement=B092PG7P83&asins=B092PG7P83&linkId=9cace3255e8196389c11eb016b956f69&show_border=true&link_opens_in_new_window=true"></iframe>[[dd]]

- Code · Kultur · Bonn: [TIC-80, eine Fantasiekonsole](https://codekulturbonn.de/tic80.html)
- [A step by step introduction to TIC 80](https://github.com/winnichenko/BLOB-87/wiki/A-step-by-step-introduction-to-TIC-80,-Part-1-The-Default-Cart)
- Im TIC-80-Wiki gibt es eine [Seite mit vielen Tutorials](https://github.com/nesbox/TIC-80/wiki/tutorials)
- *Bruno Oliveira*: [Why I spent almost a year building an RPG game for a fantasy console](https://medium.com/@btco_code/why-i-spent-almost-a-year-building-an-rpg-game-for-a-fantasy-console-2bbe2e1d8cb8) (falls Ihr Eure drei freien Zugriffe auf Medium.com je Monat schon aufgebraucht habt, liegt der Beitrag entweder hinter einer Bezahlschranke oder Ihr müßt auf den nächsten Montatsersten warten).
- *Bruno Oliveira*: [Writing a platformer for the TIC-80 fantasy console](https://medium.com/@btco_code/writing-a-platformer-for-the-tic-80-virtual-console-6fa737abe476) (ebenfalls Medium.com).
- *Bruno Oliveira*: W[riting a retro 3D FPS engine from scratch](https://medium.com/@btco_code/writing-a-retro-3d-fps-engine-from-scratch-b2a9723e6b06) (noch einmal Medium.com).

## Videos

<iframe width="560" height="315" src="https://www.youtube.com/embed/V7W4m8fKqmI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

[TIC-80 Fantasy Console Sizecoding Seminar](https://www.youtube.com/watch?v=V7W4m8fKqmI)

## Links

- [TIC-80 Home](https://tic80.com/)
- [TIC-80 @ GitHub](https://github.com/nesbox/TIC-80)
- [TIC-80 Wiki](https://github.com/nesbox/TIC-80/wiki)

- [TIC-80-VSCode](https://marketplace.visualstudio.com/items?itemName=Gi972.tic80-vscode) (This is a extension for running Lua files in TIC-80 for Mac)
- TIC-80 im *Schockwellenreiter*: [Riesenlinkschleuder für (Retro-) Spieleentwickler (und solche, die es werden wollen)](http://blog.schockwellenreiter.de/2020/12/2020120802.html)