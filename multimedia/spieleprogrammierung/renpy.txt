#title "Ren'Py"
#updated "5. Juli 2023"

<%= imageref("renpy-logo") %>

**Ren’Py Visual Novel Engine** ist eine kostenlose und frei verwendbare (MIT-Lizenz), in [Python](Python) geschriebene Engine für die Erstellung von *[Visual Novels](Visual Novels)*, aber auch von anderer Software, wie beispielsweise von Textadventures, Geschichten, Präsentationen und animierten Illustrationen.

Mit dem Programm arbeitet man über einfache Regieanweisungen die in einen Texteditor eingegeben werden. Neben eigenen Anweisungen -- beispielsweise für die Animation von Bildern -- lassen sich Ren’Py-Programme auch mit der Programmiersprache [Python](Python) erweitern.

## Funktionen

*Ren’Py* bietet zunächst von Beginn an ein Grundgerüst für Einstellungen wie Sound, Graphik, speicherbare Spielstände etc. Zum Basisbereich gehören ebenfalls die Darstellungen von Graphiken und Verzweigungsmöglichkeiten und Menüs. Für die Animation von Graphiken, beispielsweise von Charakterbildern, läßt sich die eigene Kommando-Sprache `ATL` *(Animation and Transformation Language)* nutzen, die diese Funktionen stark vereinfacht und sie leichter lesbar und anwendbar macht.

<iframe width="560" height="315" src="https://www.youtube.com/embed/h58CKf57YDk" frameborder="0" allowfullscreen></iframe>

*Ren’Py* unterstützt zudem nahezu alle Funktionen, die für eine *Visual Novel* von Bedeutung sind, ohne an dieses Genre gebunden zu sein. Inklusive verschiedene Textdarstellungen, Synchronisation, Zurückspulen zu vorherigen Punkten in einer Geschichte, eine Vielzahl von Bildübergängen und weitere. Ren’Py-Skripte haben eine drehbuchähnliche Syntax und können zusätzlich Elemente aus dem Python-Code enthalten, die es dem Benutzer erlauben eigene Funktionen einzubauen.

Multimedia-Fähigkeiten werden mit Hilfe von [PyGame](PyGame) intern möglich gemacht. Die Programmiersprache Python bietet eine mächtige Erweiterungsmöglichkeit der Skripte. Unterstützt werden Windows, MacOS X, Linux (auch in einigen Distributionen enthalten), Android-Systeme wie beispielsweise die Ouya-Konsole und iOS.

*Ren'Py* ist schon weit über 12 Jahre auf dem Markt und wird aktuell auch noch weiter gepflegt. Die jüngste Version (Version 8.1, Stand Juli 2023) ist vom 7. Juni 2023.

## Literatur

- Arne Weißendorn: *[Ren'Py als Entwicklertool für 2-D-Spiele](http://www.freiesmagazin.de/mobil/freiesMagazin-2011-11-bilder.html#11_11_renpy)*, freiesMagazin vom November 2011

## Tools

- [Ren'Py language support in Atom](https://atom.io/packages/language-renpy)

## Tutorials

- Creating A Visual Novel With Ren'Py, [Part 1: The Basics](https://forums.tigsource.com/index.php?topic=3783.0), [Part 2: The content](https://forums.tigsource.com/index.php?topic=4233.0)
- [Ren'Py: Create Your Own Visual Novel](https://www.bu.edu/lernet/artemis/years/2011/slides/renpy.pdf) (PDF)

## Ähnliche Software in diesem Wiki

- [Twine](Twine) und [Twine 2](Twine 2)

## Links

- [Ren'Py Home](https://www.renpy.org/)
- [Ren'Py Dokumentation](https://www.renpy.org/doc/html/)
- [Ren'Py @ GitHub](https://github.com/renpy/renpy)
- [Ren'Py Wiki](https://www.renpy.org/wiki/renpy/Wiki_Home_Page), veraltet
- [Ren'Py in der Wikipedia](https://de.wikipedia.org/wiki/Ren%E2%80%99Py)
- [Ren'Py im Ubuntuusers Wiki](https://wiki.ubuntuusers.de/RenPy/)