#title "Cocos2D-Swift"

**Cocos2D-Swift** (ursprünglich *cocos2d-iphone*) ist ein in [Objective-C](Objective-C) geschriebenes, freies (MIT-Lizenz) Framework, um unter [MacOS X](MacOS X) (mit Xcode) Spiele für Smartphones und Tablets zu entwickeln (iOS und [Android](Android)).

Zu Cocos2D-Swift gehört auch der [SpriteBuilder](SpriteBuilder), ein ebenfalls freier (MIT-Lizenz), visueller Editor (ab MacOS X 10.9).

Nachdem das ursprünglich in [Python](Python) geschriebene (Original-) [cocos2d](cocos2d) nicht mehr weiterentwickelt zu werden scheint, sieht sich Cocos2D-Swift als legitimer Nachfolger und nennt sich nur noch Cocos2D.

## Links

  * [Cocos2D-Swift Home](http://www.cocos2d-swift.org/)
  * [Cocos2D-Swift Downlaod](http://www.cocos2d-swift.org/download) (Binaries)
  * [Quellcode @ GitHub](https://github.com/cocos2d/cocos2d-iphone/)
  * [Cocos2D-Swift Dokumentation](http://www.cocos2d-swift.org/docs)
  * [Cocos2S-Swift Blog](http://www.cocos2d-swift.org/blog)
  * <%= wp("Cocos2d") %> in der Wikipedia