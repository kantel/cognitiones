#title "Ink und Inky"

**Ink** ist eine freie (MIT-Lizenz) Skriptsprache für interaktive Fiktion. Im Gegensatz zum verwandten [Twine](Twine 2) ist es nicht auf eine Game Engine (Ausgabe) festgelegt, sondern exportiert nach JSON, ins Web (JavaScript), aber es gibt auch ein Plug-in für [Unity](Unity).

<%= imageref("inkle") %>

Ink wurde von der Firma Inkle entwickelt, die damit ihre eigenen Spiele, wie zum Beispiel »[80 Days](https://www.inklestudios.com/80days/)« oder »[Sorcery!](https://www.inklestudios.com/sorcery/)« schreiben. Man erkennt daran, daß sich Ink nicht so schwer wie Twine bei der Einbindung multimedialer Inhalte tut. Ink ist noch jung und wurde erst im März 2016 als *Open Source* freigegeben.

Ink kann mit dem Kommandezeilenwerkzeug **Inklecate** übersetzt werden, einfacher ist die Bearbeitung in den meisten Fällen jedoch mit der für Ink entwickelten IDE **Inky**. Sie steht ebenfalls unter der MIT-Lizenz und ist ein freier, plattformübergreifender (Windows, Linux, Mac) Editor für Ink. Es ist eine [Electron](Electron)-Anwendung und sieht ungefähr so aus wie die [Markdown](Markdown)-Editoren [MacDown](MacDown) oder [Haroopad](Haroopad): Links der Quelltext und rechts das herausgeschriebene Spiel. Der Editor ist noch *alpha*, scheint aber schon recht gut zu funktionieren.

## Berichte und Tutorials

- [Writing web-based interactive fiction with ink](https://www.inklestudios.com/ink/web-tutorial/) ist ein Tutorial für absolute Anfänger

- [Writing with Ink](https://github.com/inkle/ink/blob/master/Documentation/WritingWithInk.md) ist das vollständige Handbuch für die Skript- respektive Auszeichnungssprache

- *Joseph Hunfrey*: »[Open sourcing 80 Days' narrative scripting language: ink](https://www.gamasutra.com/blogs/JosephHumfrey/20160330/268974/Open_sourcing_80_Days_narrative_scripting_language_ink.php)«, Gamasutra vom 30. März 2016

- *Luis Diaz*: »[How Unity and Ink are helping us build a narrative game](https://www.gamasutra.com/blogs/LuisDiaz/20160610/274697/How_Unity_and_Ink_are_helping_us_build_a_narrative_game.php)«, Gamasutra vom 10. Juni 2016

- *John Ingold*: »[Postmortem: Steve Jackson's Sorcery! series by Inkle](https://www.gamasutra.com/view/news/280373/Postmortem_Steve_Jacksons_Sorcery_series_by_Inkle.php)«, Gamasutra vom 22. September 2016

- *Edwin McRae*: »[Narrative Design with Ink Script](https://www.edmcrae.com/article/narrative-design-with-ink-script)«

- Von *Edwin McRae* gibt es auch eine mehrteilige Tutorial-Reihe zu Ink-Script. Bisher veröffentlicht sind [Teil 1](https://www.edmcrae.com/article/learning-ink-script-tutorial-one), [Teil 2](https://www.edmcrae.com/article/learning-ink-script-tutorial-two), [Teil 3](https://www.edmcrae.com/article/learning-ink-script-tutorial-three), [Teil 4](https://www.edmcrae.com/article/learning-ink-script-tutorial-four), [Teil 5](https://www.edmcrae.com/article/learning-ink-script-tutorial-five) und [Teil 6](https://www.edmcrae.com/article/learning-ink-script-tutorial-six). Weitere Teile scheinen nicht geplant zu sein, ich habe aber sicherheitshalber sein Blog in meinen Feedreader gepackt.

## Links

- [Ink Homepage](https://www.inklestudios.com/ink/)
- [ink @ GitHub](https://github.com/inkle/ink)
- [Inky Download](https://github.com/inkle/ink/releases)
- [Inklecate Download](https://github.com/inkle/ink/releases)
- [Ink-Unity integration](https://github.com/inkle/ink-unity-integration)
- [Inkle Homepage](https://www.inklestudios.com/)
