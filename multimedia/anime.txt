#title "Anime\.js"

**Anime.js** ist eine schlanke, freie (MIT-Lizenz) JavaScript-Animationsbibliothek mit einer einfachen, aber leistungsstarken API. Sie funktioniert mit [CSS](CSS)-Eigenschaften, [SVG](SVG), DOM-Attributen und JavaScript-Objekten mit den Browsern Chrome (24+), Safari (8+), Internet Explorer/Edge (11+), Firefox (32+) und Opera (15+).

<iframe width="560" height="315" src="https://www.youtube.com/embed/g7WnZ9hxUak" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Installation

Via npm

~~~bash
npm install animejs --save
~~~

oder manueller [Download](https://github.com/juliangarnier/anime/archive/master.zip).

## Literatur

- *Andreas Domin*: [Anime.js: Kinderleicht tolle Animationen für JavaScript und SVG](https://t3n.de/news/animejs-javascript-library-animationen-1137638/), t3n vom 20. Januar 2019
- *Björn Bohn*: [Alles animieren mit Anime.js 3.0](https://www.heise.de/developer/meldung/JavaScript-Alles-animieren-mit-Anime-js-3-0-4277510.html), heise online vom 16. Januar 2019
- *Vladimir Stepura*: [20 Anime JS Examples](https://freefrontend.com/anime-js-examples/), Free Frontend vom 18. September 2018
- *Mats Autzen*: [Anime.js: Was taugt die JavaScript-Animation-Library?](https://entwickler.de/online/javascript/anime-js-animation-javascript-579762046.html), entwickler.de vom 20. Februar 2017

## Links

- [Anime.js Home](https://animejs.com/)
- [Anime.js @ GitHub](https://github.com/juliangarnier/anime)
- [Anime.js Dokumentation](https://animejs.com/documentation/)
- [Anime.js Download](https://github.com/juliangarnier/anime/archive/master.zip)