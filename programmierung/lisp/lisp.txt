#title "Lisp"

**Lisp** ist die Mutter aller funktionalen Spreachen und etwas für die Freunde des *Klammerschlußgesetzes*.

## In diesem Wiki

  * [Clojure](Clojure)
  + [ClojureScript](ClojureScript)
  * [Common Lisp](Common Lisp)
  * [Guile](Guile)
  * [XMLisp](XMLisp)
  * [Scheme](Scheme)
  * [DrRacket](DrRacket)

## Links

  * <%= wpde("Lisp") %> in der Wikipedia