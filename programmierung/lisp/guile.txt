#title "Guile"

<%= imageref("guile") %>

**Guile** (GPL 3), ein Akronym für *GNU Ubiquitous Intelligent Language for Extensions*, ist die offizielle Erweiterungssprache für das Betriebssystem GNU und wird im Rahmen des GNU-Projekts entwickelt. Guile ist ein Interpreter für die Programmiersprache [Scheme](Scheme), einen [Lisp](Lisp)-Dialekt. Die Compiler-Infrastruktur, Bibliotheken und dynamische Programmierumgebung machen Guile zu einer mächtigen Sprache zum Schreiben von Anwendungen. Guile wird als Programmbibliothek implementiert, die in andere Programme eingebunden werden kann, um deren Erweiterbarkeit zu fördern.

## Links

- [Guile Home](https://www.gnu.org/software/guile/)
- [Guile Tutorial](https://pingus.seul.org/~grumbel/tmp/guile-1.6.0/guile-tut_toc.html)
- [Guil in der Wikipedia](https://de.wikipedia.org/wiki/GNU_Guile)