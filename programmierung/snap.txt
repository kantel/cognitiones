#title "Snap! (BYOB)"

**Snap!**, bis zu Version 3.1.1 unter dem Namen **BYOB** bekannt (englische Abkürzung von *Build Your Own Blocks*, deutsch: »Bau deine eigenen Blöcke«), ist eine freie (AGPL 3.0) auf [Scratch](Scratch) aufbauende und von [Scheme](Scheme) und damit auch [Lisp](Lisp) inspirierte erziehungsorientierte visuelle Programmiersprache inklusive ihrer Entwicklungsumgebung für fortgeschrittene Schüler und für die Erwachsenenbildung.

<%= imageref("snap") %>

Snap!/BYOB erweitert Scratch um komplexere und abstraktere Konzepte, die in Scratch zugunsten der Kindertauglichkeit bisher fehlten. Trotz der spielerischen Anmutung, die es sich aus seiner Scratch-Herkunft bewahrt hat, geht BYOB mit seinen anschaulichen und durchgängigen First-Class-Objekten und Meta-Programmierung weit über die Möglichkeiten traditioneller Programmier-Lern-Sprachen hinaus. Unter anderem kann der Lernbereich »Algorithmen und Datenstrukturen« mit BYOB direkt und vollständig abgedeckt werden, während Programmierkenntnisse nebenbei erworben werden.

Snap! kann entweder online oder aber auch lokal genutzt werden und läuft (mindestens) unter Windows, macOS und Linux.

## Literatur

[[dr]]<%= html.getLink(imageref("codiertekunst"), "http://codiertekunst.joachim-wedekind.de/") %>[[dd]]

- *Joachim Wedekind*: [Codierte Kunst – Programmieren mit Snap](http://codiertekunst.joachim-wedekind.de/), Tübingen (Selbstverlag) 2018

## Links

- [Snap! Home](https://snap.berkeley.edu/)
- [Snap! @ GitHub](https://github.com/jmoenig/Snap)
- [Snap! in der Wikipedia](https://de.wikipedia.org/wiki/Snap!_(BYOB))
- [Snap Reference Manual](https://snap.berkeley.edu/SnapManual.pdf) (<%= imageref("pdf") %>)
- [Snap! offline](https://github.com/jmoenig/Snap/blob/master/OFFLINE.md)
- [Snap! Download](https://github.com/jmoenig/Snap/releases/latest)

