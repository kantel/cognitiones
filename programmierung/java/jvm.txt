#title "JVM"

Die **Java Virtual Machine** (JVM) ist der Teil der [Java](Java)-Laufzeitumgebung (JRE) für Java-Programme, der für die Ausführung des *Bytecode* verantwortlich ist. Hierbei wird im Normalfall jedes gestartete Java-Programm in seiner eigenen virtuellen Maschine (VM) ausgeführt. Der andere Teil der Java-Laufzeitumgebung sind die Java-Klassenbibliotheken.

Die JVM dient dabei als Schnittstelle zur Maschine und zum Betriebssystem und ist für die meisten Plattformen verfügbar. (z. B. [Linux](Linux), [MacOS X](MacOS X), Palm OS, Solaris, Windows, usw.). Die JVM ist in den Programmiersprachen [C](C) und [C++](C++) geschrieben.

## Links

  * Die [Java Virtual Machine](http://de.wikipedia.org/wiki/Java_Virtual_Machine) in der Wikipedia