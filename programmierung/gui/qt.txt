#title "Qt"

<%= imageref("qt-logo") %>

**Qt** (lies englisch *cute*, Aussprache [kjuːt]) ist eine [C++](C++)-Klassenbibliothek für die plattformübergreifende Programmierung graphischer Benutzeroberflächen. Es gibt Qt sowohl unter der GPL als auch unter einer kommerziellen Lizenz, seit der Version 4.5 ist zusätzlich die LGPL hinzugekommen.

Es gibt auch Anbindungen für andere Programmier- und Scriptsprachen, für [Python](Python) sind dies [PyQt](http://www.riverbankcomputing.co.uk/software/pyqt/intro) und [PySide](http://www.pyside.org/).

## Links

  * [Qt Home](http://qt-project.org/)
  * [Qt bei Nokia](http://qt.nokia.com/products/)
  * <%= wpde("Qt (Bibliothek)") %> in der Wikipedia





