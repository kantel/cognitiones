#title "wxWidgets"

<%= imageref("wx-logo") %>

**wxWidgets** (ehemals **wxWindows**) ist eine quelloffene Klassenbibliothek zur Entwicklung graphischer Benutzeroberflächen (GUI). Sie wird in der Programmiersprache [C++](C++) entwickelt und unter einer modifizierten LGPL lizenziert, die auch das Verbreiten von abgeleiteten Werken unter eigenen Bedingungen erlaubt. Obwohl wxWidgets in C++ implementiert ist, existieren Anbindungen für eine Vielzahl weiterer Programmiersprachen -- für Python ist dies [wxPython](wxPython). Durch seine Plattformunabhängigkeit ermöglicht wxWidgets, den GUI-spezifischen Code eines Programms bei keiner oder nur geringer Modifikation auf verschiedensten Plattformen zu kompilieren und auszuführen.

## Links

  * [wxWidgets Home](http://wxwidgets.org/)
  * <%= wpde("wxWidgets") %> in der Wikipedia