#title "pandas"

<iframe src="//player.vimeo.com/video/59324550?title=0&amp;byline=0&amp;portrait=0&amp;color=a086ee" width="560" height="346" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

Die *Python Data Analysis Library* **pandas** (BSD-Lizenz) ist eine hochperformante [Python](Python)-Bibliothek für die Analyse großer Datenmengen. Dazu führt sie das aus [R](GNU R) bekannte Konstrukt der *data frames* in Python ein.

[[dr]]<iframe src="http://rcm-eu.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=derschockwell-21&o=3&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=1449319793" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe>[[dd]]

## Links

  * [pandas Home](http://pandas.pydata.org/)
  * [Quellcode @ GitHub](https://github.com/pydata/pandas)

