#title "Voilà"

**Voilà** konvertiert ein [Jupyter Notebook](Jupyter) in eine *Standalone Application*. Dabei wird für jede Voilà-Anwendung ein eigener Jupyter-Kernel erzeugt. Eine Voilà-Anwendung kann aus jeder beliebigen, von Jupyter beherrschten Sprache erzeugt werden, also mindestens [(I)Python](IPython), [Julia](Julia), [C++](C++) und [R](GNU R).

Ebenso können Voilà-Anwendungen auch mit einem bereits bestehenden Jupyter-Server verbunden werden.

Voilà-Anwendugen sind wegen der benötigten *Kernels* nicht wirklich [statische Seiten](Statische Seiten), kommen diesen aber schon ziemlich nahe.

## Video

<iframe width="560" height="315" src="https://www.youtube.com/embed/VtchVpoSdoQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Dashboarding with Jupyter Notebooks, Voila and Widgets, SciPy 2019, *M. Breddels* und *M. Renou*.

## Literatur

- [Voilà-Announcement im Jupyter-Blog](https://blog.jupyter.org/and-voil%C3%A0-f6a2c08a4a93)
- *Maika Möbus*: [Voilà – Live-Dashboards aus Jupyter Notebooks sind da](https://entwickler.de/online/development/voila-jupyter-notebook-579900384.html), Entwickler.de vom 12. Juli 2019
- *Marianne Corvellec*: [A slideshow template for Voilà apps](https://blog.jupyter.org/a-slideshow-template-for-voil%C3%A0-apps-435f67d10b4f), 29. Oktober 2019
- *Bartosz Telenczuk*: [Configure your dashboards with Voilà gridstack template](https://blog.jupyter.org/voila-gridstack-template-8a431c2b353e), 11. November 2019
- *Sylvain Corlay*: [Voilà is now a Jupyter subproject](https://blog.jupyter.org/voil%C3%A0-is-now-an-official-jupyter-subproject-87d659583490), 29. Dezember 2019

## Links

- [Voilà @ GitHub](https://github.com/voila-dashboards/voila)
- [Voilà @ ReadTheDocs](https://voila.readthedocs.io/en/stable/)
- [Voilà @ Conda](https://anaconda.org/conda-forge/voila)
- [Voilà @ PyPi](https://pypi.org/project/voila/)
- [Voilà-Galerie](https://voila-gallery.org/services/gallery/)