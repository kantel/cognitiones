#title "Pyodide"

**Pyodide** ist ein [Mozilla](Mozilla)-Projekt, das Python-Code mit Hilfe von [WebAssembly](WebAssembly) im Browser ausführbar macht. Pyodide wird unter anderem von [Jupyter Lite](JupyterLite), [pyp5js](pyp5js) und [Anacondas](Anaconda) [PyScript](PyScript) als Engine benutzt.

## Literatur

- Rainald Menge-Sonnentag: *[Python im Browser: Mozilla überführt Pyodide in eigenständiges Projekt](https://www.heise.de/news/Python-im-Browser-Mozilla-ueberfuehrt-Pyodide-in-eigenstaendiges-Projekt-6027492.html)*. heise online vom 26. April 2021

## Links

- [Pyodide Home](https://pyodide.org/en/stable/) (inklusive Dokumentation)
- [Pyodide @ GitHub](https://github.com/pyodide/pyodide)