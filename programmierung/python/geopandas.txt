#title "GeoPandas"

**GeoPandas** ist ein freies (BSD-3-Lizenz) Projekt zur Unterstützung von Pandas-Objekten für geographische Daten. Derzeit werden `GeoSeries`- und `GeoDataFrame`-Typen implementiert, die Unterklassen von `pandas.Series` bzw. `pandas.DataFrame` sind. GeoPandas-Objekte können auf Shape-Objekte einwirken und geometrische Operationen ausführen.

## Installation

GeoPandas ist auf `conda` zu finden. Die empfohlene Installation ist die Installation über den `conda-forge`-Kanal:

~~~
conda install --channel conda-forge geopandas
~~~

Dabei werden alle Abhängigkeiten aufgelöst und erfüllt. Falls man kein [Anaconda](Anaconda)-[Python](Python) installiert hat, ist auch eine Installation über `pip` möglich:

~~~
pip install geopandas
~~~

## Ähnliche Projekte in diesem Wiki

- [Cartopy](Cartopy)
- [Folium](Folium)

## Links

- [GeoPandas Dokumentation und Homepage](https://geopandas.org/)
- [GeoPandas Installationsanleitung](https://geopandas.org/install.html)
- [GeoPandas @ GitHub](https://github.com/geopandas/geopandas)