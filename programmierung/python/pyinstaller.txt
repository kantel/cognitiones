#title "PyInstaller"
#created "27. Dezember 2022"
#updated "27. Dezember 2022"

<%= imageref("pyinstaller") %>

**PyInstaller** ist ein freier (GPL) Bundler für [Python](Python)-Anwendungen. Alle notwendigen Abhängigkeiten werden zusammen in einem Paket gebündelt, das ausgeführt werden kann. PyInstaller muß unter dem Betriebssystem installiert und ausgeführt werden, für das das erstellte Paket gedacht ist, also Windows-Apps können nur unter Windows, macOS-Apps nur unter macOS und Linux-Apps mir mit einer Linux-Kiste erstellt werden.

## Installation

PyInstaller ist auf Pip verfügbar, also sollte für eine Installation

~~~
pip install pyinstaller
~~~

ausreichend sein.

## Literatur

- Felix Schürmeyer: *[Python: Eine ausführbare Datei für Windows, Mac oder Linux erstellen!](https://hellocoding.de/blog/coding-language/python/pyinstaller-app-exe-erstellen)*, Hello Coding vom 3. Oktober 2022

## Links

- [PyInstaller Home](https://pyinstaller.org/en/stable/) – gleichzeitig Dokumentation
- [PyInstaller @ GitHub](https://github.com/pyinstaller/pyinstaller)
- [PyInstaller @ PyPI](https://pypi.org/project/pyinstaller/)

**PyInstaller im Schockwellenreiter**: [Python-Apps für (fast) jedes Betriebssystem](http://blog.schockwellenreiter.de/2019/04/2019041002.html), 10. April 2019