#title "PyCharm"

<iframe width="560" height="315" src="//www.youtube.com/embed/mD_87V79PyA?rel=0" frameborder="0" allowfullscreen></iframe>

**PyCharm** ist eine mächtige [Python](Python)-Entwicklungsumgebung (IDE). Mit der Version 3.0 wird neben der bisherigen kommerziellen Edition eine freie (Apache-Lizenz) *Community Edition* angeboten. Die *Community Edition* enthält von einer Entwicklungsumgebung zu erwartende Funktionen wie automatischer Codevervollständigung, Codeanalysen, Refactorings, Debugger, Testrunner und *vitualenv*-Support. Die *Professional Edition* unterstützt zusätzlich Techniken wie die Webframeworks [Django](Django), Pyramid, [Flask](Flask) und [web2py](web2py), Googles Platform as a Service [App Engine](Google App Engine) sowie den objektrelationalen Mapper SQLAlchemy.

## Links

  * [PyCharm Home](http://www.jetbrains.com/pycharm/)
  * [Vergleich Community Edition vs. Professional Edition](http://www.jetbrains.com/pycharm/features/editions_comparison_matrix.html)
  * [PyCharm Blog](http://blog.jetbrains.com/pycharm/)
  * <%= wpde("PyCharm") %> in der Wikipedia



