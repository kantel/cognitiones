#title "VPython 7"
#created "16. April 2023"
#updated "16. April 2023"


## Links

- [VPython 7 Home](https://vpython.org/)
- [VPython 7 Dokumentation](https://www.glowscript.org/docs/VPythonDocs/index.html)