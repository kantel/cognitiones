#title "rbenv"

**rbenv** ist ein Installations- und Versionmanager für [Ruby](Ruby).

## Links

  * [rbenv Home](http://rbenv.org/)
  * [rbenv @ Github](https://github.com/sstephenson/rbenv)
  * Matt Neuburg: [How I Installed Ruby 1.9.4](http://www.apeth.com/nonblog/stories/ruby193.html)