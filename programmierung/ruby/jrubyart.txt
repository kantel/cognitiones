#title "JRubyArt"

<%= imageref("jrubyart") %>

**JRubyArt** ist ein [Ruby](Ruby)-Wrapper für [Processing 3.0](Processing) und größer. Die Software kann sowohl reguläre Ruby-2.1- wie auch -- via [JRuby](JRuby) -- Processing-Befehle ausführen. In Fällen, wo sowohl Ruby- wie auch Processing-Bibliotheken die gleiche Funktionalität besitzen, wird empfohlen, den Ruby-Bibliotheken den Vorzug zu geben.

JRubyArt ist der Nachfolger von [Ruby-Processing](Ruby-Processing), das weiterhin mit Processing 2.2.1 läuft.

## Artikel

  * JRubyArt: [Getting Started on the Mac](https://ruby-processing.github.io/mac_start/)
  * JRubyArt: [Ruby alternative methods](https://ruby-processing.github.io/alternatives/)
  * JRubyArt: [Using The Processing API](https://ruby-processing.github.io/api/)
  * JRubyArt: [Editors and Other Resources](https://ruby-processing.github.io/editors/)
  * Learning JRubyArt: [Create a Video from a Sketch](http://monkstone.github.io/_posts/create_video/)

## Links

  * [JRubyArt Home](https://ruby-processing.github.io/)
  * [About JRubyArt](https://ruby-processing.github.io/about/)
  * [JRubyArt @ GitHub](https://github.com/ruby-processing/JRubyArt)
  * [Learning JRuby Art](http://monkstone.github.io) ist ein Blog zu JRubyArt