#title "Programmierung"
#updated "8. März 2023"

## Programmiersprachen

Ich nutze (bis jetzt) nahezu ausschließlich die Betriebssysteme [Linux](Linux) und [MacOS X](MacOS X). Das heißt, ich programmiere auch nur auf diesen Betriebssystemen. Daher sind in diesem Wiki auch nur Programmiersprachen erwähnt, die auf mindestens einem dieser Systeme laufen.

### JavaScript (und verwandtes)

  * [Dart](Dart) und [Flutter](Flutter)
  * [JavaScript](JavaScript)
  * [ActionScript](ActionScript)

### Perl

  * [Perl](Perl)

### PHP

  * [PHP](PHP)
  * [HHVM](HHVM)

### Java und Java-artiges

  * [Java](Java) und die [JVM](JVM) haben eine [eigene Seite](Java) bekommen
  * [JavaFX](JavaFX)
  * [Groovy](Groovy)
  * [Processing](Processing)
  * [Scala](Scala)

### Python

Alles über [Python](Python), die von mir bevorzugte Skriptsprache.

  * [Jython](Jython)
  * [Sikuli](Sikuli)

### Ruby

  * [Ruby](Ruby) und [MacRuby](MacRuby)
  
### Blockorientierte Programmiersprachen

- [GP Blocks](gp)
- [Nodebox 3](Nodebox 3)
- [Scratch](Scratch)
- [Snap!(BYOB)](snap)


### Sonstige

  * [BASIC](BASIC)
  * [C](C)
  * [Go](Google Go)
  * [Haskell](Haskell)
  * [HyperCard](HyperCard) mit [HyperTalk](HyperTalk)
  * [Logo](Logo)
  * [Lisp](Lisp) und [Scheme](Scheme)
  * [LiveCode Community Edition](LiveCode Community Edition)
  * [Lua](Lua)
  * Der [Wirthsche](Niklaus Wirth) Kosmos: [Pascal](Pascal), [Modula-2](Modula-2)  und [Oberon](Oberon)
  * [Prolog](Prolog)
  * [REBOL](REBOL)
  * [Scala](Scala)
  * [Smalltalk](Smalltalk)
  * [Swift](Swift)
  * [UserTalk](Frontier)
  * [Wolfram Language](Wolfram Language)

## Werkzeuge

  * [(Open Source) Datenbanken](Datenbanken)
  * [Docker](Docker)
  * [Emscripten](Emscripten)
  * [Entwicklungsumgebungen](Entwicklungsumgebungen) (IDEs)
  * [Homebrew](Homebrew)
  * [py2app](py2app), ein Tool, um macOS Binaries aus Python-Skripten zu erstellen
  * [Versionsverwaltungen](Versionsverwaltung)
  * Plattformübergreifende [GUI-Tools](GUI-Tools)

## Programmieren lernen

- [Code.org](codeorg)

## Konzepte

  * [Algorithmen und Datenstrukturen](Algorithmen und Datenstrukturen)
  * [Creative Coding](Creative Coding)
  * [Funktionale Reaktive Programmierung](Funktionale Reaktive Programmierung) (FRP)
  * [Literate Programming](Literate Programming)
  * [Programmieren als Kulturtechnik](Programmieren als Kulturtechnik)
  * [Softcomputing](Softcomputing)

## Personen

  * [Niklas Wirth](Niklaus Wirth)
  * [Admiral Grace Hopper](Grace Hopper)