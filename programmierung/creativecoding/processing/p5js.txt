#title "P5.js"
#updated "9. März 2023"

<%= imageref("p5js-ide") %>

**P5.js** ist eine von [Processing](Processing) inspirierte [JavaScript](JavaScript)-Bibliothek. Während [Processing.js](processingjs) ein Processing-Klon ist, der Processing statt nach [Java](Java) nach JavaScript übersetzt, ist P5.js eine reine JavaScript-Bibliothek, die es einfach macht, auch mit anderen [HTML5](HTML5)-Elementen zu interagieren. Daher unterscheidet sich die Syntax in einigen Fällen auch von der von Processing.

P5.js wird von der Processing-Foundation unterstützt. Es existiert ein der Processing-IDE ähnelnder Editor (zur Zeit noch Beta) für Windows und MacOS X (für Linux in Vorbereitung).

<%= imageref("sublime2") %>

P5.js kann aber auch -- wie jede andere JavaScript-Anwendung -- in jedem Editor Eurer Wahl angewandt werden (wie hier im Screenshot mit Sublime2). Ihr benötigt nur einen Ordner mit einer `index.html`, einer JavaScript-Datei, z.B. `sketch.js` und gegebenefalls auch noch einer CSS-Datei.

## Video-Tutorials

<iframe width="560" height="315" src="https://www.youtube.com/embed/8j0UDiN7my4?list=PLRqwX-V7Uu6Zy51Q-x9tMWIv9cueOFTFA" frameborder="0" allowfullscreen></iframe>

*Daniel Shifman* erklärt uns P5.js in dieser 30 Videos umfassenden Tutorial-Reihe.

## Literatur

  * Michael Byrne: *[Now Would Be a Great Time To Get Into Creative JavaScript](http://motherboard.vice.com/read/this-would-be-a-great-time-to-get-into-creative-javascripting)*, Motherboard vom 11. Oktober 2015

## In diesem Wiki

- [P5.js Widget](p5xjswidget)
- [P5.play](p5play)

## Links

  * [P5.js Home](http://p5js.org)
  * [P5.js @ GitHub](https://github.com/processing/p5.js)
  * [P5.play](http://p5play.molleindustria.org) ist eine Spielebibliothek für P5.js