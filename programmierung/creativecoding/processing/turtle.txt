#title "Turtle"

<%= imageref("turtle") %>

**Turtle** ist eine von *[Leah Buechley](http://leahbuechley.com/)* entwickelte Bibliothek für [Processing](Processing) (und damit auch für [Processing.py](processingpy)), die die aus [Logo](Logo) bekannte Schildkrötengraphik implementiert.

Die Bibliothek muß heruntergeladen und manuell installiert werden.

## Links

- [Turtle Home](http://leahbuechley.com/Turtle/), hier kann die Bibliothek heruntergeladen werden.
- [Turtle @ GitHub](https://github.com/leahbuechley/Turtle)