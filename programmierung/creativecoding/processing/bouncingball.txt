#title "Bouncing Ball"

Ein wenig Spaß mit [Processing](Processing) und Physik:

[[fixme]]

Die Bewegungen des Balls wirken doch schon sehr realistisch, oder? Dabei ist die Idee ganz einfach: Zur Geschwindigkeit des Balles wird eine Gravitationskonstante (+0,5) hinzuaddiert. Fällt der Ball, ist die Geschwindigkeit positiv, das Hinzufügen der Gravitationskonstante sorgt dafür, daß der Ball immer schneller fällt. Steigt der Ball, ist die Geschwindigkeit negativ, so daß das Hinzuaddieren bewirkt, daß der Ball immer langsamer wird. Am Scheitelpunkt ist die Geschwindigkeit gleich Null und der Ball fällt wieder, erst langsam und dann immer schneller.

## Der Code

	float speedX, speedY;
	float x, y, w, h;
	float gravity;
	PImage ball;

	void setup() {
	  ball = loadImage("ball.gif");
	  size(400, 400);
	  x = 0;
	  y = 0;
	  w = 49;
	  h = w;
	  speedX = 4;
	  speedY = 0;
	  gravity = .5;
	}

	void draw() {
	  background(255);
	  image(ball, x, y, w, h);
	  x += speedX;
	  speedY += gravity;
	  y += speedY;

	  // Kollisions-Detektion:
	  if (x > width - w) {
	    x = width - w;
	    speedX *= -1;
	  }
	  else if (x < 0) {
	    x = 0;
	    speedX *= -1;
	  }
	  else if (y > height - h) {
	    y = height - h;
	    speedY *= -1;
	  }
	  else if (y < 0) {
	    y = 0;
	    speedY *= -1;
	  }
	}

## Erweiterungen

Natürlich hüpft in der realen Welt ein Ball nicht unendlich lange auf und ab. Durch Reibungsverluste und Wärmeabgabe verliert er immer mehr Energie bis er schließlich liegen bleibt. Dies ließe sich durch Hinzufügen einer Dämpfungs- und einer Reibungskonstante (Beide < 0) simulieren. Jedes Mal, wenn der Ball den Boden berührt, wird die Geschwindigkeit in y-Richtung mit der Dämpfungsskonstante und die Geschwindigkeit in x-Richtung mit der Reibungskonstante multipliziert. Ira Greenberg schlägt für die Dämpfungskonstante den Wert 0,8 und für die Reibungskonstante den Wert 0,9 vor. Das gibt schon einen sehr realistisch wirkenden hüpfenden Ball.

Beim Spielen mit den Konstanten könnt Ihr das Verhalten eines Balles auf verschiedenen Planeten simulieren. Eine geringe Gravität (zum Beispiel 0,2) simulieren einen Ball auf dem Mond, eine hohe Gravitätskonstante (zum Beispiel 0,98) simuliert das Verhalten auf Planeten mit hoher Schwerkraft und zeigen, daß Fußball auf dem Jupiter eine verdammt schwere Angelegenheit ist.

Eine Änderung der Dämpfungs- und Reibungskonstanten wiederum simulieren das Verhalten auf verschiedenen Oberflächen (Dichte und Rauheit).

## Literatur

  * Ira Greenberg: *<a href="http://www.amazon.de/gp/product/159059617X/ref=as_li_ss_tl?ie=UTF8&camp=1638&creative=19454&creativeASIN=159059617X&linkCode=as2&tag=derschockwell-21">Processing: Creative Coding and Computational Art</a><img src="http://www.assoc-amazon.de/e/ir?t=derschockwell-21&l=as2&o=3&a=159059617X" width="1" height="1" border="0" alt="" style="border:none !important; margin:0px !important;" />*, Berkeley CA (Friends of Ed) 2007

---

<%= imageref("ball") %>

