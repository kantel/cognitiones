#title "Getting Started with Arduino"

<div style="float:right;margin-left:8px"><iframe src="http://rcm-de.amazon.de/e/cm?t=derschockwell-21&o=3&p=8&l=as1&asins=0596155514&md=1M6ABJKN5YT3337HVA02&fc1=000000&IS2=1&lt1=_blank&m=amazon&lc1=0000FF&bc1=000000&bg1=FFFFFF&f=ifr" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe></div>

**Getting Started with Arduino** ist ein wunderbares Buch, das nicht nur in den Open Source Mikrokontroller [Arduino](Arduino) und die dazugehörende, auf [Processing](Processing) basierende Programmierumgebung einführt, sondern auch in die dahinterstehende Philosophie des *Tinkering*, des Tüftelns und Bastelns einführt.

Die Einführungsbeispiele sind wirklich sehr *basic* (für Fortgeschrittene gibt es ja auch das von mir hochgelobte Buch [Making Things Talk](Making Things Talk)) und ausführlichst erklärt.

## Literaturangaben

  * Massimo Banzi: *Getting Started with Arduino*, Sebastopol (O'Reilly) 2008