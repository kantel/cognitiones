#title "unlekkerLib"

Die [Processing](Processing)-Bibliothek **unlekkerLib** *»is a collection of tools and code snippets I use frequently, and which I’ve now just barely cleaned up enough for other people to use. Instead of releasing them piece by piece, I’ve decided to bundle them together in a package hierarchy. Caveat emptor: There’s not much documentation but I do provide the source code.«* Vom Autor gibt es ein flickr-Set [mit schönen Bildern dazu](http://www.flickr.com/photos/watz/sets/72157594387603246/):

<a href="http://www.flickriver.com/photos/watz/sets/72157594387603246/"><img src="http://www.flickriver.com/badge/user/set-72157594387603246/recent/shuffle/medium-horiz/ffffff/333333/98203235@N00.jpg" border="0" alt="watz - View my 'Object #1-3' set on Flickriver" title="watz - View my 'Object #1-3' set on Flickriver"/></a>

Neben vielen anderen beherrscht die Bibliothek einen Export nach [PoVRay](PoVRay) und nach STL.

## Links

  * [unlekkerLib Homepage](http://workshop.evolutionzone.com/unlekkerlib/)
