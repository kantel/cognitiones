#title "Structure Synth"

<%= imageref("structuresynthscreenshot") %>

**Structure Synth** ist eine freie (GPL), plattformübergreifende Desktop-Anwendung, die das Prinzip von [Context Free Art](Context Free Art) auf dreidimensionale Graphiken überträgt. Die verwendete Skriptsprache heißt *EisenScript* und die damit erzeugten Objekte können sowohl als `.obj` exportiert und dann in [Blender](Blender), [MeshLab](MeshLab) oder ähnlicher Software weiterverarbeitet werden. Es soll aber auch eine native [PoVRay](PoVRay)-Unterstützung geben.

Zu Structure Synth gibt es noch ein Schwesternprojekt *Fragmentarium* (ebenfalls GPL), das die Graphiken auf der GPU erzeugt, aber nicht mehr weiterentwickelt wird. Und von der Fork *FragM* gibt es (noch?) keine fertigen Binaries.

## Structure Synth und macOS

Zumindest auf meinen Mac (macOS Mojave 10.14.2) startet Powersync mit der Fehlermeldung `Could not locate directory in: Examples,../../Examples` und `Loading file: /[not dound]/Default.es`. Das beeinträchtigt zwar nicht die Funktion, stört mich aber schon ein wenig. Verknüpft man aber die *Eisenscript*-Dateien (Endung `.es`) mit Strucutre Synth und startet dann das Programm mit einem Doppelklick auf die Datei `Default.es` (im Verzeichnis `Examples`) ist alles schick.

## Literatur und Tutorials

- [Tutorial Tuesday 27: Design for Complexity With Structure Synth](https://www.shapeways.com/blog/archives/32934-tutorial-tuesday-27-design-complexity-structure-synth.html)
- [Structure Synth im Blog der Entwickler](http://blog.hvidtfeldts.net/index.php/category/structure-synth/)
- *Mikael Hvidtfeldt Christensen*: [Structural Synthesis using a Context Free Design Grammar Approach](http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.210.1443&rep=rep1&type=pdf), 12th Generative Art Conference GA2009 (PDF)

## Links

- [Structure Synth Home](http://structuresynth.sourceforge.net/)
- [Structure Synth Download](http://structuresynth.sourceforge.net/download.php)
- [Structure Synth @ Sourceforge](https://sourceforge.net/projects/structuresynth/)
- [Fragmentarium Home](http://syntopia.github.io/Fragmentarium/)
- [Fraggmentarium @ GitHub](https://github.com/Syntopia/Fragmentarium)
- [Fragmentarium Download](http://syntopia.github.io/Fragmentarium/get.html)
- [Fragmentarium Tutorial](http://syntopia.github.io/Fragmentarium/usage.html)
- [FragM](https://github.com/3Dickulus/FragM) – aktuellere Fork, ebenfalls auf GitHub
- [Syntopia Blog](http://blog.hvidtfeldts.net/)