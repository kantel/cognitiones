#title "pyprocessing"

<%= imageref("pyprocessing") %>

**pyprocessing** (BSD-Lizenz) ist der Versuch, [Processings](Processing) einfache Graphikfunktionen und Interaktionsmöglichkeiten nach [Python](Python) zu portieren. Nicht alle Processingfunktionen sollen implemeniert werden, da in vielen Fällen, z.B. beim Parsen von XML-Dateien, Python schon gute oder gar bessere Alternativen bietet.

Im Gegensatz zu Processing kommt pyprocessing nicht mit einer IDE sondern muß im Editor der Wahl gestartet werden. Außerdem muß zu Beginn die Bibliothek importiert und zum Start des Programms eine run()-Methode aufgerufen werden.

## Beispiel

Dieses einfache Processing-Skript 

	// 
	// Processing 
	//
	size(200,200);
	rectMode(CENTER);
	rect(100,100,20,100);
	ellipse(100,70,60,60);
	ellipse(81,70,16,32); 
	ellipse(119,70,16,32); 
	line(90,150,80,160);
	line(110,150,120,160);

sieht in pyprocessing so aus: 

	#
	# pyprocessing equivalent
	#
	from pyprocessing import *
	size(200,200)
	rectMode(CENTER)
	rect(100,100,20,100)
	ellipse(100,70,60,60)
	ellipse(81,70,16,32) 
	ellipse(119,70,16,32) 
	line(90,150,80,160)
	line(110,150,120,160)
	run()

Obwohl das Programm zur Zeit noch eine sehr niedrige Versionsnummer besitzt, macht es einen vielversprechenden Eindruck.

## Installation

Das pyprocessing-Backend basiert auf OpenGL und [PyGlet](PyGlet) und sollte daher überall dort laufen, wo diese Pakete auch laufen. Ein fertiges Installationspaket gibt es für Windows. 

Unter MacOS X wechselt man im Terminal in das Download-Directory, wo man das Paket heruntergeladen und ruft dann – wie gewohnt – *setup.py* auf. 

	cd downloads/pyprocessing-0.1.2 
	sudo python setup.py install

Wichtig ist, daß PyGlet **vorher** installiert wurde.

## Links

  * [pyprocessing Home at Google Code](http://code.google.com/p/pyprocessing/)
  * [Tutorial](http://code.google.com/p/pyprocessing/wiki/UsageInstructions)
  

