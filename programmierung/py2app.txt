#title "py2app"

**py2app** ist ein freies (MIT- oder PSF-Lizenz) Werkzeug, um *standalone* macOS Binaries (Anwendungen und Pug-ins) aus [Python](Python)-Skripten zu erstellen. py2app muß zwingend auf einem macOS-Rechner laufen, das Tool kann keine Mac-Anwendungen auf anderen Plattformen erzeugen.

## Installation

Die Anwendung ist auf PyPI, daher genügt ein

~~~
pip install py2app
~~~

für die Installation.

## Links

- [py2app @ GitHub](https://github.com/ronaldoussoren/py2app)
- [py2app Dokumentation](https://py2app.readthedocs.io/en/latest/)
- [py2app @ PyPI](https://pypi.org/project/py2app/)