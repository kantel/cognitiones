#title "Softcomputing"

## Themen

  * [Ameisenfarm](Ameisenfarm)
  * [Chaos](Chaos)
  * [Game of Life](Game of Life)
  * [Künstliche Intelligenz](Künstliche Intelligenz)
      * [DALL-E](DALL-E)
      * [DALL-E mini](DALL-E mini)
      * [Imagegen](Imagegen)
      * [MidJourney](MidJourney)
  * [Künstliches Leben](Künstliches Leben)
  * [Multi-Agenten-Simulation](Multi-Agenten-Simulation)
  * [Schwarmintelligenz](Schwarmintelligenz)
  * [Zelluläre Automaten](Zelluläre Automaten)
    * [Moore-Nachbarschaft](Moore-Nachbarschaft)
    * [Von-Neumann-Nachbarschaft](Von-Neumann-Nachbarschaft)
  

## Software

  * [Breve](Breve)
  * [Golly](Golly)

## Bücher

  * [Mazes For The Mind](Mazes For The Mind)
  * [Computers, Pattern, Chaos And Beauty](Computers, Pattern, Chaos And Beauty)
