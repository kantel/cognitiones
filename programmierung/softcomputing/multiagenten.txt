#title "Multi-Agenten-Simulation"

Die **Multi-Agenten-Simulation** wendet das Konzept der Multi-Agenten-Systeme in der Simulation an. Aktive Komponenten des zu untersuchenden Systems werden als Agenten betrachtet, deren Verhalten einzeln spezifiziert wird. Damit können insbesondere emergente Phänomene und dynamische Wechselwirkungen zwischen Agenten nachgewiesen werden. Multi-Agenten-Simulation ist sehr verbreitet in den Bereichen Biologie, Soziologie, Verkehrsphysik und bei Evakuierungssimulationen. Dort werden sie verwendet, um Zusammenhänge zu verstehen und Theorien in einer kontrollierten Laborumgebung zu belegen. Seit einigen Jahren wird die Multi-Agenten-Simulation auch verstärkt in der 3D-[Computergraphik](Computergraphik) und Filmtechnik eingesetzt, um Massenszenen kostengünstig zu simulieren. Ein weiteres Anwendungsgebiet sind makroökonomische Wirtschaftssimulationen. Wenn die Agenten in einer (simulierten) räumlichen Umgebung handeln und diese diskret abgebildet wird, ist der Übergang zu [Zellulären Automaten](Zelluläre Automaten) fließend.

[[dr]]<iframe src="http://rcm-eu.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=derschockwell-21&o=3&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=1119970792" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe>[[dd]]

## Software für Multi-Agenten-Simulationen

  * [OpenStarLogo und StarLogo TNG](OpenStarLogo und StarLogo TNG)
  * [NetLogo](NetLogo)

