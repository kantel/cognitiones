#title "Haskell"

**Haskell** ist eine rein funktionale Programmiersprache, benannt nach dem US-amerikanischen Mathematiker *Haskell Brooks Curry*, dessen Arbeiten zur mathematischen Logik eine Grundlage funktionaler Programmiersprachen bilden. Haskell basiert auf dem Lambda-Kalkül, weshalb auch der griechische Buchstabe Lambda als Logo verwendet wird.

## Links

  * [Haskell.org](http://www.haskell.org/)
  * <%= wpde("Haskell (Programmiersprache)") %> in der Wikipedia
  * [Blog zur funktionalen Programmierung](http://funktionale-programmierung.de/)