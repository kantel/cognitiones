#title "Docker"

<%= imageref("docker-logo") %>

[Docker](https://www.docker.io/) (Apache-Lizenz) erlaubt es, Apps in Container zu verpacken, die überall laufen - sowohl auf dem Notebook des Entwicklers als auch direkt auf einem Server oder einer virtuellen Maschine. Die neue [Version 0.7](http://blog.docker.io/2013/11/docker-0-7-docker-now-runs-on-any-linux-distribution/) unterstützt nun offiziell alle Linux-Distributionen und bietet sechs weitere wesentliche Neuerungen.

## Literatur

  * *[Docker: App-Container, die überall laufen](http://www.golem.de/news/docker-app-container-die-ueberall-laufen-1311-102976.html)*, Golem.de vom 26. November 2013
  * *[Docker nun auch für Mac OS X erhältlich](http://www.heise.de/developer/meldung/Docker-nun-auch-fuer-Mac-OS-X-erhaeltlich-2106460.html)*, heise Developer vom 6. Februar 2014
  * Golo Roden: *[Packstation. Mit Docker automatisiert Anwendungscontainer erstellen](http://www.heise.de/developer/artikel/Mit-Docker-automatisiert-Anwendungscontainer-erstellen-2145030.html)*, heise Developer vom 14. März 2014
  * René Büst: *[Docker Container: Die Zukunft moderner Applikationen und Multi-Cloud Deployments](http://www.silicon.de/41602277/docker-container-zukunft-moderner-applikationen-und-multi-cloud-deployments/)*, Silicon.de vom 4. August 2014

## Links

  * [Docker Home](https://www.docker.io/)
  * [Docker @ GitHub](https://github.com/dotcloud/docker/)
