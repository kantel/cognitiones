#title "OpenStarLogo und StarLogo TNG"

**StarLogo** ist ein von [Mitchel Resnick](Mitchel Resnick) und anderen am MIT Media Lab entwickelter [Logo](Logo)-Dialekt für massiv parallele, agenten-basierte, dezentrale Simulationen ([Multi-Agenten-Simulation](Multi-Agenten-Simulation)). Die erste Version lief auf der Connection Machine von Danny Hillis, eine spätere Version unter Mac OS (MacStarLogo Classic). Die aktuelle Version ist in Java programmiert und läuft überall dort, wo auch eine JVM läuft. Sie ist als OpenStarLogo frei erhältlich, allerdings entspricht die Lizenz nicht wirklich einer Open Source-Lizenz, da der Quellcode nicht kommerziell verwendet werden darf.

## Siehe auch

  * [NetLogo](NetLogo)
  * [Scratch](Scratch)

## Links

  * [Star Logo Home](http://education.mit.edu/starlogo/)
  * [StarLogo TNG Home](http://education.mit.edu/projects/starlogo-tng)
  * <%= wp("StarLogo") %> in der Wikipedia

