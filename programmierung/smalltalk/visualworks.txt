#title "VisualWorks"

**VisualWorks** aka Cincom Smalltalk ist eine plattformübergreifende Implementierung der Sprache [Smalltalk](Smalltalk). Es ist als Entwicklungssystem implementiert, das auf »Images« basiert, bei denen es sich um dynamische Sammlungen von Softwareobjekten handelt, die jeweils in einem Systemabbild enthalten sind.

Die Linie von VisualWorks geht auf die erste Implementierung von Smalltalk-80 von Xerox PARC zurück. In den späten achtziger Jahren wurde eine Gruppe von Smalltalk-80-Entwicklern von ParcPlace Systems ausgegliedert, um Smalltalk-80 als kommerzielles Produkt weiterzuentwickeln. Das kommerzielle Produkt wurde ursprünglich ObjectWorks und dann VisualWorks genannt. Am 31. August 1999 wurde das Produkt VisualWorks an Cincom verkauft. VisualWorks läuft unter vielen Betriebssystemen, einschließlich Windows, Mac OS X, Linux und mehreren Unix-Versionen.

Eine nichtkommerzielle Version von VisualWorks ist kostenlos verfügbar. Die nichtkommerzielle Version bietet alle Möglichkeiten und Funktionen der kommerziellen Version. In beiden Versionen, wie in allen Smalltalks, kann der Benutzer den gesamten Quellcode sehen. Dies umfaßt alle Systemklassen, einschließlich Browser und GUI-Builder.


VisualWorks unterstützt plattformübergreifende Entwicklungsprojekte aufgrund seiner integrierten Funktionen für mehrere Plattformen. Beispielsweise muß eine GUI-Anwendung nur einmal entwickelt werden und kann dann auf andere Widget-Stile umgestellt werden. Eine VisualWorks-Anwendung kann ohne Änderungen auf allen unterstützten Plattformen ausgeführt werden. Nur die virtuelle Maschine ist plattformabhängig.

## Tutorials

- The late James Robertson created a [series of introductory videos for Cincom Smalltalk (VisualWorks)](https://medium.com/@richardeng/james-robertson-on-the-basics-of-cincom-smalltalk-728d99beacf6). They’re a few years old, so they’re a bit out of date, but their essence remains intact. They should still be helpful in getting you up to speed with VisualWorks.

## Links

- [VisualWorks @ Cincom](http://www.cincomsmalltalk.com/main/products/visualworks/)
- [VisualWorks in der Wikipedia](https://en.wikipedia.org/wiki/VisualWorks)