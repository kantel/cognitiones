#title "Croquet"

**Croquet** (MIT-Lizenz) ist ein [Squeak](Squeak)-basiertes Framework für eine Multi-User-[3D](3D)-Umgebung. Im Gegensatz zu *Second Life* benötigt Croquet keinen fremden Server, denn die Kommunikation verläuft Peer-to-Peer. Jeder kann sich seine eigene Welt auf seinem eigenen Server basteln und mit anderen Welten auf anderen Servern kommunizieren.

Die Weiterentwicklung des Framerworks scheint ein wenig eingeschlafen, als Nachfolger gilt [Open Cobalt](Open Cobalt)

## Links

  * [Croquet Home](http://opencroquet.org/)
  * <%= wp("Croquet Project") %> in der englischsprachigen Wikipedia