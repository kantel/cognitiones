#title "Homebrew"

**Homebrew** ist ein in [Ruby](Ruby) geschriebener Paketmanager für [MacOS X](MacOS X). Obwohl ich wegen schlechter Erfahrungen mit MacPorts und Fink (die einem immer das halbe Internet auf die Maschine luden) Paketmanagern (für MacOS X) eigentlich skeptisch gegenüber stehe, scheint Homebrew wirklich brauchbar zu sein.

## Installation

Die Installation erfolgt am einfachsten mit folgendem [kleinen Ruby-Skript](https://github.com/mxcl/homebrew/blob/master/Library/Contributions/install_homebrew.rb):

	/usr/bin/ruby -e "$(/usr/bin/curl -fsSL
	https://raw.github.com/mxcl/homebrew/master/Library/Contributions/install_homebrew.rb)"

*(Das da oben ist ein Einzeiler, der Umbruch erfolgte nur wegen der Lesbarkeit, statt des Zeilenumbruchs ist ein Leerzeichen zu verwenden.)*

Das Script installiert Homebrew in `usr/local`, so daß kein `sudo` für den Aufruf erforderlich ist.

## Nutzung

Programme wie zum Beispiel wget können mit Homebrew einfach mit folgendem Einzeiler installiert werden:

	brew install wget


## Was tun?

Was tun, wenn [Homebrew](http://cognitiones.kantel-chaos-team.de/programmierung/homebrew.html) zickt (*`Cowardly refusing to 'sudo brew install'`*)? [Das](http://tips.llp.pl/post/17947067820/installing-nodejs-on-osx-with-homebrew)! 

	sudo chown root:staff /usr/local/bin/brew

Anschließend nicht vergessen, die Rootrechte der Brauerei wieder zu entziehen:

	sudo chown -R `whoami`:staff /usr/local

Wenn die Installation von Graphik-Paketen und -Bibliotheken Probleme bereitet, kann manchmal eine Neuinstallation der `libtiff` helfen:

	sudo brew uninstall libtiff
	sudo brew install libtiff

## Links

  * [Homebrew Home](http://mxcl.github.com/homebrew/)
  * [Installationsanweisung auf GitHub](https://github.com/mxcl/homebrew/wiki/installation)
