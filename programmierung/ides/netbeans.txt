#title "NetBeans"

<%= imageref("javafx", {:border => "0", :width => "480", :height => "359", :alt => "NetBeans Screenshot"}) %>

Die **NetBeans IDE** (Open Source) ist Suns/Oracles Antwort auf Eclipse. Wie diese auch wurde NetBeans komplett in [Java](Java) geschrieben. NetBeans wurde hauptsächlich für die Programmiersprache Java entwickelt, nach und nach kamen jedoch weitere Sprachen hinzu, unter anderem <del>[Ruby](Ruby)</del>, [PHP](PHP) und [JavaScript](JavaScript). Eine [Python](Python) Unterstützung war lange geplant, ist aber bis heute nicht realisiert worden.

NetBeans ist vor allem auch die IDE, um [JavaFX](JavaFX)-Anwendungen zu entwickeln. Seit der Version 8.1 ist die [HTML5](HTML5)-/[JavaScript](JavaScript)-Unterstützung gewachsen, so daß die IDE auch für »reine« Webentwickler interessant geworden ist.

## Links

  * [NetBeans Homepage](http://www.netbeans.org/)
  * [NetBeans](http://de.wikipedia.org/wiki/NetBeans) in der Wikipedia
   