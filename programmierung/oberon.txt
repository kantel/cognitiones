#title "Oberon"

**Oberon** ist eine von [Niklaus Wirth](Niklaus Wirth) und Jürg Gutknecht entwickelte, objektorientierte, streng strukturierte Programmiersprache. Sie ist den ebenfalls von Wirth entworfenen Vorgängern [Pascal](Pascal) und [Modula-2](Modula-2) recht ähnlich, allerdings strukturierter als Pascal und mächtiger, gleichzeitig aber erheblich weniger umfangreich als Modula-2. Das *ETH Oberon System* ist ein eigenständiges Betriebssystem der ETH Zürich, das in der Sprache Oberon implementiert ist.

<%= imageref("oberonv4", {:width => "560", :height => "420"}) %>

Es existierten mehrere Oberon-Dialekte: Die Originalversion war Oberon V4 mit nicht überlappenden Fenstern.

<%= imageref("oberons3", {:width => "560", :height => "414"}) %>

Daneben gab es Oberon S3 als System mit überlappenden Fenstern. Es wurde vor allem im [Project Voyager](http://www.statlab.uni-heidelberg.de/projects/voyager/) des [StatLabs](http://www.statlab.uni-heidelberg.de/) der Universität Heidelberg eingesetzt.

## Links

  * [Oberon](http://de.wikipedia.org/wiki/Oberon_(Programmiersprache) in der Wikipedia
  * [Oberon V4 unter Tinycore Linux](http://strotmann.de/roller/linuxkistchen/entry/oberon_v4_unter_tinycore_linux)
  * [Oberon for GNU/Linux](http://olymp.idle.at/tanis/oberon.linux.html)