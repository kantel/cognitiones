#title "LiveCode Community Edition"

<iframe width="560" height="315" src="http://www.youtube.com/embed/3AbV4Evqy8E?list=UUrP9IskEPHuirOh0KyKjvMA" frameborder="0" allowfullscreen></iframe>

**LiveCode Community Edition** ist die freie (GPL3) Version des plattformunabhängigen [HyperCard](HyperCard)-Klons und/oder Nachfolgers *LiveCode*.

Live Code hieß früher auch einmal *Runtime Revolution* (nur kommerziell) und basierte auf der *MetaCard*-Engine, die Runtime 2003 übernahm.

Live Code läuft plattformübergreifend unter MacOS X, Windows und Linux (auch [Raspberry Pi](Raspberry Pi)). Selbst die aktuelle Versionen 9.6.1 (Stable) unterstützt noch Windows 7, MacOS X 10.9.x (Mavericks) und betagte Linux-Systeme.

Mit LiveCode können *Standalone Executables* nicht nur für die unterstützten Entwicklerplattformen macOS, Windows, Linus und Raspberry Pi gebaut werden, sondern auch für [Android](Android), [iOS](iOS) und [HTML5](HTML5)

## Tutorials

- *Dr. Robert Wolf*; »[Lecture Notes LiveCode](http://www.moz.ac.at/user/rwolff/LVAs/LiveCode/LiveCodeFrameset.html)« vom 9. März 2012
- *Andreas Möller*: »[Plattformübergreifende Apps mit Livecode](https://www.linux-magazin.de/ausgaben/2016/11/livecode/)«, aus Linux-Magazin, November 2016
- *Hauke Fehr* »[Einführung in LiveCode](https://www.informatik-aktuell.de/entwicklung/programmiersprachen/einfuehrung-in-livecode.html)«, Informatik Aktuell vom 10. April 2018

## Literatur

[[dr]]<%= imageref("livecodelitecover") %>[[dd]]

  * Stephen Goldberg: *[LiveCode Lite: Computer Programming Made Ridiculously Simple](http://www.medmaster.net/livecode.html)*, Medmaster Books 2015 (kostenloser Download als PDF). Auf der gleichen Seite gibt es auch noch jeweils ein LiveCode-Scripting-Tutorial für Mac oder Windows ebenfalls zum kostenlosen Download.
  * Hauke Fehr: *[Eigene Apps programmieren. Ohne Vorkenntnisse einsteigen][a1]*, Bonn (Rheinwerk) <sup>2</sup>2020 (meines Wissens das einzige deutschsprachige Buch zu LiveCode).

[a1]: https://www.amazon.de/Eigene-Apps-programmieren-Programmieren-Vorkenntnisse/dp/3836272067?__mk_de_DE=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=1X31U13W7VVJR&dchild=1&keywords=eigene+apps+programmieren&qid=1620650599&sprefix=Eigene+Apps%2Caps%2C180&sr=8-1&linkCode=ll1&tag=derschockwell-21&linkId=5a724aa6017f7e29632db892e2d8f0b6&language=de_DE&ref_=as_li_ss_tl

## Links

  * [LiveCode Home](https://livecode.com/)
  * [Alle LiveCode Downloads](http://downloads.livecode.com/livecode/)
  * <%= wp("LiveCode") %> in der Wikipedia
  * [LiveCode Blog](https://www.livecode-blog.de/), deutschsprachiges LiveCode Blog von *Hauke Fehr*

