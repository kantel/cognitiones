# encoding: utf-8

def subnavbar()
  dirname = adrObject.dirname
  if dirname != adrSiteRootTable
    embed_in_template(process(dirname))
  end
end

def process(dir)
  # arr = Array.new
  arr = ""
  html.pagesInFolder(dir).each do |what|
    title, path = html.getTitleAndPaths(what)
    s = html.getLink(title, what)
    arr << "<li>#{s}</li>\n"
  end
  arr
end

def embed_in_template(arr)
  return "" unless arr
  ss = <<END
  <ul class="subnav">
  #{arr}
  </ul>
END
  ss
end