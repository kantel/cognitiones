def flattr(po)
  title, path = html.getTitleAndPath(po)
  rootURL = "http://cognitiones.kantel-chaos-team.de/"
  s = ""
  s << rootURL + path
  "<a href='https://flattr.com/submit/auto?user_id=kantel&url=#{s}&title=#{title}&category=text'><img src='https://api.flattr.com/button/flattr-badge-large.png' alt='Dieses Notizheft flattrn' title='#{title} bitte flattrn!' /></a>"
end