def postMacroFilter(adrPageTable)
  # adrPageTable[:postmacrotext] = process(adrPageTable[:postmacrotext])
  # example:
  # support for writing page as kramdown
  ## obsolet -ka-
  # support für Multimarkdown (mmd) von mir hinzugefügt (07.03.2012, -ka-)
  # TODO: Funzt noch nicht wirklich ...
  if adrPageTable[:mmd]
    IO.popen("/usr/local/bin/multimarkdown", "r+") do |io|
      io.write adrPageTable[:bodytext]
      io.close_write
      adrPageTable[:bodytext] = io.read
    end
  end
end